VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "My Robot"
   ClientHeight    =   4590
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   2895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   306
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   193
   StartUpPosition =   3  'Windows の既定値
   Begin MSComctlLib.ListView viewProgramList 
      Height          =   2415
      Left            =   120
      TabIndex        =   9
      Top             =   2040
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   4260
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Title"
         Object.Width           =   4498
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Path"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.CommandButton cmdLoadProgram 
      Caption         =   "Load"
      Height          =   375
      Left            =   2040
      TabIndex        =   8
      Top             =   1560
      Width           =   735
   End
   Begin VB.CommandButton cmdEditProgram 
      Caption         =   "Edit"
      Height          =   375
      Left            =   1080
      TabIndex        =   7
      Top             =   1560
      Width           =   735
   End
   Begin VB.CommandButton cmdNewProgram 
      Caption         =   "New"
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   1560
      Width           =   735
   End
   Begin VB.Timer StopTimer 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   3000
      Top             =   600
   End
   Begin VB.CommandButton cmdStopProgram 
      Caption         =   "Stop"
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdStartProgram 
      Caption         =   "Start"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.PictureBox picCapture 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      Height          =   735
      Index           =   0
      Left            =   3000
      ScaleHeight     =   49
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   65
      TabIndex        =   0
      Top             =   1200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Timer MainLoopTimer 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   3000
      Top             =   120
   End
   Begin VB.Line Line4 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   192
      Y1              =   97
      Y2              =   97
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   192
      Y1              =   96
      Y2              =   96
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   192
      Y1              =   1
      Y2              =   1
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   192
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Label lblStopTimeValue 
      Alignment       =   1  '右揃え
      BorderStyle     =   1  '実線
      Caption         =   "0"
      Height          =   255
      Left            =   1440
      TabIndex        =   5
      Top             =   1080
      Width           =   1335
   End
   Begin VB.Label lblStopTimeLabel 
      AutoSize        =   -1  'True
      Caption         =   "Stop Time (sec):"
      Height          =   180
      Left            =   120
      TabIndex        =   4
      Top             =   1080
      Width           =   1260
   End
   Begin VB.Label lblProgramTitle 
      BorderStyle     =   1  '実線
      Caption         =   "Program Title"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   2655
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "Menu"
      Begin VB.Menu mnuMenu_Exit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuList 
      Caption         =   "List"
      Enabled         =   0   'False
      Begin VB.Menu mnuList_Delete 
         Caption         =   "Delete"
         Shortcut        =   {DEL}
      End
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'#Const DEBUG_MODE = 1

Private Const PROGRAM_DIRECTORY     As String = "prog"
Private Const ONEDAY_MILLISECONDS   As Long = 86400000

Dim Program             As PCodeList
Dim CurrentList         As PCodeList
Dim Stack               As ListStack
Dim Colors()            As Long
Dim SavedPosition()     As POINTAPI
Dim Timestamp()         As SYSTEMTIME
Dim WaitStartTime       As SYSTEMTIME
Dim WaitFlag            As Boolean
Dim WaitTime            As Long
Dim WaitDay             As Long
Dim mProgDirPath        As String
Dim mProgListPath       As String
Dim mProgramList        As Dictionary
Dim mNextID             As Long
Dim mStopTime           As Long
Dim mStartTime          As Long
Dim mPath               As String
Dim mDate
Dim mDiffHeight         As Single
Dim mDefaultHeight      As Single

#If DEBUG_MODE > 0 Then

Private Function SetCursorPos(X As Long, Y As Long) As Long
    Debug.Print "SetCursorPos("; X; ","; Y; ")"
End Function

Private Sub mouse_event(ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal dwData As Long, ByVal dwExtraInfo As Long)
    Debug.Print "mouse_event("; dwFlags; ","; dx; ","; dy; ","; dwData; ","; dwExtraInfo; ")"
End Sub

#End If

Private Sub LoadProgram()
    Dim Filer       As PCodeListFiler
    Dim p           As String
    
    
    If viewProgramList.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    With viewProgramList.SelectedItem
        p = .SubItems(1)
    
        If FSO.FileExists(p) = False Then
            MsgBox "file is not exist!" & vbNewLine _
                    & .Text & vbNewLine _
                    & p, vbCritical, "Error"
            Exit Sub
        End If
    End With
    
    cmdStartProgram.Enabled = False
    cmdStopProgram.Enabled = False
    
    mPath = p
    mDate = FileDateTime(p)
    
    Set Filer = New PCodeListFiler
    Set Program = Filer.LoadCodeList(p)
    
    If Program Is Nothing Then
        MsgBox Filer.ErrorMessage, vbExclamation, "Error"
        Set Filer = Nothing
        Exit Sub
    End If
    
    mStopTime = Filer.StopTime
    
    lblProgramTitle.Caption = Filer.Title
    lblStopTimeValue.Caption = CStr(mStopTime)
    
    cmdStartProgram.Enabled = True
    
    Set Filer = Nothing
    
End Sub

Private Sub EndProgram()
    MainLoopTimer.Enabled = False
    StopTimer.Enabled = False
    cmdStopProgram.Enabled = False
    cmdStartProgram.Enabled = True
    cmdNewProgram.Enabled = True
    cmdEditProgram.Enabled = True
    cmdLoadProgram.Enabled = True
    viewProgramList.Enabled = True
    Me.Height = mDefaultHeight
    Debug.Print "End Of Program"
End Sub

Private Sub MakePosition(ByRef pt As POINTAPI, Code As PCode, _
                         iBase As Integer, iSPos As Integer, iDx As Integer, iDy As Integer)
    Select Case Code.IntData(iBase)
    Case POS_BASE_SCREEN_TOP_LEFT
        pt.X = 0&
        pt.Y = 0&
    Case POS_BASE_SCREEN_TOP_RIGHT
        pt.X = CLng(GetScreenWidth) - 1&
        pt.Y = 0&
    Case POS_BASE_SCREEN_BOTTOM_LEFT
        pt.X = 0&
        pt.Y = CLng(GetScreenHeight) - 1&
    Case POS_BASE_SCREEN_BOTTOM_RIGHT
        pt.X = CLng(GetScreenWidth) - 1&
        pt.Y = CLng(GetScreenHeight) - 1&
    Case POS_BASE_MOUSE_POINTER
        Call GetCursorPos(pt)
    Case POS_BASE_SAVED_POSITION
        pt = SavedPosition(Code.IntData(iSPos))
    End Select
    pt.X = pt.X + Code.LngData(iDx)
    pt.Y = pt.Y + Code.LngData(iDy)
End Sub

Private Function ChangeCodeList(NextCodeList As PCodeList) As Boolean
    ChangeCodeList = False
    If NextCodeList Is Nothing Then
        Exit Function
    End If
    If NextCodeList.Count = 0 Then
        Exit Function
    End If
    Call Stack.Push(CurrentList)
    Set CurrentList = NextCodeList
    Call CurrentList.Reset
    ChangeCodeList = True
End Function


Private Sub cmdEditProgram_Click()
    Dim Filer           As ProgramListFiler
    Dim pForm           As ProgramForm
    Dim pInfo1          As ProgramInfo
    Dim pInfo2          As ProgramInfo
    Dim d
    
    If viewProgramList.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    Set pInfo1 = mProgramList.Item(viewProgramList.SelectedItem.SubItems(1))
    Debug.Assert Not pInfo1 Is Nothing
    Set pInfo2 = pInfo1.GetCopy()
    
    Set pForm = New ProgramForm

    pForm.SetProgramInfo pInfo2
    
    pForm.Show vbModal, Me
        
    If pInfo1.Title <> pInfo2.Title Then
        pInfo1.Title = pInfo2.Title
        viewProgramList.ListItems.Item(pInfo1.Path).Text = pInfo1.Title
        Set Filer = New ProgramListFiler
        Call Filer.SaveProgramList(mProgListPath, mProgramList, mNextID)
        Set Filer = Nothing
    End If
    
    If mPath = pInfo1.Path Then
        d = FileDateTime(mPath)
        If DateDiff("s", mDate, d) > 0& Then
            cmdStartProgram.Enabled = False
        End If
    End If
    
    Unload pForm
    
    Set pForm = Nothing

End Sub

Private Sub cmdLoadProgram_Click()

    Call LoadProgram
    
End Sub

Private Sub cmdNewProgram_Click()
    Dim p           As String
    Dim fn          As String
    Dim NextID      As Long
    Dim pForm       As ProgramForm
    Dim pInfo       As ProgramInfo
    Dim Filer       As ProgramListFiler
    
    NextID = mNextID
    Do
        If NextID < 100& Then
            fn = "Program" & Right$("000" & CStr(NextID), 3&)
        Else
            fn = Program & CStr(NextID)
        End If
        p = FSO.BuildPath(mProgDirPath, fn & ".prog")
        NextID = NextID + 1&
    Loop While FSO.FileExists(p)
    
    Set pForm = New ProgramForm
    Set pInfo = New ProgramInfo
    
    pInfo.Title = "Program " & CStr(NextID - 1&)
    pInfo.Path = p
    
    Call pForm.SetProgramInfo(pInfo)
    
    pForm.Show vbModal, Me
    
    If FSO.FileExists(pInfo.Path) Then
        mNextID = NextID
        
        viewProgramList.ListItems.Add(Key:=pInfo.Path, Text:=pInfo.Title).SubItems(1) = pInfo.Path
        
        mProgramList.Add pInfo.Path, pInfo
        
        Set Filer = New ProgramListFiler
        Call Filer.SaveProgramList(mProgListPath, mProgramList, mNextID)
        Set Filer = Nothing
        
    End If
    
    Unload pForm
    
    Set pForm = Nothing
    Set pInfo = Nothing
    
    
End Sub

Private Sub cmdStartProgram_Click()

    Debug.Assert Not mProgramList Is Nothing
    
    cmdNewProgram.Enabled = False
    cmdEditProgram.Enabled = False
    cmdLoadProgram.Enabled = False
    viewProgramList.Enabled = False
    cmdStartProgram.Enabled = False
    
    Me.Height = Line3.Y1 * Screen.TwipsPerPixelY + mDiffHeight
    
    Set CurrentList = Program
    Call CurrentList.Reset
    
    If Stack Is Nothing Then
        Set Stack = New ListStack
    Else
        Call Stack.RemoveAll
    End If
    
    mStartTime = mStopTime
    WaitFlag = False
    Randomize Timer
    
    lblStopTimeValue.Caption = CStr(mStartTime)
    
    StopTimer.Enabled = True
    cmdStopProgram.Enabled = True
    MainLoopTimer.Enabled = True

End Sub

Private Sub cmdStopProgram_Click()
    Call EndProgram
End Sub

Private Sub Form_Load()
    Dim i       As Integer
    Dim dic     As Dictionary
    Dim Filer   As ProgramListFiler
    Dim v, W
    
    'Init Controls
    
    Line1.X1 = 0
    Line1.X2 = Me.ScaleWidth
    Line2.X1 = 0
    Line2.X2 = Line1.X2
    Line3.X1 = 0
    Line3.X2 = Line1.X2
    Line4.X1 = 0
    Line4.X2 = Line1.X2
    
    With picCapture(0)
        .Width = GetScreenWidth()
        .Height = GetScreenHeight()
    End With
    
    For i = 1 To MAX_COUNT_OF_CAPTURE
        Load picCapture(i)
        With picCapture(i)
            .Width = picCapture(0).Width
            .Height = picCapture(0).Height
        End With
    Next i
    
    ' Init Program Variables
    
    ReDim Colors(MAX_COUNT_OF_COLOR)
    ReDim SavedPosition(MAX_COUNT_OF_POSITION)
    ReDim Timestamp(MAX_COUNT_OF_TIMESTAMP)

    ' Init Path

    mProgDirPath = FSO.BuildPath(App.Path, PROGRAM_DIRECTORY)
    If FSO.FolderExists(mProgDirPath) = False Then
        FSO.CreateFolder mProgDirPath
    End If
    
    ' Load ProgramList
    
    mProgListPath = FSO.BuildPath(mProgDirPath, "Program.list")
    mNextID = 1&
    
    If FSO.FileExists(mProgListPath) Then
        Set Filer = New ProgramListFiler
        Set mProgramList = Filer.LoadProgramList(mProgListPath)
        If mProgramList Is Nothing Then
            MsgBox Filer.ErrorMessage, vbExclamation, "Error"
            MsgBox "Can not load program list!"
        Else
            W = mProgramList.Items()
            For Each v In W
                With viewProgramList
                    .ListItems.Add(Key:=v.Path, Text:=v.Title).SubItems(1) = v.Path
                End With
            Next v
            mNextID = Filer.GetNextID()
        End If
        Set Filer = Nothing
    End If
    
    If mProgramList Is Nothing Then
        Set mProgramList = New Dictionary
    End If
    
    ' Remain Window Size
    
    Me.Show
    
    mDefaultHeight = Me.Height
    mDiffHeight = Me.Height - Me.ScaleHeight * Screen.TwipsPerPixelY
    
End Sub

Private Sub MainLoopTimer_Timer()
    Dim pickF   As ColorPickForm
    Dim Code    As PCode
    Dim i       As Integer
    Dim T       As SYSTEMTIME
    Dim pt      As POINTAPI
    Dim c       As Long
    
    If WaitFlag Then
        Call GetSystemTime(T)
        c = DiffDateOfSystemTime(WaitStartTime, T)
        If c < WaitDay Then
            Exit Sub
        End If
        If c = WaitDay Then
            If DiffTimeOfSystemTime(WaitStartTime, T) < WaitTime Then
                Exit Sub
            End If
        End If
        WaitFlag = False
    End If
    
    Do While CurrentList.HasNext() = False
        If Stack.IsEmptyStack() Then
            ' End Of Program
            Debug.Print "<No Code>"
            Call EndProgram
            Exit Sub
        Else
            Set CurrentList = Stack.Pop()
            Set Code = CurrentList.GetCurrent()
            Select Case Code.CodeID
            Case PCODE_REPEAT
                Code.LngData(PARAM_REPEAT_NOW_LNG) = Code.LngData(PARAM_REPEAT_NOW_LNG) + 1&
                If Code.LngData(PARAM_REPEAT_NOW_LNG) < Code.LngData(PARAM_REPEAT_COUNT_LNG) Then
                    Call Stack.Push(CurrentList)
                    Set CurrentList = Code.NestList(PARAM_LOOP_NEST)
                    Call CurrentList.Reset
                    Debug.Print "<REPEAT> "; Code.LngData(PARAM_REPEAT_NOW_LNG); " < "; Code.LngData(PARAM_REPEAT_COUNT_LNG)
                End If
            Case PCODE_LOOP
                Call Stack.Push(CurrentList)
                Set CurrentList = Code.NestList(PARAM_LOOP_NEST)
                Call CurrentList.Reset
                Debug.Print "<LOOP>"
            End Select
        End If
    Loop
    Set Code = CurrentList.GetNext()
    
'    If Code Is Nothing Then
'        Call EndProgram
'        Exit Sub
'    End If
    
    Select Case Code.CodeID
    Case PCODE_MOUSE_LEFT_DOWN
        Debug.Print "[MOUSE-LEFT-DOWN]"
        Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_LEFT_UP
        Debug.Print "[MOUSE-LEFT-UP]"
        Call mouse_event(MOUSEEVENTF_LEFTUP, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_LEFT_CLICK
        Debug.Print "[MOUSE-LEFT-CLICK]"
        Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_LEFTUP, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_LEFT_DBLCLICK
        Debug.Print "[MOUSE-LEFT-DBLCLICK]"
        Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_LEFTUP, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_LEFTUP, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_RIGHT_DOWN
        Debug.Print "[MOUSE-RIGHT-DOWN]"
        Call mouse_event(MOUSEEVENTF_RIGHTDOWN, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_RIGHT_UP
        Debug.Print "[MOUSE-RIGHT-UP]"
        Call mouse_event(MOUSEEVENTF_RIGHTUP, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_RIGHT_CLICK
        Debug.Print "[MOUSE-RIGHT-CLICK]"
        Call mouse_event(MOUSEEVENTF_RIGHTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_RIGHTUP, 0&, 0&, 0&, 0&)
    
    Case PCODE_MOUSE_RIGHT_DBLCLICK
        Debug.Print "[MOUSE-RIGHT-DBLCLICK]"
        Call mouse_event(MOUSEEVENTF_RIGHTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_RIGHTUP, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_RIGHTDOWN, 0&, 0&, 0&, 0&)
        Call mouse_event(MOUSEEVENTF_RIGHTUP, 0&, 0&, 0&, 0&)
        
    Case PCODE_MOUSE_MOVE
        Debug.Print "[MOVE]";
        Call MakePosition(pt, Code, PARAM_MOVE_BASE_INT, PARAM_MOVE_SPOS_INT, _
                            PARAM_MOVE_ADDX_LNG, PARAM_MOVE_ADDY_LNG)
        
        Debug.Print " ("; pt.X; ","; pt.Y; ")"
        Call SetCursorPos(pt.X, pt.Y)
    
    Case PCODE_SET_POS
        If Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_MANUAL Then
            StopTimer.Enabled = False
            Debug.Print "[SET-POS] Manual "; Code.IntData(PARAM_SET_POS_FROM_INT); " to "; Code.IntData(PARAM_SET_POS_TO_INT)
            Set pickF = New ColorPickForm
            For i = Code.IntData(PARAM_SET_POS_FROM_INT) To Code.IntData(PARAM_SET_POS_TO_INT)
                pickF.SetMessage "Position ID: " & CStr(i)
                pickF.Show vbModal, Me
                With SavedPosition(i)
                    .X = pickF.PositionX
                    .Y = pickF.PositionY
                    Debug.Print "<SET-POS> P["; i; "] ("; .X; ","; .Y; ")"
                End With
            Next i
            StopTimer.Enabled = True
            Unload pickF
            Set pickF = Nothing
        Else
            Debug.Print "[SET-POS] Automatic "; Code.IntData(PARAM_SET_POS_FROM_INT); " to "; Code.IntData(PARAM_SET_POS_TO_INT);
            Call MakePosition(pt, Code, PARAM_SET_POS_BASE_INT, PARAM_SET_POS_SPOS_INT, _
                            PARAM_SET_POS_ADDX_LNG, PARAM_SET_POS_ADDY_LNG)
            
            Debug.Print " ("; pt.X; ","; pt.Y; ")"
            For i = Code.IntData(PARAM_SET_POS_FROM_INT) To Code.IntData(PARAM_SET_POS_TO_INT)
                SavedPosition(i) = pt
            Next i
        End If
    
    Case PCODE_IF_COLOR
        
        Debug.Print "[IF-COLOR] Color("; Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT); ")=";
        Debug.Print Hex$(Colors(Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT)));
        Debug.Print " " & IF_COLOR_TYPE_NAME(Code.IntData(PARAM_IF_COLOR_TYPE_INT));
        
        Select Case Code.IntData(PARAM_IF_COLOR_TYPE_INT)
        Case IF_COLOR_CONSTANT
            c = Code.LngData(PARAM_IF_COLOR_COLOR_LNG)
        Case IF_COLOR_SAVED_COLOR
            Debug.Print " Saved COLOR-ID["; Code.IntData(PARAM_IF_COLOR_SAVED_ID); "]";
            c = Colors(Code.IntData(PARAM_IF_COLOR_SAVED_ID))
        Case IF_COLOR_PICK_COLOR
            Call MakePosition(pt, Code, PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, _
                                PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG)
                    
            c = picCapture(Code.IntData(PARAM_IF_COLOR_CAP_ID_INT)).Point(pt.X, pt.Y)
            
            Debug.Print " CAP-ID["; Code.IntData(PARAM_IF_COLOR_CAP_ID_INT); "]";
            Debug.Print " "; POS_BASE_NAME(Code.IntData(PARAM_IF_COLOR_BASE_INT));
            Debug.Print " "; Code.IntData(PARAM_IF_COLOR_SPOS_INT);
            Debug.Print " ("; pt.X; ","; pt.Y; ") ";
        End Select
        Debug.Print " "; Hex$(c);
        
        If c = Colors(Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT)) Then
            Call ChangeCodeList(Code.NestList(PARAM_IF_COLOR_TRUE_NEST))
            Debug.Print " : True"
        Else
            Call ChangeCodeList(Code.NestList(PARAM_IF_COLOR_FALSE_NEST))
            Debug.Print " : False"
        End If
    
    Case PCODE_IF_TIME_OVER
        Debug.Print "[IF-TIME-OVER] T["; Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT); "]";
        Call GetSystemTime(T)                               ' 現在時刻をtに取得
        pt.X = Code.LngData(PARAM_IF_TIME_OVER_LIMIT_LNG)   ' 期限を取得(一時代入)
        pt.Y = pt.X \ ONEDAY_MILLISECONDS                   ' 期限の日数
        pt.X = pt.X Mod ONEDAY_MILLISECONDS                 ' 期限の日数切捨てのミリ秒
        c = DiffDateOfSystemTime(Timestamp(Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT)), T)
        Debug.Print " Diff Date("; c; ","; pt.Y; ") ";
        If c > pt.Y Then
            ' 日数レベルで条件成立
            Call ChangeCodeList(Code.NestList(PARAM_IF_TIME_OVER_TRUE_NEST))
            Debug.Print " : True"
        ElseIf c < pt.Y Then
            ' 日数レベルで条件不成立
            Call ChangeCodeList(Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST))
            Debug.Print " : False"
        Else
            ' 日数レベルでは同日内
            c = DiffTimeOfSystemTime(Timestamp(Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT)), T)
            Debug.Print " Millisecond("; c; ","; pt.X; ") ";
            If c > pt.X Then
                ' ミリ秒レベルで条件成立
                Call ChangeCodeList(Code.NestList(PARAM_IF_TIME_OVER_TRUE_NEST))
                Debug.Print " : True"
            Else
                ' ミリ秒レベルで条件不成立
                Call ChangeCodeList(Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST))
                Debug.Print " : False"
            End If
        End If
        
    Case PCODE_IF_POS_INTO
        Debug.Print "[IF-POS-INTO]";
        Debug.Print " ("; Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG);
        Debug.Print ","; Code.LngData(PARAM_IF_POS_INTO_TOP_LNG);
        Debug.Print ")-("; Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG);
        Debug.Print ","; Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG); ")";
        
        Call MakePosition(pt, Code, PARAM_IF_POS_INTO_BASE_INT, PARAM_IF_POS_INTO_SPOS_INT, _
                            PARAM_IF_POS_INTO_ADDX_LNG, PARAM_IF_POS_INTO_ADDY_LNG)
                            
        Debug.Print " ("; pt.X; ","; pt.Y; ")";
        If pt.X >= Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG) _
                And pt.X <= Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG) _
                And pt.Y >= Code.LngData(PARAM_IF_POS_INTO_TOP_LNG) _
                And pt.Y <= Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG) Then
            Call ChangeCodeList(Code.NestList(PARAM_IF_POS_INTO_TRUE_NEST))
            Debug.Print " : True"
        Else
            Call ChangeCodeList(Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST))
            Debug.Print " : False"
        End If
        
    Case PCODE_CAPTURE
        Call CopyDesktop(picCapture(Code.IntData(PARAM_CAPTURE_CAP_ID_INT)))
        Debug.Print "[CAPTURE] C["; Code.IntData(PARAM_CAPTURE_CAP_ID_INT); "]"
    
    Case PCODE_TIMESTAMP
        Call GetSystemTime(Timestamp(Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT)))
        Debug.Print "[TIMESTAMP] T["; Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT); "]="; MilliTimeOfSystemTime(Timestamp(Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT)))
    
    Case PCODE_LOOP
        Call ChangeCodeList(Code.NestList(PARAM_LOOP_NEST))
        Debug.Print "[LOOP]"
    
    Case PCODE_REPEAT
        Call ChangeCodeList(Code.NestList(PARAM_REPEAT_NEST))
        Code.LngData(PARAM_REPEAT_NOW_LNG) = 0&
        Debug.Print "[REPEAT] "; Code.LngData(PARAM_REPEAT_COUNT_LNG)
    
    Case PCODE_END
        Debug.Print "[END]"
        Call EndProgram
        Exit Sub
    
    Case PCODE_BREAK
        i = Code.IntData(PARAM_BREAK_COUNT_INT)
        Debug.Print "[BREAK] "; i
        Do
            If Stack.IsEmptyStack() Then
                Exit Do
            Else
                Set CurrentList = Stack.Pop()
                Set Code = CurrentList.GetCurrent()
                Select Case Code.CodeID
                Case PCODE_REPEAT, PCODE_LOOP
                    i = i - 1
                    Debug.Print "<BREAK> "; i; " : "; PCODE_NAME(Code.CodeID)
                End Select
            End If
        Loop While i > 0
    
    Case PCODE_WAIT
        Call GetSystemTime(WaitStartTime)
        WaitFlag = True
        If Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT Then
            Debug.Print "[WAIT] Constant, ";
            WaitTime = Code.LngData(PARAM_WAIT_TIME_LNG)
        Else
            Debug.Print "[WAIT] Random, ";
            Debug.Print "{"; Code.LngData(PARAM_WAIT_UPPER_LNG); ","; Code.LngData(PARAM_WAIT_UNDER_LNG); "}";
            WaitTime = (Code.LngData(PARAM_WAIT_UPPER_LNG) - Code.LngData(PARAM_WAIT_UNDER_LNG))
            Debug.Print "("; WaitTime; ") ";
            WaitTime = CLng(Rnd() * CSng(WaitTime))
            Debug.Print "("; WaitTime; ") ";
            WaitTime = WaitTime + Code.LngData(PARAM_WAIT_UNDER_LNG)
        End If
        WaitDay = WaitTime \ ONEDAY_MILLISECONDS
        WaitTime = WaitTime Mod ONEDAY_MILLISECONDS
        Debug.Print WaitDay; ", "; WaitTime
    
    Case PCODE_SET_COLOR
        i = Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT)
        Debug.Print "[SET-COLOR] ";
        Debug.Print SET_COLOR_TYPE_NAME(Code.IntData(PARAM_SET_COLOR_TYPE_INT));
        Select Case Code.IntData(PARAM_SET_COLOR_TYPE_INT)
        Case SET_COLOR_CONSTANT
            Colors(i) = Code.LngData(PARAM_SET_COLOR_COLOR_LNG)
        Case SET_COLOR_COPY
            Debug.Print " SRC Colors("; Code.IntData(PARAM_SET_COLOR_SRC_ID_INT); ")=";
            Debug.Print Right$("00000" & Hex$(Colors(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT))), 6&);
            Colors(i) = Colors(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT))
        Case SET_COLOR_MANUAL_PICK
            StopTimer.Enabled = False
            Set pickF = New ColorPickForm
            pickF.SetMessage "Color ID: " & CStr(i)
            pickF.Show vbModal, Me
            Colors(i) = pickF.PickColor
            Debug.Print " ("; pickF.PositionX; ","; pickF.PositionY; ")";
            StopTimer.Enabled = True
            Unload pickF
            Set pickF = Nothing
        Case SET_COLOR_AUTOMATIC_PICK
            Call MakePosition(pt, Code, PARAM_SET_COLOR_BASE_INT, PARAM_SET_COLOR_SPOS_INT, _
                            PARAM_SET_COLOR_ADDX_LNG, PARAM_SET_COLOR_ADDY_LNG)
            Colors(i) = picCapture(Code.IntData(PARAM_SET_COLOR_CAP_ID_INT)).Point(pt.X, pt.Y)
            Debug.Print " CAP-ID["; Code.IntData(PARAM_SET_COLOR_CAP_ID_INT); "]";
            Debug.Print " "; POS_BASE_NAME(Code.IntData(PARAM_SET_COLOR_BASE_INT));
            Debug.Print " "; Code.IntData(PARAM_SET_COLOR_SPOS_INT);
            Debug.Print " ("; pt.X; ","; pt.Y; ")";
        End Select
        Debug.Print " Colors("; i; ")="; Right$("00000" & Hex$(Colors(i)), 6&)
    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    
    Case Else
        MsgBox "Unsupported Code : " & PCODE_NAME(Code.CodeID), vbCritical Or vbOKOnly, "Error"
        Call EndProgram
        Exit Sub
        
    End Select

End Sub

Private Sub mnuList_Delete_Click()
    Dim Filer   As ProgramListFiler
    Dim res     As VbMsgBoxResult
    Dim p       As String
    
    If viewProgramList.SelectedItem Is Nothing Then
        MsgBox "No selected program", vbExclamation, "Error"
        Exit Sub
    End If
    
    If MsgBox("Delete It?" & vbNewLine & viewProgramList.SelectedItem.Text, vbQuestion Or vbYesNoCancel, "WARNING") <> vbYes Then
        Exit Sub
    End If
    
    p = viewProgramList.SelectedItem.SubItems(1)
    
    res = MsgBox("Delete File?" & vbNewLine & p, vbQuestion Or vbYesNoCancel, "WARNING")
    
    If res = vbCancel Then
        Exit Sub
    End If
    
    If res = vbYes Then
        If FSO.FileExists(p) Then
            FSO.DeleteFile p
        End If
    End If
    
    viewProgramList.ListItems.Remove p
    mProgramList.Remove p
        
    Set Filer = New ProgramListFiler
    Call Filer.SaveProgramList(mProgListPath, mProgramList, mNextID)
    Set Filer = Nothing
    
    If viewProgramList.ListItems.Count = 0 Then
        mnuList.Enabled = False
    End If
    
End Sub

Private Sub mnuMenu_Exit_Click()
    Unload Me
End Sub

Private Sub StopTimer_Timer()
    Static before   As Long
    Dim T   As Long
    
    T = CLng(Int(Timer))
    If T <> before Then
        before = T
        mStartTime = mStartTime - 1&
        lblStopTimeValue.Caption = CStr(mStartTime)
        If mStartTime <= 0& Then
            Call EndProgram
        End If
    End If
    
End Sub

Private Sub viewProgramList_DblClick()
    
    Call LoadProgram
    
End Sub

Private Sub viewProgramList_GotFocus()
    If viewProgramList.ListItems.Count > 0 Then
        mnuList.Enabled = True
    End If
End Sub

Private Sub viewProgramList_LostFocus()
    With mnuList
        If .Enabled Then
            .Enabled = False
        End If
    End With
End Sub
