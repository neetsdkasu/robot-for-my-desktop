VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PCodeList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim mList()     As PCode
Dim mIndex      As Integer
Dim mCount      As Integer
Dim mSize       As Integer

Public Property Get Count() As Integer
    Count = mCount
End Property

Public Function Reset() As PCodeList
    mIndex = 0
    Set Reset = Me
End Function

Public Function HasNext() As Boolean
    HasNext = (mIndex < mCount)
End Function

Public Function GetNext() As PCode
    If HasNext() Then
        Set GetNext = mList(mIndex)
        mIndex = mIndex + 1
    Else
        Set GetNext = Nothing
    End If
End Function

Public Function GetCurrent() As PCode
    If mIndex > 0 Then
        Set GetCurrent = mList(mIndex - 1)
    Else
        Set GetCurrent = Nothing
    End If
End Function

Public Function GetTail() As PCode
    If mCount > 0 Then
        Set GetTail = mList(mCount - 1)
    Else
        Set GetTail = Nothing
    End If
End Function

Public Function Add(Code As PCode) As PCodeList
    Set mList(mCount) = Code
    mCount = mCount + 1
    If mCount > mSize Then
        mSize = mSize + 5
        ReDim Preserve mList(mSize)
    End If
    Set Add = Me
End Function

Private Sub Class_Initialize()
    mIndex = 0
    mCount = 0
    mSize = 5
    ReDim mList(mSize)
End Sub
