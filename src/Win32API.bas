Attribute VB_Name = "Win32API"
Option Explicit

'===========================================================================

'GetLastError
' http://msdn.microsoft.com/ja-jp/library/cc428944.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetLastError Lib "kernel32" () As Long

'===========================================================================

'GetTickCount
' http://msdn.microsoft.com/ja-jp/library/cc429827.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetTickCount Lib "kernel32" () As Long

'===========================================================================

'GetSystemTime
' http://msdn.microsoft.com/ja-jp/library/cc429820.aspx
'  WindowsNT3.5/Windows95
Public Declare Sub GetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

'SYSTEMTIME
' http://msdn.microsoft.com/ja-jp/library/tc6fd5zs.aspx
Type SYSTEMTIME
        wYear As Integer
        wMonth As Integer
        wDayOfWeek As Integer
        wDay As Integer
        wHour As Integer
        wMinute As Integer
        wSecond As Integer
        wMilliseconds As Integer
End Type

'===========================================================================

'timeGetTime
' http://msdn.microsoft.com/ja-jp/library/cc428795.aspx
'  WindowsNT3.1/Windows95
'Public Declare Function timeGetTime Lib "winmm.dll" () As Long

'===========================================================================

'GetDeviceCaps
' http://msdn.microsoft.com/ja-jp/library/cc428670.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long

'nIndex
'  Device Parameters for GetDeviceCaps()
Public Const DRIVERVERSION = 0      '  Device driver version
Public Const TECHNOLOGY = 2         '  Device classification
Public Const HORZSIZE = 4           '  Horizontal size in millimeters
Public Const VERTSIZE = 6           '  Vertical size in millimeters
Public Const HORZRES = 8            '  Horizontal width in pixels
Public Const VERTRES = 10           '  Vertical width in pixels
Public Const BITSPIXEL = 12         '  Number of bits per pixel
Public Const PLANES = 14            '  Number of planes
Public Const NUMBRUSHES = 16        '  Number of brushes the device has
Public Const NUMPENS = 18           '  Number of pens the device has
Public Const NUMMARKERS = 20        '  Number of markers the device has
Public Const NUMFONTS = 22          '  Number of fonts the device has
Public Const NUMCOLORS = 24         '  Number of colors the device supports
Public Const PDEVICESIZE = 26       '  Size required for device descriptor
Public Const CURVECAPS = 28         '  Curve capabilities
Public Const LINECAPS = 30          '  Line capabilities
Public Const POLYGONALCAPS = 32     '  Polygonal capabilities
Public Const TEXTCAPS = 34          '  Text capabilities
Public Const CLIPCAPS = 36          '  Clipping capabilities
Public Const RASTERCAPS = 38        '  Bitblt capabilities
Public Const ASPECTX = 40           '  Length of the X leg
Public Const ASPECTY = 42           '  Length of the Y leg
Public Const ASPECTXY = 44          '  Length of the hypotenuse

'TECHNOLOGY return value flags
'  Device Technologies
Public Const DT_PLOTTER = 0             '  Vector plotter
Public Const DT_RASDISPLAY = 1          '  Raster display
Public Const DT_RASPRINTER = 2          '  Raster printer
Public Const DT_RASCAMERA = 3           '  Raster camera
Public Const DT_CHARSTREAM = 4          '  Character-stream, PLP
Public Const DT_METAFILE = 5            '  Metafile, VDM
Public Const DT_DISPFILE = 6            '  Display-file

'CURVECAPS return value flags
'  Curve Capabilities
Public Const CC_NONE = 0                '  Curves not supported
Public Const CC_CIRCLES = 1             '  Can do circles
Public Const CC_PIE = 2                 '  Can do pie wedges
Public Const CC_CHORD = 4               '  Can do chord arcs
Public Const CC_ELLIPSES = 8            '  Can do ellipese
Public Const CC_WIDE = 16               '  Can do wide lines
Public Const CC_STYLED = 32             '  Can do styled lines
Public Const CC_WIDESTYLED = 64         '  Can do wide styled lines
Public Const CC_INTERIORS = 128 '  Can do interiors
Public Const CC_ROUNDRECT = 256 '

'LINECAPS return value flags
'  Line Capabilities
Public Const LC_NONE = 0                '  Lines not supported
Public Const LC_POLYLINE = 2            '  Can do polylines
Public Const LC_MARKER = 4              '  Can do markers
Public Const LC_POLYMARKER = 8          '  Can do polymarkers
Public Const LC_WIDE = 16               '  Can do wide lines
Public Const LC_STYLED = 32             '  Can do styled lines
Public Const LC_WIDESTYLED = 64         '  Can do wide styled lines
Public Const LC_INTERIORS = 128 '  Can do interiors

'POLYGONALCAPS return value flags
'  Polygonal Capabilities
Public Const PC_NONE = 0                '  Polygonals not supported
Public Const PC_POLYGON = 1             '  Can do polygons
Public Const PC_RECTANGLE = 2           '  Can do rectangles
Public Const PC_WINDPOLYGON = 4         '  Can do winding polygons
Public Const PC_TRAPEZOID = 4           '  Can do trapezoids
Public Const PC_SCANLINE = 8            '  Can do scanlines
Public Const PC_WIDE = 16               '  Can do wide borders
Public Const PC_STYLED = 32             '  Can do styled borders
Public Const PC_WIDESTYLED = 64         '  Can do wide styled borders
Public Const PC_INTERIORS = 128 '  Can do interiors


'TEXTCAPS return value flags
'  Text Capabilities
Public Const TC_OP_CHARACTER = &H1              '  Can do OutputPrecision   CHARACTER
Public Const TC_OP_STROKE = &H2                 '  Can do OutputPrecision   STROKE
Public Const TC_CP_STROKE = &H4                 '  Can do ClipPrecision     STROKE
Public Const TC_CR_90 = &H8                     '  Can do CharRotAbility    90
Public Const TC_CR_ANY = &H10                   '  Can do CharRotAbility    ANY
Public Const TC_SF_X_YINDEP = &H20              '  Can do ScaleFreedom      X_YINDEPENDENT
Public Const TC_SA_DOUBLE = &H40                '  Can do ScaleAbility      DOUBLE
Public Const TC_SA_INTEGER = &H80               '  Can do ScaleAbility      INTEGER
Public Const TC_SA_CONTIN = &H100               '  Can do ScaleAbility      CONTINUOUS
Public Const TC_EA_DOUBLE = &H200               '  Can do EmboldenAbility   DOUBLE
Public Const TC_IA_ABLE = &H400                 '  Can do ItalisizeAbility  ABLE
Public Const TC_UA_ABLE = &H800                 '  Can do UnderlineAbility  ABLE
Public Const TC_SO_ABLE = &H1000                '  Can do StrikeOutAbility  ABLE
Public Const TC_RA_ABLE = &H2000                '  Can do RasterFontAble    ABLE
Public Const TC_VA_ABLE = &H4000                '  Can do VectorFontAble    ABLE
Public Const TC_RESERVED = &H8000
Public Const TC_SCROLLBLT = &H10000             '  do text scroll with blt

'RASTERCAPS return value flags
'  Raster Capabilities
Public Const RC_NONE = 0
Public Const RC_BITBLT = 1                  '  Can do standard BLT.
Public Const RC_BANDING = 2                 '  Device requires banding support
Public Const RC_SCALING = 4                 '  Device requires scaling support
Public Const RC_BITMAP64 = 8                '  Device can support >64K bitmap
Public Const RC_GDI20_OUTPUT = &H10             '  has 2.0 output calls
Public Const RC_GDI20_STATE = &H20
Public Const RC_SAVEBITMAP = &H40
Public Const RC_DI_BITMAP = &H80                '  supports DIB to memory
Public Const RC_PALETTE = &H100                 '  supports a palette
Public Const RC_DIBTODEV = &H200                '  supports DIBitsToDevice
Public Const RC_BIGFONT = &H400                 '  supports >64K fonts
Public Const RC_STRETCHBLT = &H800              '  supports StretchBlt
Public Const RC_FLOODFILL = &H1000              '  supports FloodFill
Public Const RC_STRETCHDIB = &H2000             '  supports StretchDIBits
Public Const RC_OP_DX_OUTPUT = &H4000
Public Const RC_DEVBITS = &H8000

'===========================================================================

'BitBlt
' http://msdn.microsoft.com/ja-jp/library/cc428307.aspx
'  WindowsNT3.1/Windows95
Public Declare Function BitBlt Lib "gdi32" ( _
                            ByVal hDestDC As Long, _
                            ByVal X As Long, _
                            ByVal Y As Long, _
                            ByVal nWidth As Long, _
                            ByVal nHeight As Long, _
                            ByVal hSrcDC As Long, _
                            ByVal xSrc As Long, _
                            ByVal ySrc As Long, _
                            ByVal dwRop As Long) As Long
'dwRop
'  Ternary raster operations
Public Const SRCCOPY = &HCC0020 ' (DWORD) dest = source
Public Const SRCPAINT = &HEE0086        ' (DWORD) dest = source OR dest
Public Const SRCAND = &H8800C6  ' (DWORD) dest = source AND dest
Public Const SRCINVERT = &H660046       ' (DWORD) dest = source XOR dest
Public Const SRCERASE = &H440328        ' (DWORD) dest = source AND (NOT dest )
Public Const NOTSRCCOPY = &H330008      ' (DWORD) dest = (NOT source)
Public Const NOTSRCERASE = &H1100A6     ' (DWORD) dest = (NOT src) AND (NOT dest)
Public Const MERGECOPY = &HC000CA       ' (DWORD) dest = (source AND pattern)
Public Const MERGEPAINT = &HBB0226      ' (DWORD) dest = (NOT source) OR dest
Public Const PATCOPY = &HF00021 ' (DWORD) dest = pattern
Public Const PATPAINT = &HFB0A09        ' (DWORD) dest = DPSnoo
Public Const PATINVERT = &H5A0049       ' (DWORD) dest = pattern XOR dest
Public Const DSTINVERT = &H550009       ' (DWORD) dest = (NOT dest)
Public Const BLACKNESS = &H42 ' (DWORD) dest = BLACK
Public Const WHITENESS = &HFF0062       ' (DWORD) dest = WHITE
