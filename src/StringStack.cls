VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StringStack"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim mStack()    As String
Dim mCount      As Integer
Dim mSize       As Integer

Public Sub RemoveAll(Optional CleanUp As Boolean = True, Optional RenewSize As Integer = -1)
    Dim i   As Integer
           
    If RenewSize > 0 Then
        mSize = IIf(RenewSize > 5, RenewSize, 5)
        ReDim mStack(mSize)
    End If
    
    If CleanUp Then
        For i = 0 To mSize
            mStack(i) = ""
        Next i
    End If
    
    mCount = 0
End Sub

Public Function Exists(Key As String) As Boolean
    Dim i As Integer
    
    For i = mCount - 1 To 0 Step -1
        If mStack(i) = Key Then
            Exists = True
            Exit Function
        End If
    Next i
    
    Exists = False
End Function

Public Function IsEmptyStack() As Boolean
    IsEmptyStack = (mCount = 0)
End Function

Public Sub Push(NewString As String)
    mStack(mCount) = NewString
    mCount = mCount + 1
    If mCount > mSize Then
        mSize = mSize + 5
        ReDim Preserve mStack(mSize)
    End If
End Sub

Public Function Pop() As String
    If IsEmptyStack() Then
        Pop = ""
    Else
        mCount = mCount - 1
        Pop = mStack(mCount)
    End If
End Function

Public Function Peek() As String
    If IsEmptyStack() Then
        Peek = ""
    Else
        Peek = mStack(mCount - 1)
    End If
End Function

Private Sub Class_Initialize()
    mCount = 0
    mSize = 5
    ReDim mStack(mSize)
End Sub

