VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TagWriter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'
' タグを書き込むだけ
'
'

Dim mStack          As New StringStack
Dim mDoc            As TextStream
Dim mIndent         As Long
Dim mOpened         As Integer
Dim mLastTagName    As String

Public Function ApplyEntity(strData As String) As String
    ApplyEntity = Replace(Replace(Replace(Replace(Replace(strData, _
                    "&", "&amp;"), "<", "&lt;"), ">", "&gt;"), """", "&quot;"), "'", "&apos;")
End Function

Public Function SetDocument(doc As TextStream) As TagWriter
    Set mDoc = doc
    mIndent = 0&
    mOpened = 0
    Call mStack.RemoveAll
    Set SetDocument = Me
End Function

Private Sub WriteIndent()
    If mIndent > 0& Then
        Call mDoc.Write(String$(mIndent, vbTab))
    End If
End Sub

Public Function AppendStartTag(TagName As String) As TagWriter
    
    Select Case mOpened
    Case 1
        Call mDoc.WriteLine(">")
    Case 2
        Call mDoc.WriteLine(" />")
    Case 3
        Call mDoc.WriteLine
    End Select
    
    mOpened = 1
    
    Call WriteIndent
    
    Call mDoc.Write("<")
    Call mDoc.Write(TagName)    ' No Check
    
    mIndent = mIndent + 1&
    mLastTagName = TagName
    
    Call mStack.Push(TagName)
    
    Set AppendStartTag = Me
End Function

Public Function CloseAllTags() As TagWriter
    Dim temp        As String
    
    Do While mStack.IsEmptyStack() = False
        temp = mStack.Peek()
        Call AppendCloseTag(temp)
    Loop
    
    Set CloseAllTags = Me
End Function

Public Function AppendCloseTag(Optional TagName As String = "") As TagWriter
    Dim temp    As String
    
    If mStack.IsEmptyStack() Then
        Set AppendCloseTag = Me
        Exit Function
    End If
    
    If TagName = "" Then
        temp = mStack.Peek()
        Set AppendCloseTag = AppendCloseTag(temp)
        Exit Function
    ElseIf mStack.Exists(TagName) Then
        temp = mStack.Peek()
        Do While temp <> TagName
            Call AppendCloseTag(temp)
            temp = mStack.Peek()
        Loop
        Call mStack.Pop
    End If
    
    mIndent = mIndent - 1&
    
    Select Case mOpened
    Case 1
        If mLastTagName = TagName Then
            Call mDoc.Write(">")
        Else
            Call mDoc.WriteLine(">")
            Call WriteIndent
        End If
    Case 2
        Call mDoc.WriteLine(" />")
        Call WriteIndent
    Case 3
    Case Else
        Call WriteIndent
    End Select

    Call mDoc.Write("</")
    Call mDoc.Write(TagName)    ' No Check
    Call mDoc.WriteLine(">")
    
    mOpened = 0
    mLastTagName = ""

    Set AppendCloseTag = Me
End Function

Public Function AppendSimpleTag(TagName As String) As TagWriter
    Select Case mOpened
    Case 1
        Call mDoc.WriteLine(">")
    Case 2
        Call mDoc.WriteLine(" />")
    Case 3
        Call mDoc.WriteLine
    End Select
    
    Call WriteIndent
    
    Call mDoc.Write("<")
    Call mDoc.Write(TagName)    ' No Check
    
    mOpened = 2
    mLastTagName = ""
    
    Set AppendSimpleTag = Me
End Function

Public Function AddParam(ParamName As String, ParamValue As String) As TagWriter
    Select Case mOpened
    Case 1, 2
        Call mDoc.Write(" ")
        Call mDoc.Write(ParamName)  ' No Check
        Call mDoc.Write("=""")
        Call mDoc.Write(ApplyEntity(ParamValue)) ' No Check
        Call mDoc.Write("""")
    End Select
    
    Set AddParam = Me
End Function

Public Function AppendTextNode(Text As String) As TagWriter
    
    Select Case mOpened
    Case 1
        Call mDoc.Write(">")
    Case 2
        Call mDoc.WriteLine(" />")
        Call WriteIndent
    Case 3
    Case Else
        Call WriteIndent
    End Select
    
    Call mDoc.Write(ApplyEntity(Text))   ' No Check
    
    mOpened = 3
    
    Set AppendTextNode = Me
End Function

Public Function AppendComment(Comment As String) As TagWriter

    Select Case mOpened
    Case 1
        Call mDoc.WriteLine(">")
    Case 2
        Call mDoc.WriteLine(" />")
    Case 3
        Call mDoc.WriteLine
    End Select
        
    Call WriteIndent

    Call mDoc.Write("<!-- ")
    Call mDoc.Write(ApplyEntity(Comment))    ' No Check
    Call mDoc.WriteLine(" -->")
    
    mOpened = 0
    mLastTagName = ""

    Set AppendComment = Me
End Function

Public Function AppendSpecialTag(TagName As String, Value As String) As TagWriter

    Select Case mOpened
    Case 1
        Call mDoc.WriteLine(">")
    Case 2
        Call mDoc.WriteLine(" />")
    Case 3
        Call mDoc.WriteLine
    End Select
        
    Call WriteIndent

    Select Case Left$(TagName, 1&)
    Case "!"
        Call mDoc.Write("<" & TagName & " " & Value & ">")  ' No Check
    Case "?"
        Call mDoc.Write("<" & TagName & " " & Value & "?>") ' No Check
    Case Else
        Call mDoc.Write("<!" & TagName & " " & Value & ">") ' No Check
    End Select
        
    mOpened = 0
    mLastTagName = ""

    Set AppendSpecialTag = Me
End Function

