Attribute VB_Name = "Constants"
Option Explicit


Public Const MAX_COUNT_OF_CAPTURE           As Integer = 3
Public Const MAX_COUNT_OF_POSITION          As Integer = 1000
Public Const MAX_COUNT_OF_TIMESTAMP         As Integer = 20
Public Const MAX_COUNT_OF_COLOR             As Integer = 1000

Public Const BREAK_LIMIT                    As Integer = 1000
Public Const WAIT_LIMIT                     As Long = 1000000
Public Const REPEAT_LIMIT                   As Long = 10000&
Public Const TIME_OVER_LIMIT_LIMIT          As Long = 1000000

Public Const BREAK_LOWER                    As Integer = 1
Public Const WAIT_LOWER                     As Long = 0&
Public Const REPEAT_LOWER                   As Long = 1&
Public Const TIME_OVER_LIMIT_LOWER          As Long = 0&

Public Const SET_POS_MANUAL                 As Integer = 0
Public Const SET_POS_AUTOMATIC              As Integer = 1

Public Const SET_COLOR_CONSTANT             As Integer = 0
Public Const SET_COLOR_COPY                 As Integer = 1
Public Const SET_COLOR_MANUAL_PICK          As Integer = 2
Public Const SET_COLOR_AUTOMATIC_PICK       As Integer = 3
Public Const SET_COLOR_COUNT                As Integer = 4

Public Const IF_COLOR_CONSTANT              As Integer = 0
Public Const IF_COLOR_SAVED_COLOR           As Integer = 1
Public Const IF_COLOR_PICK_COLOR            As Integer = 2
Public Const IF_COLOR_COUNT                 As Integer = 3

Public Const WAIT_CONSTANT                  As Integer = 0
Public Const WAIT_RANDOM                    As Integer = 1

Public Const DEFAULT_STOP_TIME              As Long = 300&      ' seconds
Public Const STOP_TIME_LIMIT                As Long = 2592000   ' = 30 * 24 * 60 * 60
Public Const STOP_TIME_LOWER                As Long = 1&

'===========================================================================
'Param Index

Public Const PARAM_SET_POS_TYPE_INT         As Integer = 0
Public Const PARAM_SET_POS_FROM_INT         As Integer = 1
Public Const PARAM_SET_POS_TO_INT           As Integer = 2
Public Const PARAM_SET_POS_BASE_INT         As Integer = 3
Public Const PARAM_SET_POS_SPOS_INT         As Integer = 4
Public Const PARAM_SET_POS__INT             As Integer = 5
Public Const PARAM_SET_POS_ADDX_LNG         As Integer = 0
Public Const PARAM_SET_POS_ADDY_LNG         As Integer = 1
Public Const PARAM_SET_POS__LNG             As Integer = 2
Public Const PARAM_SET_POS__NEST            As Integer = 0

Public Const PARAM_WAIT_TYPE_INT            As Integer = 0
Public Const PARAM_WAIT__INT                As Integer = 1
Public Const PARAM_WAIT_TIME_LNG            As Integer = 0
Public Const PARAM_WAIT_UNDER_LNG           As Integer = 0
Public Const PARAM_WAIT_UPPER_LNG           As Integer = 1
Public Const PARAM_WAIT__LNG                As Integer = 2
Public Const PARAM_WAIT__NEST               As Integer = 0

Public Const PARAM_MOVE_BASE_INT            As Integer = 0
Public Const PARAM_MOVE_SPOS_INT            As Integer = 1
Public Const PARAM_MOVE__INT                As Integer = 2
Public Const PARAM_MOVE_ADDX_LNG            As Integer = 0
Public Const PARAM_MOVE_ADDY_LNG            As Integer = 1
Public Const PARAM_MOVE__LNG                As Integer = 2
Public Const PARAM_MOVE__NEST               As Integer = 0

Public Const PARAM_TIMESTAMP_TIME_ID_INT    As Integer = 0
Public Const PARAM_TIMESTAMP__INT           As Integer = 1
Public Const PARAM_TIMESTAMP__LNG           As Integer = 0
Public Const PARAM_TIMESTAMP__NEST          As Integer = 0

Public Const PARAM_CAPTURE_CAP_ID_INT       As Integer = 0
Public Const PARAM_CAPTURE__INT             As Integer = 1
Public Const PARAM_CAPTURE__LNG             As Integer = 0
Public Const PARAM_CAPTURE__NEST            As Integer = 0

Public Const PARAM_IF_COLOR_COLOR_ID_INT    As Integer = 0
Public Const PARAM_IF_COLOR_TYPE_INT        As Integer = 1
Public Const PARAM_IF_COLOR_SAVED_ID        As Integer = 2
Public Const PARAM_IF_COLOR_CAP_ID_INT      As Integer = 2
Public Const PARAM_IF_COLOR_BASE_INT        As Integer = 3
Public Const PARAM_IF_COLOR_SPOS_INT        As Integer = 4
Public Const PARAM_IF_COLOR__INT            As Integer = 5
Public Const PARAM_IF_COLOR_COLOR_LNG       As Integer = 0
Public Const PARAM_IF_COLOR_ADDX_LNG        As Integer = 1
Public Const PARAM_IF_COLOR_ADDY_LNG        As Integer = 2
Public Const PARAM_IF_COLOR__LNG            As Integer = 3
Public Const PARAM_IF_COLOR_TRUE_NEST       As Integer = 0
Public Const PARAM_IF_COLOR_FALSE_NEST      As Integer = 1
Public Const PARAM_IF_COLOR__NEST           As Integer = 2

Public Const PARAM_LOOP__INT                As Integer = 0
Public Const PARAM_LOOP__LNG                As Integer = 0
Public Const PARAM_LOOP_NEST                As Integer = 0
Public Const PARAM_LOOP__NEST               As Integer = 1

Public Const PARAM_REPEAT__INT              As Integer = 0
Public Const PARAM_REPEAT_COUNT_LNG         As Integer = 0
Public Const PARAM_REPEAT_NOW_LNG           As Integer = 1
Public Const PARAM_REPEAT__LNG              As Integer = 2
Public Const PARAM_REPEAT_NEST              As Integer = 0
Public Const PARAM_REPEAT__NEST             As Integer = 1

Public Const PARAM_BREAK_COUNT_INT          As Integer = 0
Public Const PARAM_BREAK__INT               As Integer = 1
Public Const PARAM_BREAK__LNG               As Integer = 0
Public Const PARAM_BREAK__NEST              As Integer = 0

Public Const PARAM_IF_TIME_OVER_TIME_ID_INT As Integer = 0
Public Const PARAM_IF_TIME_OVER__INT        As Integer = 1
Public Const PARAM_IF_TIME_OVER_LIMIT_LNG   As Integer = 0
Public Const PARAM_IF_TIME_OVER__LNG        As Integer = 1
Public Const PARAM_IF_TIME_OVER_TRUE_NEST   As Integer = 0
Public Const PARAM_IF_TIME_OVER_FALSE_NEST  As Integer = 1
Public Const PARAM_IF_TIME_OVER__NEST       As Integer = 2

Public Const PARAM_IF_POS_INTO_BASE_INT     As Integer = 0
Public Const PARAM_IF_POS_INTO_SPOS_INT     As Integer = 1
Public Const PARAM_IF_POS_INTO__INT         As Integer = 2
Public Const PARAM_IF_POS_INTO_LEFT_LNG     As Integer = 0
Public Const PARAM_IF_POS_INTO_TOP_LNG      As Integer = 1
Public Const PARAM_IF_POS_INTO_RIGHT_LNG    As Integer = 2
Public Const PARAM_IF_POS_INTO_BOTTOM_LNG   As Integer = 3
Public Const PARAM_IF_POS_INTO_ADDX_LNG     As Integer = 4
Public Const PARAM_IF_POS_INTO_ADDY_LNG     As Integer = 5
Public Const PARAM_IF_POS_INTO__LNG         As Integer = 6
Public Const PARAM_IF_POS_INTO_TRUE_NEST    As Integer = 0
Public Const PARAM_IF_POS_INTO_FALSE_NEST   As Integer = 1
Public Const PARAM_IF_POS_INTO__NEST        As Integer = 2

Public Const PARAM_SET_COLOR_TYPE_INT       As Integer = 0
Public Const PARAM_SET_COLOR_COLOR_ID_INT   As Integer = 1
Public Const PARAM_SET_COLOR_SRC_ID_INT     As Integer = 2
Public Const PARAM_SET_COLOR_CAP_ID_INT     As Integer = 2
Public Const PARAM_SET_COLOR_BASE_INT       As Integer = 3
Public Const PARAM_SET_COLOR_SPOS_INT       As Integer = 4
Public Const PARAM_SET_COLOR__INT           As Integer = 5
Public Const PARAM_SET_COLOR_COLOR_LNG      As Integer = 0
Public Const PARAM_SET_COLOR_ADDX_LNG       As Integer = 0
Public Const PARAM_SET_COLOR_ADDY_LNG       As Integer = 1
Public Const PARAM_SET_COLOR__LNG           As Integer = 2
Public Const PARAM_SET_COLOR__NEST          As Integer = 0

' Position of [NEW-CODE]
' Public Const PARAM_NEW_CODE__INT            As Integer = 0
' Public Const PARAM_NEW_CODE__LNG            As Integer = 0
' Public Const PARAM_NEW_CODE__NEST           As Integer = 0

'===========================================================================
'Program Code

Public Const PCODE_COUNT = 21

Public Const PCODE_MOUSE_LEFT_DOWN      As Integer = 0
Public Const PCODE_MOUSE_LEFT_UP        As Integer = 1
Public Const PCODE_MOUSE_LEFT_CLICK     As Integer = 2
Public Const PCODE_MOUSE_LEFT_DBLCLICK  As Integer = 3

Public Const PCODE_MOUSE_RIGHT_DOWN     As Integer = 4
Public Const PCODE_MOUSE_RIGHT_UP       As Integer = 5
Public Const PCODE_MOUSE_RIGHT_CLICK    As Integer = 6
Public Const PCODE_MOUSE_RIGHT_DBLCLICK As Integer = 7

Public Const PCODE_MOUSE_MOVE           As Integer = 8

Public Const PCODE_WAIT                 As Integer = 9

Public Const PCODE_CAPTURE              As Integer = 10
Public Const PCODE_SET_POS              As Integer = 11
Public Const PCODE_TIMESTAMP            As Integer = 12

Public Const PCODE_IF_COLOR             As Integer = 13
Public Const PCODE_IF_TIME_OVER         As Integer = 14
Public Const PCODE_IF_POS_INTO          As Integer = 15

Public Const PCODE_REPEAT               As Integer = 16
Public Const PCODE_LOOP                 As Integer = 17
Public Const PCODE_BREAK                As Integer = 18
Public Const PCODE_END                  As Integer = 19

Public Const PCODE_SET_COLOR            As Integer = 20

' Position of [NEW-CODE]
' Public Const PCODE_NEW_CODE             As Integer = -1

'===========================================================================
'Base Position

Public Const POS_BASE_COUNT = 6

Public Const POS_BASE_SCREEN_TOP_LEFT       As Integer = 0
Public Const POS_BASE_SCREEN_TOP_RIGHT      As Integer = 1
Public Const POS_BASE_SCREEN_BOTTOM_LEFT    As Integer = 2
Public Const POS_BASE_SCREEN_BOTTOM_RIGHT   As Integer = 3
Public Const POS_BASE_MOUSE_POINTER         As Integer = 4
Public Const POS_BASE_SAVED_POSITION        As Integer = 5

'===========================================================================

Public Property Get PCODE_NAME(id As Integer) As String

    Select Case id
    
    Case PCODE_MOUSE_LEFT_DOWN
        PCODE_NAME = "mouse-left-down"
    Case PCODE_MOUSE_LEFT_UP
        PCODE_NAME = "mouse-left-up"
    Case PCODE_MOUSE_LEFT_CLICK
        PCODE_NAME = "mouse-left-click"
    Case PCODE_MOUSE_LEFT_DBLCLICK
        PCODE_NAME = "mouse-left-dblclick"
    
    Case PCODE_MOUSE_RIGHT_DOWN
        PCODE_NAME = "mouse-right-down"
    Case PCODE_MOUSE_RIGHT_UP
        PCODE_NAME = "mouse-right-up"
    Case PCODE_MOUSE_RIGHT_CLICK
        PCODE_NAME = "mouse-right-click"
    Case PCODE_MOUSE_RIGHT_DBLCLICK
        PCODE_NAME = "mouse-right-dblclick"
    
    Case PCODE_MOUSE_MOVE
        PCODE_NAME = "mouse-move"
    
    Case PCODE_WAIT
        PCODE_NAME = "wait"
    
    Case PCODE_CAPTURE
        PCODE_NAME = "capture"
    Case PCODE_TIMESTAMP
        PCODE_NAME = "timestamp"
    Case PCODE_SET_POS
        PCODE_NAME = "set-pos"
    
    Case PCODE_IF_COLOR
        PCODE_NAME = "if-color"
    Case PCODE_IF_TIME_OVER
        PCODE_NAME = "if-time-over"
    Case PCODE_IF_POS_INTO
        PCODE_NAME = "if-pos-into"
    
    Case PCODE_REPEAT
        PCODE_NAME = "repeat"
    Case PCODE_LOOP
        PCODE_NAME = "loop"
    Case PCODE_BREAK
        PCODE_NAME = "break"
        
    Case PCODE_END
        PCODE_NAME = "end"
    
    Case PCODE_SET_COLOR
        PCODE_NAME = "set-color"
    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    '     PCODE_NAME = "new-code"
    
    Case Else
        PCODE_NAME = "unknown-program-code-" & CStr(id)
    End Select
    
End Property

'===========================================================================

Public Property Get POS_BASE_NAME(id As Integer) As String
    Select Case id
    Case POS_BASE_SCREEN_TOP_LEFT
        POS_BASE_NAME = "Screen-Top-Left"
    Case POS_BASE_SCREEN_TOP_RIGHT
        POS_BASE_NAME = "Screen-Top-Right"
    Case POS_BASE_SCREEN_BOTTOM_LEFT
        POS_BASE_NAME = "Screen-Bottom-Left"
    Case POS_BASE_SCREEN_BOTTOM_RIGHT
        POS_BASE_NAME = "Screen-Bottom-Right"
    Case POS_BASE_MOUSE_POINTER
        POS_BASE_NAME = "Mouse-Pointer"
    Case POS_BASE_SAVED_POSITION
        POS_BASE_NAME = "Saved-Position"
    Case Else
        POS_BASE_NAME = "Unknown-Base-Position-" & CStr(id)
    End Select
End Property

'===========================================================================

Public Property Get SET_POS_TYPE_NAME(id As Integer) As String
    Select Case id
    Case SET_POS_AUTOMATIC
        SET_POS_TYPE_NAME = "Automatic"
    Case SET_POS_MANUAL
        SET_POS_TYPE_NAME = "Manual"
    Case Else
        SET_POS_TYPE_NAME = "Unknown-Set-Pos-Type-" & CStr(id)
    End Select
End Property

'===========================================================================

Public Property Get SET_COLOR_TYPE_NAME(id As Integer) As String
    Select Case id
    Case SET_COLOR_CONSTANT
        SET_COLOR_TYPE_NAME = "Constant"
    Case SET_COLOR_COPY
        SET_COLOR_TYPE_NAME = "Copy"
    Case SET_COLOR_AUTOMATIC_PICK
        SET_COLOR_TYPE_NAME = "Automatic-Pick"
    Case SET_COLOR_MANUAL_PICK
        SET_COLOR_TYPE_NAME = "Manual-Pick"
    Case Else
        SET_COLOR_TYPE_NAME = "Unknown-Set-Color-Type-" & CStr(id)
    End Select
End Property

'===========================================================================

Public Property Get IF_COLOR_TYPE_NAME(id As Integer) As String
    Select Case id
    Case IF_COLOR_CONSTANT
        IF_COLOR_TYPE_NAME = "Constant"
    Case IF_COLOR_SAVED_COLOR
        IF_COLOR_TYPE_NAME = "Saved-Color"
    Case IF_COLOR_PICK_COLOR
        IF_COLOR_TYPE_NAME = "Pick-Color"
    Case Else
        IF_COLOR_TYPE_NAME = "Unknown-If-Color-Type-" & CStr(id)
    End Select
End Property

'===========================================================================

Public Property Get WAIT_TYPE_NAME(id As Integer) As String
    Select Case id
    Case WAIT_CONSTANT
        WAIT_TYPE_NAME = "Constant"
    Case WAIT_RANDOM
        WAIT_TYPE_NAME = "Random"
    Case Else
        WAIT_TYPE_NAME = "Unknown-Wait-Type-" & CStr(id)
    End Select
End Property
