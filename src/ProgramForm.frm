VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form ProgramForm 
   BorderStyle     =   1  '�Œ�(����)
   Caption         =   "Program Editor"
   ClientHeight    =   8040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   536
   ScaleMode       =   3  '�߸��
   ScaleWidth      =   794
   StartUpPosition =   2  '��ʂ̒���
   Begin VB.TextBox txtStopTime 
      Alignment       =   1  '�E����
      Height          =   375
      IMEMode         =   3  '�̌Œ�
      Left            =   5640
      MaxLength       =   5
      TabIndex        =   69
      Text            =   "300"
      Top             =   120
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   8280
      TabIndex        =   20
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   9600
      TabIndex        =   19
      Top             =   120
      Width           =   2055
   End
   Begin VB.TextBox txtProgramTitle 
      Height          =   375
      Left            =   600
      MaxLength       =   50
      TabIndex        =   9
      Text            =   "Non Title"
      Top             =   120
      Width           =   3735
   End
   Begin MSComDlg.CommonDialog dlgColorPicker 
      Left            =   7320
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CommandButton cmdDeleteCode 
      Caption         =   "Delete"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton cmdAddCode 
      Caption         =   "Add"
      Default         =   -1  'True
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   720
      Width           =   2415
   End
   Begin VB.Frame fraCode 
      Caption         =   "Code"
      Height          =   6735
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Width           =   3615
      Begin VB.Frame fraParamIfColor 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   103
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.OptionButton optIfColorType 
            Caption         =   "Pick Color"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   124
            Top             =   2520
            Width           =   1575
         End
         Begin VB.OptionButton optIfColorType 
            Caption         =   "Saved Color"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   123
            Top             =   1560
            Width           =   1575
         End
         Begin VB.OptionButton optIfColorType 
            Caption         =   "Constant"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   122
            Top             =   720
            Value           =   -1  'True
            Width           =   1575
         End
         Begin VB.CommandButton cmdIfColorPick 
            Caption         =   "Pick"
            Enabled         =   0   'False
            Height          =   255
            Left            =   1680
            TabIndex        =   113
            Top             =   1320
            Width           =   735
         End
         Begin VB.CommandButton cmdIfColorSelect 
            Caption         =   "Select"
            Enabled         =   0   'False
            Height          =   255
            Left            =   2520
            TabIndex        =   112
            Top             =   1320
            Width           =   735
         End
         Begin VB.TextBox txtIfColorPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   111
            Text            =   "0"
            Top             =   3720
            Width           =   1575
         End
         Begin VB.ComboBox cmbIfColorBase 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0000
            Left            =   1680
            List            =   "ProgramForm.frx":0002
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   110
            Top             =   3360
            Width           =   1575
         End
         Begin VB.TextBox txtIfColorColor 
            Alignment       =   2  '��������
            Enabled         =   0   'False
            Height          =   270
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   109
            Text            =   "Text1"
            Top             =   960
            Width           =   1575
         End
         Begin VB.ComboBox cmbIfColorCapID 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0004
            Left            =   1680
            List            =   "ProgramForm.frx":0006
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   108
            Top             =   3000
            Width           =   1575
         End
         Begin VB.TextBox txtIfColorAddY 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   107
            Text            =   "0"
            Top             =   4440
            Width           =   1575
         End
         Begin VB.TextBox txtIfColorAddX 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   106
            Text            =   "0"
            Top             =   4080
            Width           =   1575
         End
         Begin VB.TextBox txtIfColorColorID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   105
            Text            =   "0"
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox txtIfColorSavedColorID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   104
            Text            =   "0"
            Top             =   1920
            Width           =   1575
         End
         Begin VB.Label lblIfColorPosID 
            AutoSize        =   -1  'True
            Caption         =   "Base Position ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   121
            Top             =   3720
            Width           =   1320
         End
         Begin VB.Label lblIfColorBase 
            AutoSize        =   -1  'True
            Caption         =   "Base:"
            Height          =   180
            Left            =   240
            TabIndex        =   120
            Top             =   3360
            Width           =   420
         End
         Begin VB.Label lblIfColorColor 
            AutoSize        =   -1  'True
            Caption         =   "Color:"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   119
            Top             =   960
            Width           =   435
         End
         Begin VB.Label lblIfColorCapID 
            AutoSize        =   -1  'True
            Caption         =   "Capture ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   118
            Top             =   3000
            Width           =   855
         End
         Begin VB.Label lblIfColorAddY 
            AutoSize        =   -1  'True
            Caption         =   "Additional Y:"
            Height          =   180
            Left            =   240
            TabIndex        =   117
            Top             =   4440
            Width           =   960
         End
         Begin VB.Label lblIfColorAddX 
            AutoSize        =   -1  'True
            Caption         =   "Additional X:"
            Height          =   180
            Left            =   240
            TabIndex        =   116
            Top             =   4080
            Width           =   960
         End
         Begin VB.Label lblIfColorColorID 
            AutoSize        =   -1  'True
            Caption         =   "Color ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   115
            Top             =   360
            Width           =   660
         End
         Begin VB.Label lblIfColorSavedColorID 
            AutoSize        =   -1  'True
            Caption         =   "Saved Color ID:"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   114
            Top             =   1920
            Width           =   1185
         End
      End
      Begin VB.ListBox lstCodeList 
         Height          =   1500
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   240
         Width           =   3375
      End
      Begin VB.Frame fraParamSetColor 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   71
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtSetColorSrcColorID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   81
            Text            =   "0"
            Top             =   1920
            Width           =   1575
         End
         Begin VB.OptionButton optSetColorType 
            Caption         =   "Automatic Pick"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   83
            Top             =   2640
            Width           =   1815
         End
         Begin VB.TextBox txtSetColorColorID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   73
            Text            =   "0"
            Top             =   360
            Width           =   1575
         End
         Begin VB.OptionButton optSetColorType 
            Caption         =   "Manual Pick"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   82
            Top             =   2280
            Width           =   1815
         End
         Begin VB.OptionButton optSetColorType 
            Caption         =   "Copy"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   79
            Top             =   1560
            Width           =   1455
         End
         Begin VB.OptionButton optSetColorType 
            Caption         =   "Constant"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   74
            Top             =   720
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.TextBox txtSetColorAddX 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   91
            Text            =   "0"
            Top             =   4080
            Width           =   1575
         End
         Begin VB.TextBox txtSetColorAddY 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   93
            Text            =   "0"
            Top             =   4440
            Width           =   1575
         End
         Begin VB.ComboBox cmbSetColorCapID 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0008
            Left            =   1680
            List            =   "ProgramForm.frx":000A
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   85
            Top             =   3000
            Width           =   1575
         End
         Begin VB.TextBox txtSetColorColor 
            Alignment       =   2  '��������
            Enabled         =   0   'False
            Height          =   270
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   76
            Text            =   "Text1"
            Top             =   960
            Width           =   1575
         End
         Begin VB.ComboBox cmbSetColorBase 
            Height          =   300
            ItemData        =   "ProgramForm.frx":000C
            Left            =   1680
            List            =   "ProgramForm.frx":000E
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   87
            Top             =   3360
            Width           =   1575
         End
         Begin VB.TextBox txtSetColorPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   89
            Text            =   "0"
            Top             =   3720
            Width           =   1575
         End
         Begin VB.CommandButton cmdSetColorSelect 
            Caption         =   "Select"
            Enabled         =   0   'False
            Height          =   255
            Left            =   2520
            TabIndex        =   78
            Top             =   1320
            Width           =   735
         End
         Begin VB.CommandButton cmdSetColorPick 
            Caption         =   "Pick"
            Enabled         =   0   'False
            Height          =   255
            Left            =   1680
            TabIndex        =   77
            Top             =   1320
            Width           =   735
         End
         Begin VB.Label lblSetColorSrcColorID 
            AutoSize        =   -1  'True
            Caption         =   "Source Color ID:"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   80
            Top             =   1920
            Width           =   1245
         End
         Begin VB.Label lblSetColorColorID 
            AutoSize        =   -1  'True
            Caption         =   "Color ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   72
            Top             =   360
            Width           =   660
         End
         Begin VB.Label lblSetColorAddX 
            AutoSize        =   -1  'True
            Caption         =   "Additional X:"
            Height          =   180
            Left            =   240
            TabIndex        =   90
            Top             =   4080
            Width           =   960
         End
         Begin VB.Label lblSetColorAddY 
            AutoSize        =   -1  'True
            Caption         =   "Additional Y:"
            Height          =   180
            Left            =   240
            TabIndex        =   92
            Top             =   4440
            Width           =   960
         End
         Begin VB.Label lblSetColorCapID 
            AutoSize        =   -1  'True
            Caption         =   "Capture ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   84
            Top             =   3000
            Width           =   855
         End
         Begin VB.Label lblSetColorColor 
            AutoSize        =   -1  'True
            Caption         =   "Color:"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   75
            Top             =   960
            Width           =   435
         End
         Begin VB.Label lblSetColorBase 
            AutoSize        =   -1  'True
            Caption         =   "Base:"
            Height          =   180
            Left            =   240
            TabIndex        =   86
            Top             =   3360
            Width           =   420
         End
         Begin VB.Label lblSetColorPosID 
            AutoSize        =   -1  'True
            Caption         =   "Base Position ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   88
            Top             =   3720
            Width           =   1320
         End
      End
      Begin VB.Frame fraParamSetPos 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   26
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtSetPosToPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   40
            Text            =   "0"
            Top             =   600
            Width           =   1575
         End
         Begin VB.TextBox txtSetPosFromPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   39
            Text            =   "0"
            Top             =   240
            Width           =   1575
         End
         Begin VB.OptionButton optSetPosType 
            Caption         =   "Automatic"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   37
            Top             =   1200
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.TextBox txtSetPosPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   3
            TabIndex        =   30
            Text            =   "0"
            Top             =   1920
            Width           =   1575
         End
         Begin VB.ComboBox cmbSetPosBase 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0010
            Left            =   1680
            List            =   "ProgramForm.frx":0012
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   29
            Top             =   1560
            Width           =   1575
         End
         Begin VB.TextBox txtSetPosAddY 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   28
            Text            =   "0"
            Top             =   2640
            Width           =   1575
         End
         Begin VB.TextBox txtSetPosAddX 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   27
            Text            =   "0"
            Top             =   2280
            Width           =   1575
         End
         Begin VB.OptionButton optSetPosType 
            Caption         =   "Manual"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   36
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblSetPosToPosID 
            AutoSize        =   -1  'True
            Caption         =   "to"
            Height          =   180
            Left            =   1200
            TabIndex        =   42
            Top             =   600
            Width           =   150
         End
         Begin VB.Label lblSetPosFromPosID 
            AutoSize        =   -1  'True
            Caption         =   "from"
            Height          =   180
            Left            =   1200
            TabIndex        =   41
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblSetPosPosIDs 
            AutoSize        =   -1  'True
            Caption         =   "Position ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   38
            Top             =   240
            Width           =   870
         End
         Begin VB.Label lblSetPosPosID 
            AutoSize        =   -1  'True
            Caption         =   "Base Position ID:"
            Height          =   180
            Left            =   240
            TabIndex        =   34
            Top             =   1920
            Width           =   1320
         End
         Begin VB.Label lblSetPosBase 
            AutoSize        =   -1  'True
            Caption         =   "Base:"
            Height          =   180
            Left            =   240
            TabIndex        =   33
            Top             =   1560
            Width           =   420
         End
         Begin VB.Label lblSetPosAddY 
            AutoSize        =   -1  'True
            Caption         =   "Additional Y:"
            Height          =   180
            Left            =   240
            TabIndex        =   32
            Top             =   2640
            Width           =   960
         End
         Begin VB.Label lblSetPosAddX 
            AutoSize        =   -1  'True
            Caption         =   "Additional X:"
            Height          =   180
            Left            =   240
            TabIndex        =   31
            Top             =   2280
            Width           =   960
         End
      End
      Begin VB.Frame fraParamIfPosInto 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   43
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.CommandButton cmdIfPosIntoRectPick 
            Caption         =   "Pick"
            Height          =   255
            Left            =   1560
            TabIndex        =   70
            Top             =   1800
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoLeft 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   55
            Text            =   "0"
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoTop 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   54
            Text            =   "0"
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoRight 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   53
            Text            =   "0"
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoBottom 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   52
            Text            =   "0"
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoPosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   3
            TabIndex        =   47
            Text            =   "0"
            Top             =   2520
            Width           =   1575
         End
         Begin VB.ComboBox cmbIfPosIntoBase 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0014
            Left            =   1560
            List            =   "ProgramForm.frx":0016
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   46
            Top             =   2160
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoAddY 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   45
            Text            =   "0"
            Top             =   3240
            Width           =   1575
         End
         Begin VB.TextBox txtIfPosIntoAddX 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   44
            Text            =   "0"
            Top             =   2880
            Width           =   1575
         End
         Begin VB.Label lblIfPosIntoLeft 
            AutoSize        =   -1  'True
            Caption         =   "Left:"
            Height          =   180
            Left            =   120
            TabIndex        =   59
            Top             =   360
            Width           =   330
         End
         Begin VB.Label lblIfPosIntoTop 
            AutoSize        =   -1  'True
            Caption         =   "Top:"
            Height          =   180
            Left            =   120
            TabIndex        =   58
            Top             =   720
            Width           =   315
         End
         Begin VB.Label lblIfPosIntoRight 
            AutoSize        =   -1  'True
            Caption         =   "Right:"
            Height          =   180
            Left            =   120
            TabIndex        =   57
            Top             =   1080
            Width           =   435
         End
         Begin VB.Label lblIfPosIntoBottom 
            AutoSize        =   -1  'True
            Caption         =   "Bottom:"
            Height          =   180
            Left            =   120
            TabIndex        =   56
            Top             =   1440
            Width           =   585
         End
         Begin VB.Label lblIfPosIntoPosID 
            AutoSize        =   -1  'True
            Caption         =   "Base Position ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   51
            Top             =   2520
            Width           =   1320
         End
         Begin VB.Label lblIfPosIntoBase 
            AutoSize        =   -1  'True
            Caption         =   "Base:"
            Height          =   180
            Left            =   120
            TabIndex        =   50
            Top             =   2160
            Width           =   420
         End
         Begin VB.Label lblIfPosIntoAddY 
            AutoSize        =   -1  'True
            Caption         =   "Additional Y:"
            Height          =   180
            Left            =   120
            TabIndex        =   49
            Top             =   3240
            Width           =   960
         End
         Begin VB.Label lblIfPosIntoAddX 
            AutoSize        =   -1  'True
            Caption         =   "Additional X:"
            Height          =   180
            Left            =   120
            TabIndex        =   48
            Top             =   2880
            Width           =   960
         End
      End
      Begin VB.Frame fraParamMove 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   10
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtMoveAddX 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   16
            Text            =   "0"
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtMoveAddY 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   18
            Text            =   "0"
            Top             =   1440
            Width           =   1575
         End
         Begin VB.ComboBox cmbMoveBase 
            Height          =   300
            ItemData        =   "ProgramForm.frx":0018
            Left            =   1560
            List            =   "ProgramForm.frx":001A
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   12
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox txtMovePosID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            MaxLength       =   3
            TabIndex        =   14
            Text            =   "0"
            Top             =   720
            Width           =   1575
         End
         Begin VB.Label lblMoveAddX 
            AutoSize        =   -1  'True
            Caption         =   "Additional X:"
            Height          =   180
            Left            =   120
            TabIndex        =   15
            Top             =   1080
            Width           =   960
         End
         Begin VB.Label lblMoveAddY 
            AutoSize        =   -1  'True
            Caption         =   "Additional Y:"
            Height          =   180
            Left            =   120
            TabIndex        =   17
            Top             =   1440
            Width           =   960
         End
         Begin VB.Label lblMoveBase 
            AutoSize        =   -1  'True
            Caption         =   "Base:"
            Height          =   180
            Left            =   120
            TabIndex        =   11
            Top             =   360
            Width           =   420
         End
         Begin VB.Label lblMovePosID 
            AutoSize        =   -1  'True
            Caption         =   "Base Position ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   13
            Top             =   720
            Width           =   1320
         End
      End
      Begin VB.Frame fraParamIfTimeOver 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   60
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtIfTimeOverLimit 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   63
            Text            =   "100"
            Top             =   960
            Width           =   1575
         End
         Begin VB.TextBox txtIfTimeOverTimeID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   61
            Text            =   "0"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblIfTimeOverLimit 
            AutoSize        =   -1  'True
            Caption         =   "Differemce Time Limit (x100msec):"
            Height          =   180
            Left            =   120
            TabIndex        =   64
            Top             =   720
            Width           =   2685
         End
         Begin VB.Label lblIfTimeOverTimeID 
            AutoSize        =   -1  'True
            Caption         =   "Timestamp ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   62
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame fraParamTimestamp 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   65
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtTimestampTimeID 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   66
            Text            =   "0"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblTimestampTimeID 
            AutoSize        =   -1  'True
            Caption         =   "Timestamp ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   67
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame fraParamCapture 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   24
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.ComboBox cmbCaptureCapID 
            Height          =   300
            ItemData        =   "ProgramForm.frx":001C
            Left            =   1560
            List            =   "ProgramForm.frx":001E
            Style           =   2  '��ۯ���޳� ؽ�
            TabIndex        =   35
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblCaptureCapID 
            AutoSize        =   -1  'True
            Caption         =   "Capture ID:"
            Height          =   180
            Left            =   120
            TabIndex        =   25
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.Frame fraParamBreak 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   21
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtBreakCount 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   22
            Text            =   "1"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblBreakCount 
            AutoSize        =   -1  'True
            Caption         =   "Break Count:"
            Height          =   180
            Left            =   120
            TabIndex        =   23
            Top             =   240
            Width           =   990
         End
      End
      Begin VB.Frame fraParamRepeat 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   5
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtRepeatCount 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   7
            Text            =   "10"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblRepeatCount 
            AutoSize        =   -1  'True
            Caption         =   "Repeat Count:"
            Height          =   180
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Width           =   1080
         End
      End
      Begin VB.Frame fraParamWait 
         Caption         =   "Param"
         Enabled         =   0   'False
         Height          =   4815
         Left            =   120
         TabIndex        =   94
         Top             =   1800
         Visible         =   0   'False
         Width           =   3375
         Begin VB.TextBox txtWaitConstant 
            Alignment       =   1  '�E����
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   99
            Text            =   "10"
            Top             =   960
            Width           =   1575
         End
         Begin VB.OptionButton optWaitType 
            Caption         =   "Constant"
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   98
            Top             =   240
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton optWaitType 
            Caption         =   "Random"
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   97
            Top             =   1320
            Width           =   1455
         End
         Begin VB.TextBox txtWaitUpper 
            Alignment       =   1  '�E����
            Enabled         =   0   'False
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   96
            Text            =   "20"
            Top             =   2040
            Width           =   1575
         End
         Begin VB.TextBox txtWaitUnder 
            Alignment       =   1  '�E����
            Enabled         =   0   'False
            Height          =   270
            IMEMode         =   3  '�̌Œ�
            Left            =   1560
            TabIndex        =   95
            Text            =   "10"
            Top             =   2640
            Width           =   1575
         End
         Begin VB.Label lblWaitConstant 
            AutoSize        =   -1  'True
            Caption         =   "Wait Tiime (x100msec):"
            Height          =   180
            Left            =   240
            TabIndex        =   102
            Top             =   720
            Width           =   1785
         End
         Begin VB.Label lblWaitUpper 
            AutoSize        =   -1  'True
            Caption         =   "Wait Tiime Upper  (x100msec):"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   101
            Top             =   1800
            Width           =   2355
         End
         Begin VB.Label lblWaitUnder 
            AutoSize        =   -1  'True
            Caption         =   "Wait Tiime Under (x100msec):"
            Enabled         =   0   'False
            Height          =   180
            Left            =   240
            TabIndex        =   100
            Top             =   2400
            Width           =   2295
         End
      End
   End
   Begin MSComctlLib.TreeView treeCodeView 
      Height          =   7215
      Left            =   3840
      TabIndex        =   0
      Top             =   720
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   12726
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   6
      SingleSel       =   -1  'True
      Appearance      =   1
   End
   Begin VB.Label lblStopTime 
      AutoSize        =   -1  'True
      Caption         =   "StopTime(sec):"
      Height          =   180
      Left            =   4440
      TabIndex        =   68
      Top             =   120
      Width           =   1140
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000005&
      X1              =   8
      X2              =   784
      Y1              =   41
      Y2              =   41
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   8
      X2              =   784
      Y1              =   40
      Y2              =   40
   End
   Begin VB.Label lblProgramTitle 
      AutoSize        =   -1  'True
      Caption         =   "Title:"
      Height          =   180
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   375
   End
End
Attribute VB_Name = "ProgramForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'#Const DEBUG_MODE = 1

Private Const NEST_FLAG         As String = "NEST"
Private Const TRUE_KEY          As String = "-true"
Private Const FALSE_KEY         As String = "-false"
Private Const NEST_KEY          As String = "-nest"

Dim fraParam()                  As Frame
Dim optSetPosTypeAutomatic      As OptionButton
Dim optSetPosTypeManual         As OptionButton
Dim optSetColorTypeAutomatic    As OptionButton
Dim optSetColorTypeManual       As OptionButton
Dim optSetColorTypeConstant     As OptionButton
Dim optSetColorTypeCopy         As OptionButton
Dim optIfColorTypeConstant      As OptionButton
Dim optIfColorTypeSaved         As OptionButton
Dim optIfColorTypePick          As OptionButton
Dim optWaitTypeConstant         As OptionButton
Dim optWaitTypeRandom           As OptionButton
Dim nodeCount                   As Integer
Dim dicPCode                    As New Dictionary
Dim mProgramInfo
Dim UpdateTarget                As PCode
Dim UpdateKey                   As String
Dim UpdateFlag                  As Boolean

Public Sub SetProgramInfo(aProgramInfo As ProgramInfo)
    Set mProgramInfo = aProgramInfo
End Sub

Private Function GetPickColor() As Long
    Dim picker As New ColorPickForm
        
    picker.SetMessage "Pick a Color on Display"
    picker.Show vbModal, Me
    
    GetPickColor = picker.PickColor
    
    Unload picker
    Set picker = Nothing
    
End Function

Private Sub SelectColor(txtColor As TextBox)
    On Error GoTo Error_SelectColor
    
    With dlgColorPicker
        .Color = txtColor.BackColor
        Call .ShowColor
        Call ShowPickedColor(txtColor, .Color)
    End With
    
    Exit Sub
Error_SelectColor:
End Sub

Private Sub ExpandProgram(CodeList As PCodeList)
    Dim lStack      As ListStack
    Dim sStack      As StringStack
    Dim Cur         As PCodeList
    Dim Key         As String
    Dim Code        As PCode
    Dim Flag        As Integer
    Dim n           As Node
    
    Debug.Assert Not CodeList Is Nothing
    
    Set lStack = New ListStack
    Set sStack = New StringStack
    Set Cur = CodeList
    
    Do
        If Cur.HasNext() Then
            Set Code = Cur.GetNext()
            Key = AddCode(Code)
            Debug.Assert Key <> ""
            Select Case Code.CodeID
            Case PCODE_REPEAT
                Call lStack.Push(Cur)
                Set Cur = Code.NestList(PARAM_REPEAT_NEST)
            Case PCODE_LOOP
                Call lStack.Push(Cur)
                Set Cur = Code.NestList(PARAM_LOOP_NEST)
            Case PCODE_IF_COLOR
                Call lStack.Push(Cur)
                Set Cur = Code.NestList(PARAM_IF_COLOR_TRUE_NEST)
                If Cur Is Nothing Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_COLOR_FALSE_NEST)
                ElseIf Cur.Count = 0 Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_COLOR_FALSE_NEST)
                End If
            Case PCODE_IF_POS_INTO
                Call lStack.Push(Cur)
                Set Cur = Code.NestList(PARAM_IF_POS_INTO_TRUE_NEST)
                If Cur Is Nothing Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST)
                ElseIf Cur.Count = 0 Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST)
                End If
            Case PCODE_IF_TIME_OVER
                Call lStack.Push(Cur)
                Set Cur = Code.NestList(PARAM_IF_TIME_OVER_TRUE_NEST)
                If Cur Is Nothing Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST)
                ElseIf Cur.Count = 0 Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Cur = Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST)
                End If
            
            ' Position of [NEW-CODE] with NEST
            ' Case PCODE_NEW_CODE
            
            End Select
            If Cur Is Nothing Then
                Set Cur = lStack.Pop()
                treeCodeView.SelectedItem.Parent.Selected = True
            End If
        Else
            If lStack.IsEmptyStack() Then
                Exit Do
            Else
                If treeCodeView.SelectedItem.Tag <> NEST_FLAG Then
                    treeCodeView.SelectedItem.Parent.Selected = True
                End If
                If InStr(1&, treeCodeView.SelectedItem.Key, TRUE_KEY) > 0& Then
                    treeCodeView.SelectedItem.Next.Selected = True
                    Set Code = lStack.Peek().GetCurrent()
                    Select Case Code.CodeID
                    Case PCODE_IF_COLOR
                        Set Cur = Code.NestList(PARAM_IF_COLOR_FALSE_NEST)
                    Case PCODE_IF_POS_INTO
                        Set Cur = Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST)
                    Case PCODE_IF_TIME_OVER
                        Set Cur = Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST)
                    ' Position of [NEW-CODE] with NEST-IF
                    ' Case PCODE_NEW_CODE
                    End Select
                    If Cur Is Nothing Then
                        Set Cur = lStack.Pop()
                        treeCodeView.SelectedItem.Parent.Selected = True
                    End If
                Else
                    Set Cur = lStack.Pop()
                    treeCodeView.SelectedItem.Parent.Selected = True
                End If
            End If
        End If
    Loop
    
End Sub

Private Function LoadProgram() As Boolean
    Dim CodeList    As PCodeList
    Dim Filer       As PCodeListFiler
    
    Debug.Assert Not mProgramInfo Is Nothing
    Debug.Assert FSO.FileExists(mProgramInfo.Path)
    
    Set Filer = New PCodeListFiler
    
    Set CodeList = Filer.LoadCodeList(mProgramInfo.Path)

    If CodeList Is Nothing Then
        ' Format Error ?
        LoadProgram = False
        Exit Function
    End If
    
    mProgramInfo.Title = Filer.Title
    txtProgramTitle.Text = Filer.Title
    txtStopTime.Text = CStr(Filer.StopTime)
    
    Set Filer = Nothing
    
    Call ExpandProgram(CodeList)

    Set CodeList = Nothing
    
    LoadProgram = True

End Function

Private Sub SetPanelBase(Code As PCode, _
        cmbBase As ComboBox, txtBaseId As TextBox, txtAddX As TextBox, txtAddY As TextBox, _
        BaseIndex As Integer, BaseIdIndex As Integer, AddXIndex As Integer, AddYIndex As Integer)
    Dim i As Integer
    
    With cmbBase
        For i = 0 To .ListCount - 1
            If .ItemData(i) = CLng(Code.IntData(BaseIndex)) Then
                .ListIndex = i
                Exit For
            End If
        Next i
    End With
    
    If Code.IntData(BaseIndex) = POS_BASE_SAVED_POSITION Then
        txtBaseId.Text = CStr(Code.IntData(BaseIdIndex))
    End If
    
    txtAddX.Text = CStr(Code.LngData(AddXIndex))
    txtAddY.Text = CStr(Code.LngData(AddYIndex))
        
End Sub

Private Sub SelectPanel(Code As PCode)

    Dim i   As Integer
        
    With lstCodeList
        For i = 0 To .ListCount - 0
            If .List(i) = PCODE_NAME(Code.CodeID) Then
                .ListIndex = i
            End If
        Next i
    End With
    
    Select Case Code.CodeID
    Case PCODE_BREAK
        txtBreakCount.Text = CStr(Code.IntData(PARAM_BREAK_COUNT_INT))
    
    Case PCODE_REPEAT
        txtRepeatCount.Text = CStr(Code.LngData(PARAM_REPEAT_COUNT_LNG))
    
    Case PCODE_TIMESTAMP
        txtTimestampTimeID.Text = CStr(Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT))
    
    Case PCODE_CAPTURE
        cmbCaptureCapID.ListIndex = Code.IntData(PARAM_CAPTURE_CAP_ID_INT)
    
    Case PCODE_WAIT
        If Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT Then
            optWaitTypeConstant.Value = True
            txtWaitConstant.Text = CStr(Code.LngData(PARAM_WAIT_TIME_LNG) \ 100&)
        Else
            optWaitTypeRandom.Value = True
            txtWaitUnder.Text = CStr(Code.LngData(PARAM_WAIT_UNDER_LNG) \ 100&)
            txtWaitUpper.Text = CStr(Code.LngData(PARAM_WAIT_UPPER_LNG) \ 100&)
        End If
    
    Case PCODE_MOUSE_MOVE
        Call SetPanelBase(Code, _
            cmbMoveBase, txtMovePosID, txtMoveAddX, txtMoveAddY, _
            PARAM_MOVE_BASE_INT, PARAM_MOVE_SPOS_INT, _
            PARAM_MOVE_ADDX_LNG, PARAM_MOVE_ADDY_LNG)
        
    
    Case PCODE_SET_POS
        txtSetPosFromPosID.Text = CStr(Code.IntData(PARAM_SET_POS_FROM_INT))
        txtSetPosToPosID.Text = CStr(Code.IntData(PARAM_SET_POS_TO_INT))
        If Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_MANUAL Then
            optSetPosTypeManual.Value = True
        Else
            optSetPosTypeAutomatic.Value = True
            Call SetPanelBase(Code, _
                cmbSetPosBase, txtSetPosPosID, txtSetPosAddX, txtSetPosAddY, _
                PARAM_SET_POS_BASE_INT, PARAM_SET_POS_SPOS_INT, _
                PARAM_SET_POS_ADDX_LNG, PARAM_SET_POS_ADDY_LNG)
        End If
        
    Case PCODE_IF_COLOR
        txtIfColorColorID.Text = CStr(Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT))
        Select Case Code.IntData(PARAM_IF_COLOR_TYPE_INT)
        Case IF_COLOR_CONSTANT
            optIfColorTypeConstant.Value = True
            Call ShowPickedColor(txtIfColorColor, Code.LngData(PARAM_IF_COLOR_COLOR_LNG))
        Case IF_COLOR_SAVED_COLOR
            optIfColorTypeSaved.Value = True
            txtIfColorSavedColorID.Text = CStr(Code.IntData(PARAM_IF_COLOR_SAVED_ID))
        Case IF_COLOR_PICK_COLOR
            optIfColorTypePick.Value = True
            cmbIfColorCapID.ListIndex = Code.IntData(PARAM_IF_COLOR_CAP_ID_INT)
            Call SetPanelBase(Code, _
                cmbIfColorBase, txtIfColorPosID, txtIfColorAddX, txtIfColorAddY, _
                PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, _
                PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG)
        End Select
    
    Case PCODE_IF_POS_INTO
        txtIfPosIntoLeft.Text = CStr(Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG))
        txtIfPosIntoTop.Text = CStr(Code.LngData(PARAM_IF_POS_INTO_TOP_LNG))
        txtIfPosIntoBottom.Text = CStr(Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG))
        txtIfPosIntoRight.Text = CStr(Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG))
        Call SetPanelBase(Code, _
            cmbIfPosIntoBase, txtIfPosIntoPosID, txtIfPosIntoAddX, txtIfPosIntoAddY, _
            PARAM_IF_POS_INTO_BASE_INT, PARAM_IF_POS_INTO_SPOS_INT, _
            PARAM_IF_POS_INTO_ADDX_LNG, PARAM_IF_POS_INTO_ADDY_LNG)
            
    Case PCODE_IF_TIME_OVER
        txtIfTimeOverTimeID.Text = CStr(Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT))
        txtIfTimeOverLimit.Text = CStr(Code.LngData(PARAM_IF_TIME_OVER_LIMIT_LNG) \ 100&)
    
    Case PCODE_SET_COLOR
        txtSetColorColorID.Text = CStr(Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT))
        Select Case Code.IntData(PARAM_SET_COLOR_TYPE_INT)
        Case SET_COLOR_CONSTANT
            optSetColorTypeConstant.Value = True
            Call ShowPickedColor(txtSetColorColor, Code.LngData(PARAM_SET_COLOR_COLOR_LNG))
        Case SET_COLOR_COPY
            optSetColorTypeCopy.Value = True
            txtSetColorSrcColorID.Text = CStr(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT))
        Case SET_COLOR_MANUAL_PICK
            optSetColorTypeManual.Value = True
        Case SET_COLOR_AUTOMATIC_PICK
            optSetColorTypeAutomatic.Value = True
            cmbSetColorCapID.ListIndex = Code.IntData(PARAM_SET_COLOR_CAP_ID_INT)
            Call SetPanelBase(Code, _
                cmbSetColorBase, txtSetColorPosID, txtSetColorAddX, txtSetColorAddY, _
                PARAM_SET_COLOR_BASE_INT, PARAM_SET_COLOR_SPOS_INT, _
                PARAM_SET_COLOR_ADDX_LNG, PARAM_SET_COLOR_ADDY_LNG)
        End Select
        
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    
    End Select
    
    
End Sub

Private Function GetNewNodeKey() As String
    GetNewNodeKey = "N" & CStr(nodeCount)
    nodeCount = nodeCount + 1
End Function

Private Sub InitCmbCapID(cmbCapID As ComboBox)
    Dim i As Integer
    With cmbCapID
        For i = 0 To MAX_COUNT_OF_CAPTURE - 1
            .AddItem CStr(i)
            .ItemData(.NewIndex) = i
        Next i
        .ListIndex = 0
    End With
End Sub

Private Sub InitCmbBase(cmbBase As ComboBox)
    Dim i As Integer
    With cmbBase
        For i = 0 To POS_BASE_COUNT - 1
            .AddItem POS_BASE_NAME(i)
            .ItemData(.NewIndex) = i
        Next i
        .ListIndex = 0
    End With
End Sub

Private Sub InitParamFrameOfIfColor()
    Call InitCmbCapID(cmbIfColorCapID)
    Call InitCmbBase(cmbIfColorBase)
    Call ShowPickedColor(txtIfColorColor, vbWhite)
    Set optIfColorTypeConstant = optIfColorType(0)
    Set optIfColorTypeSaved = optIfColorType(1)
    Set optIfColorTypePick = optIfColorType(2)
    optIfColorTypeConstant.Caption = IF_COLOR_TYPE_NAME(IF_COLOR_CONSTANT)
    optIfColorTypeSaved.Caption = IF_COLOR_TYPE_NAME(IF_COLOR_SAVED_COLOR)
    optIfColorTypePick.Caption = IF_COLOR_TYPE_NAME(IF_COLOR_PICK_COLOR)
    Call optIfColorType_Click(0)
End Sub

Private Sub InitParamFrameOfMove()
    Call InitCmbBase(cmbMoveBase)
End Sub

Private Sub InitParamFrameOfSetPos()
    Call InitCmbBase(cmbSetPosBase)
    Set optSetPosTypeManual = optSetPosType(0)
    Set optSetPosTypeAutomatic = optSetPosType(1)
    optSetPosTypeManual.Caption = SET_POS_TYPE_NAME(SET_POS_MANUAL)
    optSetPosTypeAutomatic.Caption = SET_POS_TYPE_NAME(SET_POS_AUTOMATIC)
End Sub

Private Sub InitParamFrameOfSetColor()
    Call InitCmbCapID(cmbSetColorCapID)
    Call InitCmbBase(cmbSetColorBase)
    Call ShowPickedColor(txtSetColorColor, vbWhite)
    Set optSetColorTypeConstant = optSetColorType(0)
    Set optSetColorTypeCopy = optSetColorType(1)
    Set optSetColorTypeManual = optSetColorType(2)
    Set optSetColorTypeAutomatic = optSetColorType(3)
    optSetColorTypeConstant.Caption = SET_COLOR_TYPE_NAME(SET_COLOR_CONSTANT)
    optSetColorTypeCopy.Caption = SET_COLOR_TYPE_NAME(SET_COLOR_COPY)
    optSetColorTypeManual.Caption = SET_COLOR_TYPE_NAME(SET_COLOR_MANUAL_PICK)
    optSetColorTypeAutomatic.Caption = SET_COLOR_TYPE_NAME(SET_COLOR_AUTOMATIC_PICK)
    Call optSetColorType_Click(0)
End Sub

Private Sub InitParamFrameOfIfPosInto()
    With cmbIfPosIntoBase
        .AddItem POS_BASE_NAME(POS_BASE_MOUSE_POINTER)
        .ItemData(.NewIndex) = POS_BASE_MOUSE_POINTER
        .AddItem POS_BASE_NAME(POS_BASE_SAVED_POSITION)
        .ItemData(.NewIndex) = POS_BASE_SAVED_POSITION
        .ListIndex = 0
    End With
End Sub

Private Sub InitParamFrameOfCapture()
    Call InitCmbCapID(cmbCaptureCapID)
End Sub


Private Sub SetTagCaptionOfIfColor(ByRef Tag As String, ByRef Caption As String, Code As PCode)
    Dim strTemp     As String
    Dim i           As Integer
    
    strTemp = CStr(Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT))
    Tag = Tag & " " & strTemp
    Caption = Caption & " COLOR-ID[" & strTemp & "]"
    
    i = Code.IntData(PARAM_IF_COLOR_TYPE_INT)
    
    Tag = Tag & " " & CStr(i)
    Caption = Caption & " " & IF_COLOR_TYPE_NAME(i)
    
    Select Case i
    Case IF_COLOR_CONSTANT
        Tag = Tag & " " & CStr(Code.LngData(PARAM_IF_COLOR_COLOR_LNG))
        Caption = Caption & " &H" & Right$("00000" & Hex$(Code.LngData(PARAM_IF_COLOR_COLOR_LNG)), 6&)
    Case IF_COLOR_SAVED_COLOR
        strTemp = CStr(Code.IntData(PARAM_IF_COLOR_SAVED_ID))
        Tag = Tag & " " & strTemp
        Caption = Caption & " Saved COLOR-ID[" & strTemp & "]"
    Case IF_COLOR_PICK_COLOR
        strTemp = CStr(Code.IntData(PARAM_IF_COLOR_CAP_ID_INT))
        Tag = Tag & " " & strTemp
        Caption = Caption & " CAP-ID[" & strTemp & "]"
        
        Call SetTagCaptionOfBasePos(Tag, Caption, Code, _
                PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, _
                PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG)
    End Select
    
End Sub

Private Sub SetTagCaptionOfMove(ByRef Tag As String, ByRef Caption As String, Code As PCode)
    Call SetTagCaptionOfBasePos(Tag, Caption, Code, _
                PARAM_MOVE_BASE_INT, PARAM_MOVE_SPOS_INT, _
                PARAM_MOVE_ADDX_LNG, PARAM_MOVE_ADDY_LNG)
End Sub

Private Sub SetTagCaptionOfBasePos(ByRef Tag As String, ByRef Caption As String, Code As PCode, _
            BaseIndex As Integer, BasePosIDIndex As Integer, AddXIndex As Integer, AddYIndex As Integer)
    Dim strTemp     As String
    
    Tag = Tag & " " & CStr(Code.IntData(BaseIndex))
    Caption = Caption & " " & POS_BASE_NAME(Code.IntData(BaseIndex))
    If Code.IntData(BaseIndex) = POS_BASE_SAVED_POSITION Then
        strTemp = CStr(Code.IntData(BasePosIDIndex))
        Tag = Tag & " " & strTemp
        Caption = Caption & " POS-ID[" & strTemp & "]"
    End If
    
    strTemp = CStr(Code.LngData(AddXIndex))
    Tag = Tag & " " & strTemp
    Caption = Caption & " (" & strTemp
    
    strTemp = CStr(Code.LngData(AddYIndex))
    Tag = Tag & " " & strTemp
    Caption = Caption & "x" & strTemp & ")"
    
End Sub

Private Sub SetTagCaptionOfSetPos(ByRef Tag As String, ByRef Caption As String, Code As PCode)
    Dim strTemp As String
    
    strTemp = CStr(Code.IntData(PARAM_SET_POS_FROM_INT))
    Tag = Tag & " " & strTemp
    Caption = Caption & " from POS-ID[" & strTemp & "]"
    
    strTemp = CStr(Code.IntData(PARAM_SET_POS_TO_INT))
    Tag = Tag & " " & strTemp
    Caption = Caption & " to POS-ID[" & strTemp & "]"
    
    Tag = Tag & " " & CStr(Code.IntData(PARAM_SET_POS_TYPE_INT))
    Caption = Caption & " " & SET_POS_TYPE_NAME(Code.IntData(PARAM_SET_POS_TYPE_INT))
    
    If Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_AUTOMATIC Then
        Call SetTagCaptionOfBasePos(Tag, Caption, Code, _
                PARAM_SET_POS_BASE_INT, PARAM_SET_POS_SPOS_INT, _
                PARAM_SET_POS_ADDX_LNG, PARAM_SET_POS_ADDY_LNG)
    End If
End Sub

Private Sub SetTagCaptionOfIfPosInto(ByRef Tag As String, ByRef Caption As String, Code As PCode)
    Dim strTemp     As String
    
    strTemp = CStr(Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG))
    Tag = Tag & " " & strTemp
    Caption = Caption & " (" & strTemp
    
    strTemp = CStr(Code.LngData(PARAM_IF_POS_INTO_TOP_LNG))
    Tag = Tag & " " & strTemp
    Caption = Caption & "x" & strTemp
    
    strTemp = CStr(Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG))
    Tag = Tag & " " & strTemp
    Caption = Caption & ")-(" & strTemp
    
    strTemp = CStr(Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG))
    Tag = Tag & " " & strTemp
    Caption = Caption & "x" & strTemp & ")"
    
    Call SetTagCaptionOfBasePos(Tag, Caption, Code, _
            PARAM_IF_POS_INTO_BASE_INT, PARAM_IF_POS_INTO_SPOS_INT, _
            PARAM_IF_POS_INTO_ADDX_LNG, PARAM_IF_POS_INTO_ADDY_LNG)
    
End Sub

Private Sub ShowPickedColor(txtColor As TextBox, col As Long)
    Dim c   As String
    Dim T   As Long
    T = (col And &HFF&) Or ((col / &H100&) And &HFF&) Or (col / &H10000)
    With txtColor
        .BackColor = col
        If T < &H80& Then
            .ForeColor = vbWhite
        Else
            .ForeColor = vbBlack
        End If
        c = Hex$(.BackColor)
        If Len(c) < 6& Then
            c = String(6& - Len(c), "0") & c
        End If
        .Text = c
    End With
End Sub

Private Sub ChangeStateCmbBase(cmbBase As ComboBox, lblPosID As Label, txtPosID As TextBox)
    Dim Value   As Boolean
    With cmbBase
        Value = .Enabled And (.ItemData(.ListIndex) = POS_BASE_SAVED_POSITION)
        lblPosID.Enabled = Value
        txtPosID.Enabled = Value
    End With
End Sub

Private Sub FormatTxtAddX(txtAddX As TextBox)
    Dim W As Double
    W = CDbl(GetScreenWidth())
    With txtAddX
        .Text = CStr(Int(LimitValue(Val(.Text), -W, W)))
    End With
End Sub

Private Sub FormatTxtX(txtX As TextBox)
    Dim W As Double
    W = CDbl(GetScreenWidth())
    With txtX
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, W)))
    End With
End Sub

Private Sub FormatTxtAddY(txtAddY As TextBox)
    Dim H As Double
    H = CDbl(GetScreenHeight())
    With txtAddY
        .Text = CStr(Int(LimitValue(Val(.Text), -H, H)))
    End With
End Sub

Private Sub FormatTxtY(txtY As TextBox)
    Dim H As Double
    H = CDbl(GetScreenHeight())
    With txtY
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, H)))
    End With
End Sub

Private Sub FormatTxtPosID(txtPosID As TextBox)
    With txtPosID
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, CDbl(MAX_COUNT_OF_POSITION))))
    End With
End Sub

Private Sub FormatTxtColorID(txtColorID As TextBox)
    With txtColorID
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, CDbl(MAX_COUNT_OF_COLOR))))
    End With
End Sub

Private Function MakeCode() As PCode
    Dim codeType    As Integer
    Dim Code        As PCode
    
    If lstCodeList.ListIndex < 0 Then
        Set MakeCode = Nothing
        Exit Function
    End If
    
    codeType = CInt(lstCodeList.ItemData(lstCodeList.ListIndex))
    
    Set Code = New PCode
    Code.CodeID = codeType
    Call Code.InitCode(codeType)

    Select Case codeType
        
    Case PCODE_REPEAT
        Code.LngData(PARAM_REPEAT_COUNT_LNG) = CLng(Val(txtRepeatCount.Text))
    
    Case PCODE_TIMESTAMP
        Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT) = CInt(Val(txtTimestampTimeID.Text))
        
    Case PCODE_BREAK
        Code.IntData(PARAM_BREAK_COUNT_INT) = CInt(Val(txtBreakCount.Text))
    
    Case PCODE_CAPTURE
        With cmbCaptureCapID
            Code.IntData(PARAM_CAPTURE_CAP_ID_INT) = CInt(.ItemData(.ListIndex))
        End With
    
    Case PCODE_IF_TIME_OVER
        Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT) = CInt(Val(txtIfTimeOverTimeID.Text))
        Code.LngData(PARAM_IF_TIME_OVER_LIMIT_LNG) = CLng(Val(txtIfTimeOverLimit.Text)) * 100&
    
    Case PCODE_WAIT
        If optWaitTypeConstant.Value Then
            Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT
            Code.LngData(PARAM_WAIT_TIME_LNG) = CLng(Val(txtWaitConstant.Text)) * 100&
        Else
            Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_RANDOM
            Code.LngData(PARAM_WAIT_UNDER_LNG) = CLng(Val(txtWaitUnder.Text)) * 100&
            Code.LngData(PARAM_WAIT_UPPER_LNG) = CLng(Val(txtWaitUpper.Text)) * 100&
        End If
    
    Case PCODE_MOUSE_MOVE
        With cmbMoveBase
            If CInt(.ItemData(.ListIndex)) = POS_BASE_SAVED_POSITION Then
                Code.IntData(PARAM_MOVE_SPOS_INT) = CInt(Val(txtMovePosID.Text))
            End If
            Code.IntData(PARAM_MOVE_BASE_INT) = CInt(.ItemData(.ListIndex))
        End With
        Code.LngData(PARAM_MOVE_ADDX_LNG) = CLng(Val(txtMoveAddX.Text))
        Code.LngData(PARAM_MOVE_ADDY_LNG) = CLng(Val(txtMoveAddY.Text))
        
    Case PCODE_SET_POS
        If optSetPosTypeAutomatic.Value Then
            With cmbSetPosBase
                If CInt(.ItemData(.ListIndex)) = POS_BASE_SAVED_POSITION Then
                    Code.IntData(PARAM_SET_POS_SPOS_INT) = CInt(Val(txtSetPosPosID.Text))
                End If
                Code.IntData(PARAM_SET_POS_BASE_INT) = CInt(.ItemData(.ListIndex))
            End With
            Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_AUTOMATIC
            Code.LngData(PARAM_SET_POS_ADDX_LNG) = CLng(Val(txtSetPosAddX.Text))
            Code.LngData(PARAM_SET_POS_ADDY_LNG) = CLng(Val(txtSetPosAddY.Text))
        Else
            Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_MANUAL
        End If
        Code.IntData(PARAM_SET_POS_FROM_INT) = CInt(Val(txtSetPosFromPosID.Text))
        Code.IntData(PARAM_SET_POS_TO_INT) = CInt(Val(txtSetPosToPosID.Text))
    
    Case PCODE_IF_POS_INTO
        Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG) = CLng(Val(txtIfPosIntoLeft.Text))
        Code.LngData(PARAM_IF_POS_INTO_TOP_LNG) = CLng(Val(txtIfPosIntoTop.Text))
        Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG) = CLng(Val(txtIfPosIntoRight.Text))
        Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG) = CLng(Val(txtIfPosIntoBottom.Text))
        With cmbIfPosIntoBase
            Code.IntData(PARAM_IF_POS_INTO_BASE_INT) = CInt(.ItemData(.ListIndex))
            If CInt(.ItemData(.ListIndex)) = POS_BASE_SAVED_POSITION Then
                Code.IntData(PARAM_IF_POS_INTO_SPOS_INT) = CInt(Val(txtIfPosIntoPosID.Text))
            End If
        End With
        Code.LngData(PARAM_IF_POS_INTO_ADDX_LNG) = CLng(Val(txtIfPosIntoAddX.Text))
        Code.LngData(PARAM_IF_POS_INTO_ADDY_LNG) = CLng(Val(txtIfPosIntoAddY.Text))
    
    Case PCODE_IF_COLOR
        Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT) = CInt(Val(txtIfColorColorID.Text))
        If optIfColorTypeConstant.Value Then
            Code.IntData(PARAM_IF_COLOR_TYPE_INT) = IF_COLOR_CONSTANT
            Code.LngData(PARAM_IF_COLOR_COLOR_LNG) = txtIfColorColor.BackColor
        ElseIf optIfColorTypeSaved.Value Then
            Code.IntData(PARAM_IF_COLOR_TYPE_INT) = IF_COLOR_SAVED_COLOR
            Code.IntData(PARAM_IF_COLOR_SAVED_ID) = CInt(Val(txtIfColorSavedColorID.Text))
        Else
            Code.IntData(PARAM_IF_COLOR_TYPE_INT) = IF_COLOR_PICK_COLOR
            With cmbIfColorCapID
                Code.IntData(PARAM_IF_COLOR_CAP_ID_INT) = CInt(.ItemData(.ListIndex))
            End With
            With cmbIfColorBase
                Code.IntData(PARAM_IF_COLOR_BASE_INT) = CInt(.ItemData(.ListIndex))
                If CInt(.ItemData(.ListIndex)) = POS_BASE_SAVED_POSITION Then
                    Code.IntData(PARAM_IF_COLOR_SPOS_INT) = CInt(Val(txtIfColorPosID.Text))
                End If
            End With
            Code.LngData(PARAM_IF_COLOR_ADDX_LNG) = CLng(Val(txtIfColorAddX.Text))
            Code.LngData(PARAM_IF_COLOR_ADDY_LNG) = CLng(Val(txtIfColorAddY.Text))
        End If
        
    Case PCODE_SET_COLOR
        Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT) = CInt(Val(txtSetColorColorID.Text))
        If optSetColorTypeConstant.Value Then
            Code.IntData(PARAM_SET_COLOR_TYPE_INT) = SET_COLOR_CONSTANT
            Code.LngData(PARAM_SET_COLOR_COLOR_LNG) = txtSetColorColor.BackColor
        ElseIf optSetColorTypeCopy Then
            Code.IntData(PARAM_SET_COLOR_TYPE_INT) = SET_COLOR_COPY
            Code.IntData(PARAM_SET_COLOR_SRC_ID_INT) = CInt(Val(txtSetColorSrcColorID.Text))
        ElseIf optSetColorTypeManual Then
            Code.IntData(PARAM_SET_COLOR_TYPE_INT) = SET_COLOR_MANUAL_PICK
        Else
            Code.IntData(PARAM_SET_COLOR_TYPE_INT) = SET_COLOR_AUTOMATIC_PICK
            Code.IntData(PARAM_SET_COLOR_CAP_ID_INT) = cmbSetColorCapID.ListIndex
            With cmbSetColorBase
                If CInt(.ItemData(.ListIndex)) = POS_BASE_SAVED_POSITION Then
                    Code.IntData(PARAM_SET_COLOR_SPOS_INT) = CInt(Val(txtSetColorPosID.Text))
                End If
                Code.IntData(PARAM_SET_COLOR_BASE_INT) = CInt(.ItemData(.ListIndex))
            End With
            Code.LngData(PARAM_SET_COLOR_ADDX_LNG) = CLng(Val(txtSetColorAddX.Text))
            Code.LngData(PARAM_SET_COLOR_ADDY_LNG) = CLng(Val(txtSetColorAddY.Text))
            
        End If
    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    
    End Select

    Set MakeCode = Code
    
End Function

Private Sub MakeTagAndCaption(Code As PCode, ByRef Tag As String, ByRef Caption As String)
    Dim strTemp     As String
    
    Tag = CStr(Code.CodeID)
    
    Select Case Code.CodeID
    Case PCODE_REPEAT
        strTemp = CStr(Code.LngData(PARAM_REPEAT_COUNT_LNG))
        Tag = Tag & " " & strTemp
        Caption = Caption & " " & strTemp
    
    Case PCODE_TIMESTAMP
        strTemp = CStr(Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT))
        Tag = Tag & " " & strTemp
        Caption = Caption & " TIME-ID[" & strTemp & "]"
        
    Case PCODE_BREAK
        strTemp = CStr(Code.IntData(PARAM_BREAK_COUNT_INT))
        Tag = Tag & " " & strTemp
        Caption = Caption & " " & strTemp
    
    Case PCODE_CAPTURE
        strTemp = CStr(Code.IntData(PARAM_CAPTURE_CAP_ID_INT))
        Tag = Tag & " " & strTemp
        Caption = Caption & " CAP-ID[" & strTemp & "]"
    
    Case PCODE_IF_TIME_OVER
        strTemp = CStr(Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT))
        Tag = Tag & " " & strTemp
        Caption = Caption & " TIME-ID[" & strTemp & "]"
        strTemp = CStr(Code.LngData(PARAM_IF_TIME_OVER_LIMIT_LNG))
        Tag = Tag & " " & strTemp
        Caption = Caption & " " & strTemp & "ms"
    
    Case PCODE_IF_POS_INTO
        Call SetTagCaptionOfIfPosInto(Tag, Caption, Code)
    
    Case PCODE_SET_POS
        Call SetTagCaptionOfSetPos(Tag, Caption, Code)
    
    Case PCODE_IF_COLOR
        Call SetTagCaptionOfIfColor(Tag, Caption, Code)
    
    Case PCODE_MOUSE_MOVE
        Call SetTagCaptionOfMove(Tag, Caption, Code)
        
    Case PCODE_WAIT
        Tag = Tag & " " & CStr(Code.IntData(PARAM_WAIT_TYPE_INT))
        Caption = Caption & " " & WAIT_TYPE_NAME(Code.IntData(PARAM_WAIT_TYPE_INT))
        If Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT Then
            strTemp = CStr(Code.LngData(PARAM_WAIT_TIME_LNG))
            Tag = Tag & " " & strTemp
            Caption = Caption & " " & strTemp & "ms"
        Else
            strTemp = CStr(Code.LngData(PARAM_WAIT_UNDER_LNG))
            Tag = Tag & " " & strTemp
            Caption = Caption & " " & strTemp & "ms"
            strTemp = CStr(Code.LngData(PARAM_WAIT_UPPER_LNG))
            Tag = Tag & " " & strTemp
            Caption = Caption & " - " & strTemp & "ms"
        End If
                
    Case PCODE_SET_COLOR
        Tag = Tag & " " & CStr(Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT))
        Caption = Caption & " COLOR-ID[" & CStr(Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT)) & "]"
        Tag = Tag & " " & CStr(Code.IntData(PARAM_SET_COLOR_TYPE_INT))
        Caption = Caption & " " & SET_COLOR_TYPE_NAME(Code.IntData(PARAM_SET_COLOR_TYPE_INT))
        Select Case Code.IntData(PARAM_SET_COLOR_TYPE_INT)
        Case SET_COLOR_CONSTANT
            Tag = Tag & " " & CStr(Code.LngData(PARAM_SET_COLOR_COLOR_LNG))
            Caption = Caption & " " & Right$("00000" & Hex$(Code.LngData(PARAM_SET_COLOR_COLOR_LNG)), 6)
        Case SET_COLOR_COPY
            Tag = Tag & " " & CStr(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT))
            Caption = Caption & " source COLOR-ID[" & CStr(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT)) & "]"
        Case SET_COLOR_AUTOMATIC_PICK
            Tag = Tag & " " & CStr(Code.IntData(PARAM_SET_COLOR_CAP_ID_INT))
            Caption = Caption & " CAP-ID[" & CStr(Code.IntData(PARAM_SET_COLOR_CAP_ID_INT)) & "]"
            Call SetTagCaptionOfBasePos(Tag, Caption, Code, _
                    PARAM_SET_COLOR_BASE_INT, PARAM_SET_COLOR_SPOS_INT, _
                    PARAM_SET_COLOR_ADDX_LNG, PARAM_SET_COLOR_ADDY_LNG)
        End Select
                    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
                    
    End Select
    
    Caption = PCODE_NAME(Code.CodeID) & " " & Caption
End Sub

Private Sub CancelUpdate()
    UpdateFlag = False
    UpdateKey = ""
    Set UpdateTarget = Nothing
    cmdAddCode.Caption = "Add"
End Sub

Private Function UpdateCode()
    
    Debug.Assert UpdateFlag
    Debug.Assert Not UpdateTarget Is Nothing
    Debug.Assert UpdateKey <> ""
    Debug.Assert dicPCode.Exists(UpdateKey)
    Debug.Assert Not treeCodeView.SelectedItem Is Nothing
    Debug.Assert treeCodeView.SelectedItem.Key = UpdateKey

    Dim Code        As PCode
    Dim Tag         As String
    Dim Caption     As String
    
    Set Code = MakeCode()
    
    Call MakeTagAndCaption(Code, Tag, Caption)
    
    With treeCodeView.SelectedItem
        .Text = Caption
        .Tag = Tag
    End With
    
    Set dicPCode.Item(UpdateKey) = Code
    
    Call CancelUpdate
    
End Function

Private Function AddCode(Code As PCode) As String
    
    If Code Is Nothing Then
        Exit Function
    End If
    
    Dim n           As Node
    Dim Nest        As Boolean
    Dim rship       As Integer
    Dim rel         As String
    Dim Tag         As String
    Dim Caption     As String
    Dim strTemp     As String
    
    'make Tag & Caption -----------
    
    Call MakeTagAndCaption(Code, Tag, Caption)
    
    'make New Node -----------
    
    Set n = treeCodeView.SelectedItem
    
    If Not (n Is Nothing) Then
        rel = n.Key
        If n.Tag = NEST_FLAG Then
            If n.Children > 0 Then
                Set n = n.Child
                rel = n.Key
                rship = tvwFirst
            Else
                rship = tvwChild
            End If
        Else
            rship = tvwNext
        End If
        Set n = treeCodeView.Nodes.Add(rel, rship, GetNewNodeKey(), Caption)
    Else
        
        Set n = treeCodeView.Nodes.Add(, , GetNewNodeKey(), Caption)
    End If
    
    AddCode = n.Key
    n.Tag = Tag
    dicPCode.Add n.Key, Code

    'make Nest Node ----------

    Select Case Code.CodeID
    Case PCODE_IF_COLOR, PCODE_IF_TIME_OVER, PCODE_IF_POS_INTO
        treeCodeView.Nodes.Add(n.Key, tvwChild, n.Key & TRUE_KEY, "(True)").Tag = NEST_FLAG
        treeCodeView.Nodes.Add(n.Key, tvwChild, n.Key & FALSE_KEY, "(False)").Tag = NEST_FLAG
        Set n = n.Child
    Case PCODE_REPEAT, PCODE_LOOP
        Set n = treeCodeView.Nodes.Add(n.Key, tvwChild, n.Key & NEST_KEY, "(Nest)")
        n.Tag = NEST_FLAG
    ' Position of [NEW-CODE] with NEST
    End Select
        
    n.Selected = True
End Function

Private Function MakeCodeList() As PCodeList
    Dim n           As Node
    Dim Stack       As ListStack
    Dim CodeList    As PCodeList
    Dim Code        As PCode
    Dim Flag        As Integer
    
    Debug.Assert treeCodeView.Nodes.Count > 1
    Debug.Assert treeCodeView.Nodes.Item(1).Key = "START"
    
    Set n = treeCodeView.Nodes.Item("START").Next
    Set CodeList = New PCodeList
    Set Stack = New ListStack
    
    Do
#If DEBUG_MODE > 0 Then
        Debug.Print n.Text
#End If
           
        Debug.Assert dicPCode.Exists(n.Key)
        
        Set Code = dicPCode.Item(n.Key)
        
        CodeList.Add Code
        
        Flag = 0
        
        If n.Children > 0 Then
            Select Case Code.CodeID
            Case PCODE_LOOP
                Set Code.NestList(PARAM_LOOP_NEST) = New PCodeList
                If n.Child.Children > 0 Then
                    Flag = 1
                    Call Stack.Push(CodeList)
                    Set CodeList = Code.NestList(PARAM_LOOP_NEST)
                    Set n = n.Child.Child
                End If
            Case PCODE_REPEAT
                Set Code.NestList(PARAM_REPEAT_NEST) = New PCodeList
                If n.Child.Children > 0 Then
                    Flag = 1
                    Call Stack.Push(CodeList)
                    Set CodeList = Code.NestList(PARAM_REPEAT_NEST)
                    Set n = n.Child.Child
                End If
                
            Case PCODE_IF_COLOR
                Set Code.NestList(PARAM_IF_COLOR_TRUE_NEST) = New PCodeList
                Set Code.NestList(PARAM_IF_COLOR_FALSE_NEST) = New PCodeList
                If n.Child.Children > 0 Then
                    Flag = 1
                    Call Stack.Push(CodeList)
                    Set CodeList = Code.NestList(PARAM_IF_COLOR_TRUE_NEST)
                    Set n = n.Child.Child
                Else
                    Debug.Assert Not n.Child.Next Is Nothing
                    If n.Child.Next.Children > 0 Then
                        Flag = 1
                        Call Stack.Push(CodeList)
                        Set CodeList = Code.NestList(PARAM_IF_COLOR_FALSE_NEST)
                        Set n = n.Child.Next.Child
                    End If
                End If
            
            Case PCODE_IF_POS_INTO
                Set Code.NestList(PARAM_IF_POS_INTO_TRUE_NEST) = New PCodeList
                Set Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST) = New PCodeList
                If n.Child.Children > 0 Then
                    Flag = 1
                    Call Stack.Push(CodeList)
                    Set CodeList = Code.NestList(PARAM_IF_POS_INTO_TRUE_NEST)
                    Set n = n.Child.Child
                Else
                    Debug.Assert Not n.Child.Next Is Nothing
                    If n.Child.Next.Children > 0 Then
                        Flag = 1
                        Call Stack.Push(CodeList)
                        Set CodeList = Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST)
                        Set n = n.Child.Next.Child
                    End If
                End If
            
            Case PCODE_IF_TIME_OVER
                Set Code.NestList(PARAM_IF_TIME_OVER_TRUE_NEST) = New PCodeList
                Set Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST) = New PCodeList
                If n.Child.Children > 0 Then
                    Flag = 1
                    Call Stack.Push(CodeList)
                    Set CodeList = Code.NestList(PARAM_IF_TIME_OVER_TRUE_NEST)
                    Set n = n.Child.Child
                Else
                    Debug.Assert Not n.Child.Next Is Nothing
                    If n.Child.Next.Children > 0 Then
                        Flag = 1
                        Call Stack.Push(CodeList)
                        Set CodeList = Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST)
                        Set n = n.Child.Next.Child
                    End If
                End If
                                        
            ' Position of [NEW-CODE] with NEST
            ' Case PCODE_NEW_CODE
                            
            Case Else
                Debug.Assert False
                Set MakeCodeList = Nothing
                Exit Function
            End Select
        End If
        
        If Flag = 0 Then
            Do While n.Next Is Nothing
                If n.Parent Is Nothing Then
                    Exit Do
                Else
                    Debug.Assert Not Stack.IsEmptyStack()
                    
                    If InStr(1&, n.Parent.Key, TRUE_KEY) > 0& Then
                        Debug.Assert Not n.Parent.Next Is Nothing
                        If n.Parent.Next.Children > 0 Then
                            Set n = n.Parent.Next.Child
                            Set Code = Stack.Peek().GetTail()
                            Flag = 1
                            Select Case Code.CodeID
                            Case PCODE_IF_COLOR
                                Set CodeList = Code.NestList(PARAM_IF_COLOR_FALSE_NEST)
                            Case PCODE_IF_POS_INTO
                                Set CodeList = Code.NestList(PARAM_IF_POS_INTO_FALSE_NEST)
                            Case PCODE_IF_TIME_OVER
                                Set CodeList = Code.NestList(PARAM_IF_TIME_OVER_FALSE_NEST)
                            ' Position of [NEW-CODE] with NEST-IF
                            ' Case PCODE_NEW_CODE
                            Case Else
                                Debug.Assert False
                                Set MakeCodeList = Nothing
                                Exit Function
                            End Select
                            Exit Do
                        Else
                            Debug.Assert Not n.Parent.Parent Is Nothing
                            Set n = n.Parent.Parent
                            Set CodeList = Stack.Pop()
                        End If
                    Else
                        Debug.Assert InStr(1&, n.Parent.Key, NEST_KEY) > 0& _
                                        Or InStr(1&, n.Parent.Key, FALSE_KEY) > 0&
                        Debug.Assert Not n.Parent.Parent Is Nothing
                        Set n = n.Parent.Parent
                        Set CodeList = Stack.Pop()
                    End If
                End If
            Loop
            If Flag = 0 Then
                If n.Next Is Nothing Then
                    Exit Do
                Else
                    Set n = n.Next
                End If
            End If
        End If
        
    Loop
    
    Set MakeCodeList = CodeList
    
    Call Stack.RemoveAll
    Set Code = Nothing
    Set CodeList = Nothing
    Set Stack = Nothing
    Set n = Nothing
    
End Function

Private Sub cmbIfColorBase_Click()
    Call ChangeStateCmbBase(cmbIfColorBase, lblIfColorPosID, txtIfColorPosID)
End Sub

Private Sub cmbIfPosIntoBase_Click()
    Call ChangeStateCmbBase(cmbIfPosIntoBase, lblIfPosIntoPosID, txtIfPosIntoPosID)
End Sub

Private Sub cmbMoveBase_Click()
    Call ChangeStateCmbBase(cmbMoveBase, lblMovePosID, txtMovePosID)
End Sub

Private Sub cmbSetColorBase_Click()
    Call ChangeStateCmbBase(cmbSetColorBase, lblSetColorPosID, txtSetColorPosID)
End Sub

Private Sub cmbSetPosBase_Click()
    Call ChangeStateCmbBase(cmbSetPosBase, lblSetPosPosID, txtSetPosPosID)
End Sub

Private Sub cmdAddCode_Click()
    If UpdateFlag Then
        Call UpdateCode
    Else
        Call AddCode(MakeCode())
    End If
End Sub

Private Sub cmdAddCode_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If UpdateFlag And Button = vbRightButton Then
        Call CancelUpdate
    End If
End Sub

Private Sub cmdCancel_Click()
    Me.Visible = False
End Sub

Private Sub cmdDeleteCode_Click()
    Dim n As Node
    
    Set n = treeCodeView.SelectedItem
    
    If n Is Nothing Then
        Exit Sub
    End If
        
    If n.Tag = "START" Then
        Exit Sub
    End If
        
    If MsgBox(n.Text & vbNewLine & "Delete This Code?", vbYesNoCancel) <> vbYes Then
        Exit Sub
    End If
    
    If n.Tag = NEST_FLAG Then
        treeCodeView.Nodes.Add(n.Key, tvwPrevious, GetNewNodeKey(), n.Text).Tag = NEST_FLAG
    End If
        
    treeCodeView.Nodes.Remove n.Key
    
    If UpdateFlag Then
        Call CancelUpdate
    End If
    
End Sub

Private Sub cmdIfColorPick_Click()
    
    Call ShowPickedColor(txtIfColorColor, GetPickColor())
    
End Sub

Private Sub cmdIfColorSelect_Click()

    Call SelectColor(txtIfColorColor)

End Sub

Private Sub cmdIfPosIntoRectPick_Click()
    Dim rForm       As RectSelectForm
    
    Set rForm = New RectSelectForm
    
    rForm.Show vbModal, Me
    
    txtIfPosIntoLeft.Text = CStr(rForm.PositionLeft)
    txtIfPosIntoTop.Text = CStr(rForm.PositionTop)
    txtIfPosIntoRight.Text = CStr(rForm.PositionRight)
    txtIfPosIntoBottom.Text = CStr(rForm.PositionBottom)
    
    Unload rForm
    
    Set rForm = Nothing
    
End Sub

Private Sub cmdOK_Click()
    Dim Filer       As PCodeListFiler
    Dim CodeList    As PCodeList
    Dim R           As Integer
    
    If treeCodeView.Nodes.Count < 2 Then
        Call MsgBox("No Codes!", vbExclamation, "Error")
        Exit Sub
    End If
    
    Set CodeList = MakeCodeList()
    
    Set Filer = New PCodeListFiler
    
    mProgramInfo.Title = txtProgramTitle.Text
    
    R = Filer.SaveCodeList(mProgramInfo.Path, CodeList, txtProgramTitle.Text, CLng(txtStopTime.Text))
    
    Set Filer = Nothing
    
#If DEBUG_MODE > 0 Then
    Shell "notepad """ & mPath & """", vbNormalFocus
#End If
    
    Me.Visible = False
    
End Sub

Private Sub cmdSetColorPick_Click()

    Call ShowPickedColor(txtSetColorColor, GetPickColor())

End Sub

Private Sub cmdSetColorSelect_Click()
    
    Call SelectColor(txtSetColorColor)

End Sub

Private Sub Form_Load()
    
    Dim i As Integer
    
    ' Init Controls
    
    With lstCodeList
        For i = 0 To PCODE_COUNT - 1
            .AddItem PCODE_NAME(i)
            .ItemData(.NewIndex) = i
        Next i
    End With
    
    Call InitParamFrameOfMove
    Call InitParamFrameOfIfColor
    Call InitParamFrameOfSetPos
    Call InitParamFrameOfSetColor
    Call InitParamFrameOfCapture
    Call InitParamFrameOfIfPosInto
    
    Set optWaitTypeConstant = optWaitType(0)
    Set optWaitTypeRandom = optWaitType(1)
    optWaitTypeConstant.Caption = WAIT_TYPE_NAME(WAIT_CONSTANT)
    optWaitTypeRandom.Caption = WAIT_TYPE_NAME(WAIT_RANDOM)
    
    fraParamMove.Tag = PCODE_NAME(PCODE_MOUSE_MOVE)
    fraParamWait.Tag = PCODE_NAME(PCODE_WAIT)
    fraParamIfColor.Tag = PCODE_NAME(PCODE_IF_COLOR)
    fraParamRepeat.Tag = PCODE_NAME(PCODE_REPEAT)
    fraParamSetPos.Tag = PCODE_NAME(PCODE_SET_POS)
    fraParamCapture.Tag = PCODE_NAME(PCODE_CAPTURE)
    fraParamBreak.Tag = PCODE_NAME(PCODE_BREAK)
    fraParamTimestamp.Tag = PCODE_NAME(PCODE_TIMESTAMP)
    fraParamIfPosInto.Tag = PCODE_NAME(PCODE_IF_POS_INTO)
    fraParamIfTimeOver.Tag = PCODE_NAME(PCODE_IF_TIME_OVER)
    fraParamSetColor.Tag = PCODE_NAME(PCODE_SET_COLOR)
    
    i = -1
    
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamMove
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamWait
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamIfColor
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamRepeat
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamSetPos
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamCapture
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamBreak
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamTimestamp
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamIfPosInto
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamIfTimeOver
    ReDim Preserve fraParam(iInc(i)): Set fraParam(i) = fraParamSetColor
    
    treeCodeView.Nodes.Add(Key:="START", Text:="(Start Point)").Tag = "START"
    
    ' Load Program
    
    If Not mProgramInfo Is Nothing Then
        If FSO.FileExists(mProgramInfo.Path) Then
            Call LoadProgram
        Else
            txtProgramTitle.Text = mProgramInfo.Title
        End If
    End If
    
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Cancel = True
        Me.Visible = False
    End If
End Sub


Private Sub lstCodeList_Click()
    
    Dim Key As String
    Dim i   As Integer
    
    With lstCodeList
        Key = .List(.ListIndex)
    End With
    
    For i = 0 To UBound(fraParam)
        With fraParam(i)
            .Enabled = (.Tag = Key)
            .Visible = .Enabled
        End With
    Next i
    
    If UpdateFlag Then
        Debug.Assert Not UpdateTarget Is Nothing
        If Key <> PCODE_NAME(UpdateTarget.CodeID) Then
            Call CancelUpdate
        End If
    End If
    
End Sub

Private Sub lstCodeList_DblClick()
    Call AddCode(MakeCode())
End Sub

Private Sub optIfColorType_Click(Index As Integer)
    Dim Value As Boolean
    
    Value = optIfColorTypeConstant.Value
    
    lblIfColorColor.Enabled = Value
    txtIfColorColor.Enabled = Value
    cmdIfColorPick.Enabled = Value
    cmdIfColorSelect.Enabled = Value
    
    Value = optIfColorTypeSaved.Value
    
    lblIfColorSavedColorID.Enabled = Value
    txtIfColorSavedColorID.Enabled = Value
    
    Value = optIfColorTypePick.Value
    
    lblIfColorCapID.Enabled = Value
    cmbIfColorCapID.Enabled = Value
    lblIfColorBase.Enabled = Value
    cmbIfColorBase.Enabled = Value
    lblIfColorPosID.Enabled = Value
    txtIfColorPosID.Enabled = Value
    lblIfColorAddX.Enabled = Value
    txtIfColorAddX.Enabled = Value
    lblIfColorAddY.Enabled = Value
    txtIfColorAddY.Enabled = Value
    
End Sub

Private Sub optSetColorType_Click(Index As Integer)
    Dim Value As Boolean
    
    
    Value = optSetColorTypeConstant.Value
    
    lblSetColorColor.Enabled = Value
    txtSetColorColor.Enabled = Value
    cmdSetColorPick.Enabled = Value
    cmdSetColorSelect.Enabled = Value
    
    Value = optSetColorTypeCopy.Value
    
    lblSetColorSrcColorID.Enabled = Value
    txtSetColorSrcColorID.Enabled = Value
    
    Value = optSetColorTypeAutomatic.Value
    
    lblSetColorCapID.Enabled = Value
    cmbSetColorCapID.Enabled = Value
    lblSetColorBase.Enabled = Value
    cmbSetColorBase.Enabled = Value
    lblSetColorPosID.Enabled = Value
    txtSetColorPosID.Enabled = Value
    lblSetColorAddX.Enabled = Value
    txtSetColorAddX.Enabled = Value
    lblSetColorAddY.Enabled = Value
    txtSetColorAddY.Enabled = Value


    Call ChangeStateCmbBase(cmbSetColorBase, lblSetColorPosID, txtSetColorPosID)

End Sub

Private Sub optSetPosType_Click(Index As Integer)
    Dim Value As Boolean
        
    Value = optSetPosTypeAutomatic.Value
    
    lblSetPosBase.Enabled = Value
    cmbSetPosBase.Enabled = Value
    lblSetPosPosID.Enabled = Value
    txtSetPosPosID.Enabled = Value
    lblSetPosAddX.Enabled = Value
    txtSetPosAddX.Enabled = Value
    lblSetPosAddY.Enabled = Value
    txtSetPosAddY.Enabled = Value
    
    Call ChangeStateCmbBase(cmbSetPosBase, lblSetPosPosID, txtSetPosPosID)
    
End Sub

Private Sub optWaitType_Click(Index As Integer)

    Dim Value As Boolean
    
    Value = optWaitTypeConstant.Value
    
    lblWaitConstant.Enabled = Value
    txtWaitConstant.Enabled = Value
        
    Value = optWaitTypeRandom.Value
        
    lblWaitUpper.Enabled = Value
    txtWaitUpper.Enabled = Value
    lblWaitUnder.Enabled = Value
    txtWaitUnder.Enabled = Value
End Sub

Private Sub treeCodeView_Click()
    
    If UpdateFlag Then
        Call CancelUpdate
    End If
    
End Sub

Private Sub treeCodeView_DblClick()
    Dim Key     As String
    
    If treeCodeView.SelectedItem Is Nothing Then
        Exit Sub
    End If
    
    Key = treeCodeView.SelectedItem.Key
    
    If dicPCode.Exists(Key) = False Then
        If UpdateFlag Then
            Call CancelUpdate
        End If
        Exit Sub
    End If
    
    Set UpdateTarget = dicPCode.Item(Key)
    
    UpdateKey = Key
    
    UpdateFlag = True
    
    Call SelectPanel(UpdateTarget)
    
    cmdAddCode.Caption = "Update"
    
End Sub

Private Sub txtBreakCount_LostFocus()
    With txtBreakCount
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(BREAK_LOWER), CDbl(BREAK_LIMIT))))
    End With
End Sub

Private Sub txtIfColorAddX_LostFocus()
    Call FormatTxtAddX(txtIfColorAddX)
End Sub

Private Sub txtIfColorAddY_LostFocus()
    Call FormatTxtAddY(txtIfColorAddY)
End Sub

Private Sub txtIfColorColorID_LostFocus()
    Call FormatTxtColorID(txtIfColorColorID)
End Sub

Private Sub txtIfColorPosID_LostFocus()
    Call FormatTxtPosID(txtIfColorPosID)
End Sub

Private Sub txtIfColorSavedColorID_LostFocus()
    Call FormatTxtColorID(txtIfColorSavedColorID)
End Sub

Private Sub txtIfPosIntoBottom_LostFocus()
    Call FormatTxtY(txtIfPosIntoBottom)
    With txtIfPosIntoTop
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, Val(txtIfPosIntoBottom.Text))))
    End With
End Sub

Private Sub txtIfPosIntoAddX_LostFocus()
    Call FormatTxtAddX(txtIfPosIntoAddX)
End Sub

Private Sub txtIfPosIntoAddY_LostFocus()
    Call FormatTxtAddY(txtIfPosIntoAddY)
End Sub

Private Sub txtIfPosIntoLeft_LostFocus()
    Call FormatTxtX(txtIfPosIntoLeft)
    With txtIfPosIntoRight
        .Text = CStr(Int(LimitValue(Val(.Text), Val(txtIfPosIntoLeft.Text), CDbl(GetScreenWidth()))))
    End With
End Sub

Private Sub txtIfPosIntoPosID_LostFocus()
    Call FormatTxtPosID(txtIfPosIntoPosID)
End Sub

Private Sub txtIfPosIntoRight_LostFocus()
    Call FormatTxtX(txtIfPosIntoRight)
    With txtIfPosIntoLeft
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, Val(txtIfPosIntoRight.Text))))
    End With
End Sub

Private Sub txtIfPosIntoTop_LostFocus()
    Call FormatTxtY(txtIfPosIntoTop)
    With txtIfPosIntoBottom
        .Text = CStr(Int(LimitValue(Val(.Text), Val(txtIfPosIntoTop.Text), CDbl(GetScreenHeight()))))
    End With
End Sub

Private Sub txtIfTimeOverLimit_LostFocus()
    With txtIfTimeOverLimit
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(TIME_OVER_LIMIT_LOWER \ 100&), CDbl(TIME_OVER_LIMIT_LIMIT \ 100&))))
    End With
End Sub

Private Sub txtMoveAddX_LostFocus()
    Call FormatTxtAddX(txtMoveAddX)
End Sub

Private Sub txtMoveAddY_LostFocus()
    Call FormatTxtAddY(txtMoveAddY)
End Sub

Private Sub txtMovePosID_LostFocus()
    Call FormatTxtPosID(txtMovePosID)
End Sub

Private Sub txtRepeatCount_LostFocus()
    With txtRepeatCount
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(REPEAT_LOWER), CDbl(REPEAT_LIMIT))))
    End With
End Sub

Private Sub txtSetColorAddX_LostFocus()
    Call FormatTxtAddX(txtSetColorAddX)
End Sub

Private Sub txtSetColorAddY_LostFocus()
    Call FormatTxtAddY(txtSetColorAddY)
End Sub

Private Sub txtSetColorColorID_LostFocus()
    Call FormatTxtColorID(txtSetColorColorID)
End Sub

Private Sub txtSetColorPosID_LostFocus()
    Call FormatTxtPosID(txtSetColorPosID)
End Sub

Private Sub txtSetColorSrcColorID_LostFocus()
    Call FormatTxtColorID(txtSetColorSrcColorID)
End Sub

Private Sub txtSetPosAddX_LostFocus()
    Call FormatTxtAddX(txtSetPosAddX)
End Sub

Private Sub txtSetPosAddY_LostFocus()
    Call FormatTxtAddY(txtSetPosAddY)
End Sub

Private Sub txtSetPosFromPosID_LostFocus()
    Call FormatTxtPosID(txtSetPosFromPosID)
    With txtSetPosToPosID
        .Text = CStr(Int(LimitValue(Val(.Text), Val(txtSetPosFromPosID.Text), CDbl(MAX_COUNT_OF_POSITION))))
    End With
End Sub

Private Sub txtSetPosPosID_LostFocus()
    Call FormatTxtPosID(txtSetPosPosID)
End Sub

Private Sub txtSetPosToPosID_LostFocus()
    Call FormatTxtPosID(txtSetPosToPosID)
    With txtSetPosFromPosID
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, Val(txtSetPosToPosID.Text))))
    End With
End Sub

Private Sub txtIfTimeOverTimeID_LostFocus()
    With txtIfTimeOverTimeID
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, CDbl(MAX_COUNT_OF_TIMESTAMP))))
    End With
End Sub

Private Sub txtStopTime_LostFocus()
    With txtTimestampTimeID
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(STOP_TIME_LOWER), CDbl(STOP_TIME_LIMIT))))
    End With
End Sub

Private Sub txtTimestampTimeID_LostFocus()
    With txtTimestampTimeID
        .Text = CStr(Int(LimitValue(Val(.Text), 0#, CDbl(MAX_COUNT_OF_TIMESTAMP))))
    End With
End Sub

Private Sub txtWaitConstant_LostFocus()
    With txtWaitConstant
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(WAIT_LOWER \ 100&), CDbl(WAIT_LIMIT \ 100&))))
    End With
End Sub

Private Sub txtWaitUnder_LostFocus()
    With txtWaitUnder
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(WAIT_LOWER \ 100&), CDbl(WAIT_LIMIT \ 100&))))
    End With
    With txtWaitUpper
        .Text = CStr(Int(LimitValue(Val(.Text), Val(txtWaitUnder.Text), CDbl(WAIT_LIMIT \ 100&))))
    End With
End Sub

Private Sub txtWaitUpper_LostFocus()
    With txtWaitUpper
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(WAIT_LOWER \ 100&), CDbl(WAIT_LIMIT \ 100&))))
    End With
    With txtWaitUnder
        .Text = CStr(Int(LimitValue(Val(.Text), CDbl(WAIT_LOWER \ 100&), Val(txtWaitUpper.Text))))
    End With
End Sub
