Attribute VB_Name = "Utilities"
Option Explicit

'===========================================================================

Public Function DiffDateOfSystemTime(st1 As SYSTEMTIME, st2 As SYSTEMTIME) As Long
    DiffDateOfSystemTime = CLng(DateDiff("d", DateSerial(st1.wYear, st1.wMonth, st1.wDay), _
                    DateSerial(st2.wYear, st2.wMonth, st2.wDay)))
End Function
'===========================================================================

Public Function MilliTimeOfSystemTime(st As SYSTEMTIME) As Long
    MilliTimeOfSystemTime = _
            CLng(st.wHour) * 3600000 _
            + CLng(st.wMinute) * 60000 _
            + CLng(st.wSecond) * 1000& _
            + CLng(st.wMilliseconds)
End Function

'===========================================================================

Public Function DiffTimeOfSystemTime(st1 As SYSTEMTIME, st2 As SYSTEMTIME) As Long
    DiffTimeOfSystemTime = MilliTimeOfSystemTime(st2) - MilliTimeOfSystemTime(st1)
End Function


'===========================================================================

Public Function iInc(ByRef Value As Integer) As Integer
    Value = Value + 1
    iInc = Value
End Function

'===========================================================================

Public Function LimitValue(Value As Double, minValue As Double, maxValue As Double) As Double

    If Value < minValue Then
        Value = minValue
    ElseIf Value > maxValue Then
        Value = maxValue
    End If
    
    LimitValue = Value

End Function

'===========================================================================

Public Function ParseInt(strValue As String, minValue As Integer, maxValue As Integer, ByRef ErrorFlag As Integer) As Integer
    
    If Len(strValue) > Len(CStr(maxValue)) Then
        ErrorFlag = -1
        Exit Function
    End If
    
    If IsNumeric(strValue) = False Then
        ErrorFlag = -2
        Exit Function
    End If
    
    If ErrorFlag = 0 Then
        ParseInt = CInt(LimitValue(Val(strValue), CDbl(minValue - 1), CDbl(maxValue + 1)))
        If ParseInt < minValue Or ParseInt > maxValue Then
            ErrorFlag = -3
        End If
    Else
        ParseInt = CInt(LimitValue(Val(strValue), CDbl(minValue), CDbl(maxValue)))
    End If

End Function

'===========================================================================

Public Function ParseLng(strValue As String, minValue As Long, maxValue As Long, ByRef ErrorFlag As Integer) As Long
    
    If Len(strValue) > Len(CStr(maxValue)) Then
        ErrorFlag = -1
        Exit Function
    End If
    
    If IsNumeric(strValue) = False Then
        ErrorFlag = -2
        Exit Function
    End If
    
    If ErrorFlag = 0 Then
        ParseLng = CLng(LimitValue(Val(strValue), CDbl(minValue - 1&), CDbl(maxValue + 1&)))
        If ParseLng < minValue Or ParseLng > maxValue Then
            ErrorFlag = -3
        End If
    Else
        ParseLng = CLng(LimitValue(Val(strValue), CDbl(minValue), CDbl(maxValue)))
    End If

End Function
