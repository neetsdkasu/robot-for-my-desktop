VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public CodeID       As Integer
Dim mNestList()     As PCodeList
Dim mIntData()      As Integer
Dim mLngData()      As Long

Public Sub InitCode(id As Integer)
    
    CodeID = id
    
    Select Case id
    Case PCODE_SET_POS
        Call Init(PARAM_SET_POS__INT, PARAM_SET_POS__LNG, PARAM_SET_POS__NEST)
    Case PCODE_WAIT
        Call Init(PARAM_WAIT__INT, PARAM_WAIT__LNG, PARAM_WAIT__NEST)
    Case PCODE_MOUSE_MOVE
        Call Init(PARAM_MOVE__INT, PARAM_MOVE__LNG, PARAM_MOVE__NEST)
    Case PCODE_TIMESTAMP
        Call Init(PARAM_TIMESTAMP__INT, PARAM_TIMESTAMP__LNG, PARAM_TIMESTAMP__NEST)
    Case PCODE_CAPTURE
        Call Init(PARAM_CAPTURE__INT, PARAM_CAPTURE__LNG, PARAM_CAPTURE__NEST)
    Case PCODE_IF_COLOR
        Call Init(PARAM_IF_COLOR__INT, PARAM_IF_COLOR__LNG, PARAM_IF_COLOR__NEST)
    Case PCODE_LOOP
        Call Init(PARAM_LOOP__INT, PARAM_LOOP__LNG, PARAM_LOOP__NEST)
    Case PCODE_REPEAT
        Call Init(PARAM_REPEAT__INT, PARAM_REPEAT__LNG, PARAM_REPEAT__NEST)
    Case PCODE_BREAK
        Call Init(PARAM_BREAK__INT, PARAM_BREAK__LNG, PARAM_BREAK__NEST)
    Case PCODE_IF_TIME_OVER
        Call Init(PARAM_IF_TIME_OVER__INT, PARAM_IF_TIME_OVER__LNG, PARAM_IF_TIME_OVER__NEST)
    Case PCODE_IF_POS_INTO
        Call Init(PARAM_IF_POS_INTO__INT, PARAM_IF_POS_INTO__LNG, PARAM_IF_POS_INTO__NEST)
    Case PCODE_SET_COLOR
        Call Init(PARAM_SET_COLOR__INT, PARAM_SET_COLOR__LNG, PARAM_SET_COLOR__NEST)
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    '     Call Init(PARAM_NEW_CODE__INT, PARAM_NEW_CODE__LNT, PARAM_NEW_CODE__NEST)
    Case Else
        Call Init(0, 0, 0)
    End Select
    
End Sub

Public Sub Init(IntDataSize As Integer, LngDataSize As Integer, NestListSize As Integer)
    
    If IntDataSize > 0 Then
        ReDim mIntData(IntDataSize - 1)
    End If
    
    If LngDataSize > 0 Then
        ReDim mLngData(LngDataSize - 1)
    End If
    
    If NestListSize > 0 Then
        ReDim mNestList(NestListSize - 1)
    End If
    
End Sub

Public Property Get NestList(Index As Integer) As PCodeList
    Set NestList = mNestList(Index)
End Property

Public Property Set NestList(Index As Integer, NewNestList As PCodeList)
    Set mNestList(Index) = NewNestList
End Property

Public Property Get IntData(Index As Integer) As Integer
    IntData = mIntData(Index)
End Property

Public Property Let IntData(Index As Integer, NewIntData As Integer)
    mIntData(Index) = NewIntData
End Property

Public Property Get LngData(Index As Integer) As Long
    LngData = mLngData(Index)
End Property

Public Property Let LngData(Index As Integer, NewLngData As Long)
    mLngData(Index) = NewLngData
End Property
