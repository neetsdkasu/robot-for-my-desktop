VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ProgramListFiler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const PROGRAMLIST_TAG           As String = "program-list"
Private Const PROGRAMINFO_TAG           As String = "program-info"
Private Const PARAM_KEY_TITLE           As String = "title"
Private Const PARAM_KEY_PATH            As String = "path"
Private Const PARAM_KEY_FORMAT          As String = "format"
Private Const PARAM_KEY_NEXTID          As String = "next-id"
Private Const PROGRAMLIST_VERSION       As Integer = 1

Dim WithEvents Parser   As TagParser
Attribute Parser.VB_VarHelpID = -1
Dim dicProgramList      As Dictionary
Dim Term                As Integer
Dim mNextID             As Long
Dim mErrorMessage       As String

Public Property Get ErrorMessage() As String
    ErrorMessage = mErrorMessage
End Property

Public Function GetNextID() As Long
    GetNextID = mNextID
End Function

Public Function LoadProgramList(aPath As String, Optional ByRef NextID As Long) As Dictionary
    
    Dim ts          As TextStream
    
    If FSO.FileExists(aPath) = False Then
        Exit Function
    End If
    
    Set Parser = New TagParser
    Set dicProgramList = New Dictionary
    
    Set ts = FSO.OpenTextFile(aPath)
    
    mErrorMessage = "Syntax Error"
    Term = 0
    
    If Parser.Parse(ts) = False Then
        Exit Function
    Else
        mErrorMessage = ""
    End If
    
    NextID = mNextID
    
    Set LoadProgramList = dicProgramList
    Set dicProgramList = Nothing
    
End Function

Public Function SaveProgramList(aPath As String, aProgramList As Dictionary, Optional NextID As Long = 1&)

    Dim ts          As TextStream
    Dim Writer      As TagWriter
    Dim Info        As ProgramInfo
    Dim v, W
    
    mErrorMessage = ""
    
    Set Writer = New TagWriter
    Set ts = FSO.CreateTextFile(aPath)
    
    Call Writer.SetDocument(ts)
    
    Call Writer.AppendStartTag(PROGRAMLIST_TAG)
    Call Writer.AddParam(PARAM_KEY_FORMAT, CStr(PROGRAMLIST_VERSION))
    Call Writer.AddParam(PARAM_KEY_NEXTID, CStr(NextID))
    
    v = aProgramList.Items()
    
    For Each W In v
        Set Info = W
        Call Writer.AppendSimpleTag(PROGRAMINFO_TAG)
        
        Call Writer.AddParam(PARAM_KEY_TITLE, Info.Title)
        Call Writer.AddParam(PARAM_KEY_PATH, Info.Path)
        
    Next W
    
    Call Writer.CloseAllTags
    
    Call ts.Close
    Set ts = Nothing
    Set Writer = Nothing
End Function

Private Sub Parser_FindEndTag(TagName As String, Cancel As Boolean)
    Select Case Term
    Case 0
        Cancel = True
    Case 1
        If (TagName <> PROGRAMLIST_TAG) And (TagName <> PROGRAMINFO_TAG) Then
            Cancel = True
        End If
    End Select
End Sub

Private Sub Parser_FindStartTag(TagName As String, Params As Scripting.Dictionary, Cancel As Boolean)
    Dim sParam      As String
    Dim Info        As ProgramInfo
    
    Select Case Term
    Case 0
        If TagName <> PROGRAMLIST_TAG Then
            Cancel = True
            Exit Sub
        End If
        mErrorMessage = "Paramater Error"
        If Params.Exists(PARAM_KEY_FORMAT) = False Then
            Cancel = True
            Exit Sub
        End If
        sParam = Params.Item(PARAM_KEY_FORMAT)
        If Len(sParam) > Len(CStr(PROGRAMLIST_VERSION)) Then
            Cancel = True
            Exit Sub
        End If
        If IsNumeric(sParam) = False Then
            Cancel = True
            Exit Sub
        End If
        If CInt(sParam) < 1 Then
            Cancel = True
            Exit Sub
        End If
        If CInt(sParam) > PROGRAMLIST_VERSION Then
            mErrorMessage = "Unsupported Format Version : " & sParam
            Cancel = True
            Exit Sub
        End If
        If Params.Exists(PARAM_KEY_NEXTID) = False Then
            Cancel = True
            Exit Sub
        End If
        sParam = Params.Item(PARAM_KEY_NEXTID)
        If IsNumeric(sParam) = False Then
            Cancel = True
            Exit Sub
        End If
        If Len(sParam) > 6& Then
            Cancel = True
            Exit Sub
        End If
        mErrorMessage = "Syntax Error"
        mNextID = CLng(sParam)
        Term = 1
        
    Case 1
        If TagName <> PROGRAMINFO_TAG Then
            Cancel = True
            Exit Sub
        End If
        If Params.Exists(PARAM_KEY_TITLE) = False Then
            mErrorMessage = "Paramater Error"
            Cancel = True
            Exit Sub
        End If
        If Params.Exists(PARAM_KEY_PATH) = False Then
            mErrorMessage = "Paramater Error"
            Cancel = True
            Exit Sub
        End If
        Set Info = New ProgramInfo
        Info.Title = Params.Item(PARAM_KEY_TITLE)
        Info.Path = Params.Item(PARAM_KEY_PATH)
        dicProgramList.Add Info.Path, Info
        Set Info = Nothing
    End Select
    
End Sub

Private Sub Parser_FormatError(posLine As Long, posColumn As Long)
    mErrorMessage = "Parse Error"
End Sub
