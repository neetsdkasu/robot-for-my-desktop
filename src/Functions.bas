Attribute VB_Name = "Functions"
Option Explicit
'#Const DEBUG_MODE = 1

'===========================================================================

Private mFSO As New FileSystemObject
Private mScreenWidth    As Single
Private mScreenHeight   As Single


'===========================================================================
#If DEBUG_MODE > 0 Then

Public Function Hoge()
    Dim k() As Long
    Dim v
    
    v = k
    Debug.Print TypeName(v), UBound(v)
    
End Function

Public Function Baz(ParamArray v())
    Dim CodeList As PCodeList
    Set CodeList = v(0)
    CodeList.Reset
    Do While CodeList.HasNext()
        Debug.Print String(v(1), " "); PCODE_NAME(CodeList.GetNext().CodeID)
        Select Case CodeList.GetCurrent().CodeID
        Case PCODE_LOOP, PCODE_REPEAT
            If Not CodeList.GetCurrent().NestList(0) Is Nothing Then
                Baz CodeList.GetCurrent().NestList(0), v(1) + 4
            End If
        Case PCODE_IF_COLOR, PCODE_IF_TIME_OVER, PCODE_IF_POS_INTO
            Debug.Print String(v(1) + 2, " "); "true"
            If Not CodeList.GetCurrent().NestList(0) Is Nothing Then
                Baz CodeList.GetCurrent().NestList(0), v(1) + 4
            End If
            Debug.Print String(v(1) + 2, " "); "false"
            If Not CodeList.GetCurrent().NestList(1) Is Nothing Then
                Baz CodeList.GetCurrent().NestList(1), v(1) + 4
            End If
        End Select
    Loop
    
End Function

Public Function Bar(ParamArray v())
    Dim Code As PCode
    Set Code = New PCode
    Code.InitCode CInt(v(0))
    Set Bar = Code
End Function

Public Function Foo()
    'Test Codes
    
    Dim p   As String
    Dim f   As String
    
    p = Environ$("TEMP")
    
    Do
        f = FSO.BuildPath(p, FSO.GetTempName() & ".txt")
    Loop While FSO.FileExists(f) Or FSO.FolderExists(f)
    
    Debug.Print "Path= "; f
        
    
    Dim dic     As New Dictionary
    Dim Info    As New ProgramInfo
    Dim ii, jj
    
    Info.Title = "Test Program"
    Info.Path = FSO.BuildPath(App.Path, "program")
    
    For ii = 0 To 10
        Set jj = Info.GetCopy()
        jj.Title = jj.Title & ii
        jj.Path = jj.Path & ii & ".prog"
        dic.Add jj.Path, jj
    Next ii
    
    Dim plf As New ProgramListFiler
    
    plf.SaveProgramList f, dic
    
    Shell "notepad """ & f & """", vbNormalFocus

    Set dic = plf.LoadProgramList(f)
    
    jj = dic.Items()
    For Each ii In jj
        Debug.Print ii.Title, ii.Path
    Next
    
    Exit Function
    
    Dim CodeList    As New PCodeList
    
    CodeList.Add Bar(PCODE_SET_POS)
    CodeList.Add(Bar(PCODE_MOUSE_MOVE)).Add Bar(PCODE_MOUSE_LEFT_CLICK)
    CodeList.Add Bar(PCODE_LOOP)
    Set CodeList.GetTail().NestList(0) = New PCodeList
    CodeList.GetTail().NestList(0).Add Bar(PCODE_IF_POS_INTO)
    Set CodeList.GetTail().NestList(0).GetTail().NestList(0) = New PCodeList
    CodeList.GetTail().NestList(0).GetTail().NestList(0).Add(Bar(PCODE_BREAK)).GetTail().IntData(0) = 1
    Set CodeList.GetTail().NestList(0).GetTail().NestList(1) = New PCodeList
    CodeList.GetTail().NestList(0).GetTail().NestList(1).Add(Bar(PCODE_MOUSE_MOVE)).Add Bar(PCODE_MOUSE_LEFT_CLICK)
    CodeList.GetTail().NestList(0).Add(Bar(PCODE_WAIT)).GetTail().LngData(0) = WAIT_LIMIT
    CodeList.Add Bar(PCODE_MOUSE_MOVE)
    CodeList.Add Bar(PCODE_MOUSE_LEFT_CLICK)
    CodeList.Add Bar(PCODE_MOUSE_LEFT_DBLCLICK)
    CodeList.Add Bar(PCODE_MOUSE_LEFT_DOWN)
    CodeList.Add Bar(PCODE_MOUSE_LEFT_UP)
    CodeList.Add Bar(PCODE_MOUSE_RIGHT_CLICK)
    CodeList.Add Bar(PCODE_MOUSE_RIGHT_DBLCLICK)
    CodeList.Add Bar(PCODE_MOUSE_RIGHT_DOWN)
    CodeList.Add Bar(PCODE_MOUSE_RIGHT_UP)
    CodeList.Add(Bar(PCODE_REPEAT)).GetTail().LngData(0) = 1
    CodeList.Add(Bar(PCODE_WAIT)).GetTail().IntData(PARAM_WAIT_TYPE_INT) = WAIT_RANDOM
    CodeList.GetTail().LngData(PARAM_WAIT_UNDER_LNG) = WAIT_LOWER
    CodeList.GetTail().LngData(PARAM_WAIT_UPPER_LNG) = WAIT_LIMIT
    CodeList.Add Bar(PCODE_IF_COLOR)
    CodeList.GetTail().IntData(PARAM_IF_COLOR_BASE_INT) = POS_BASE_MOUSE_POINTER
    CodeList.Add Bar(PCODE_IF_POS_INTO)
    CodeList.GetTail().IntData(PARAM_IF_POS_INTO_BASE_INT) = POS_BASE_SAVED_POSITION
    CodeList.Add Bar(PCODE_IF_TIME_OVER)
    CodeList.Add Bar(PCODE_CAPTURE)
    CodeList.Add Bar(PCODE_TIMESTAMP)
    CodeList.Add(Bar(PCODE_SET_POS)).GetTail().IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_AUTOMATIC
    CodeList.Add Bar(PCODE_END)
    
    
    DebugPrintCodeList CodeList
       
    Dim wr  As New TagWriter
    Dim fm  As New PCodeListFiler
    Dim tx  As TextStream

    fm.SaveCodeList f, CodeList, "<That's ""Test & Program"">", 270&
    
    Shell "notepad """ & f & """", vbNormalFocus
    
    Set fm = Nothing
    
    Dim cl As PCodeList
    
    Set cl = fm.LoadCodeList(f)
    
    If cl Is Nothing Then
        Debug.Print "Failure to Load!"
    Else
        Debug.Print "Success to Load!"
        Debug.Print "TITLE::", fm.Title
        Debug.Print "STOPTIME::", fm.StopTime
        DebugPrintCodeList cl
    End If
    
    FSO.DeleteFile f
    
    Foo = f
    
End Function


Public Sub DebugPrintCodeList(CodeList As PCodeList, Optional Nest As Long = 1&)
    CodeList.Reset
    Do While CodeList.HasNext()
        Debug.Print String(Nest, " "); PCODE_NAME(CodeList.GetNext().CodeID)
        Select Case CodeList.GetCurrent().CodeID
        Case PCODE_LOOP, PCODE_REPEAT
            If Not CodeList.GetCurrent().NestList(0) Is Nothing Then
                DebugPrintCodeList CodeList.GetCurrent().NestList(0), Nest + 4
            End If
        Case PCODE_IF_COLOR, PCODE_IF_TIME_OVER, PCODE_IF_POS_INTO
            Debug.Print String(Nest + 2, " "); "true"
            If Not CodeList.GetCurrent().NestList(0) Is Nothing Then
                DebugPrintCodeList CodeList.GetCurrent().NestList(0), Nest + 4
            End If
            Debug.Print String(Nest + 2, " "); "false"
            If Not CodeList.GetCurrent().NestList(1) Is Nothing Then
                DebugPrintCodeList CodeList.GetCurrent().NestList(1), Nest + 4
            End If
        End Select
    Loop
    
End Sub
#End If


'===========================================================================

Public Property Get FSO() As FileSystemObject
    Set FSO = mFSO
End Property

'===========================================================================

Public Function GetScreenWidth() As Single
    If mScreenWidth = 0! Then
        mScreenWidth = Screen.Width / Screen.TwipsPerPixelX
    End If
    GetScreenWidth = mScreenWidth
End Function

'===========================================================================

Public Function GetScreenHeight() As Single
    If mScreenHeight = 0! Then
        mScreenHeight = Screen.Height / Screen.TwipsPerPixelY
    End If
    GetScreenHeight = mScreenHeight
End Function

'===========================================================================

Public Function GetPCodeID(strCode As String) As Integer
    Dim i   As Integer
    
    For i = 0 To PCODE_COUNT - 1
        If strCode = PCODE_NAME(i) Then
            GetPCodeID = i
            Exit Function
        End If
    Next i
    
    GetPCodeID = -1
End Function

'===========================================================================
Public Function URLEncode(strData As String) As String
    Dim i   As Long
    Dim sz  As Long
    Dim a   As Long
    Dim c   As String
    Dim e   As String
    Dim h   As String
    
    sz = Len(strData)
    
    For i = 1& To sz
        c = Mid$(strData, i, 1&)
        Select Case c
        Case "0" To "9", "A" To "Z", "a" To "z", "*", "-", "_", "."
            e = e & c
        Case " "
            e = e & "+"
        Case Else
            a = CLng(Asc(c))
            If a >= &H0& And a <= &HFF& Then
                e = e & "%" & Right$("0" & Hex$(a), 2&)
            Else
                h = "0000" & Hex$(a)
                e = e & "%" & Mid$(h, Len(h) - 3&, 2&) & "%" & Right$(h, 2&)
            End If
        End Select
        
    Next i
    
    URLEncode = e

End Function

'===========================================================================
Public Function URLDecode(strData As String) As String
    Dim i   As Long
    Dim sz  As Long
    Dim a   As Long
    Dim c   As String
    Dim e   As String
    Dim h   As String
    Dim f1  As Boolean
    Dim f2  As Boolean
    Dim h1  As String
    
    sz = Len(strData)
    i = 1&
    
    Do While i <= sz
        c = Mid$(strData, i, 1&)
        Select Case c
        Case "0" To "9", "A" To "Z", "a" To "z", "*", "-", "_", "."
            If h1 = "" Then
                e = e & c
            Else
                a = CLng(c)
                If ((a >= &H40& And a <= &H7E&) Or (a >= &H80& And a <= &HFC&)) Then
                    e = e & Chr$("&h" & h1 & Hex$(a))
                Else
                    e = e & Chr$("&h" & h1) & c
                End If
                h1 = ""
            End If
        Case "+"
            e = e & " "
        
        Case "%"
            h = Mid$(strData, i + 1&, 2&)
            a = CLng("&h" & h)
            f2 = ((a >= &H40& And a <= &H7E&) Or (a >= &H80& And a <= &HFC&))
            f1 = ((a >= &H81& And a <= &H9F&) Or (a >= &HE0& And a <= &HFC&))
            If h1 = "" Then
                If f1 Then
                    h1 = h
                Else
                    e = e & Chr$(CInt(a))
                End If
            Else
                If f2 Then
                    e = e & Chr$("&h" & h1 & h)
                    h1 = ""
                ElseIf f1 Then
                    e = e & Chr$("&h" & h1)
                    h1 = h
                Else
                    e = e & Chr$("&h" & h1) & Chr$(CInt(a))
                    h1 = ""
                End If
            End If
            i = i + 2&
            
        Case Else
            URLDecode = strData
            Exit Function
            
        End Select
        i = i + 1&
    Loop
    
    URLDecode = e

End Function

'===========================================================================

Public Function CopyDesktop(dest As PictureBox) As Long
    Dim hDC As Long, hWnd As Long, result As Long
    
    CopyDesktop = &H0&
    
    hWnd = GetDesktopWindow()
    hDC = GetDC(hWnd)
    
    If hDC = 0& Then
        CopyDesktop = &H1&
        Exit Function
    End If
    
    result = GetDeviceCaps(dest.hDC, RASTERCAPS)
    
    If (result And RC_BITBLT) <> RC_BITBLT Then
        CopyDesktop = &H10&
    End If
    
    result = GetDeviceCaps(hDC, RASTERCAPS)
    
    If (result And RC_BITBLT) <> RC_BITBLT Then
        CopyDesktop = CopyDesktop Or &H20&
    End If
    
    If CopyDesktop = &H0& Then
        
        With dest
            result = BitBlt(.hDC, 0&, 0&, CLng(.Width), CLng(.Height), _
                    hDC, 0&, 0&, SRCCOPY)
        End With
        
        If result = 0& Then
            CopyDesktop = &H2&
        End If
        
    End If
    
    result = ReleaseDC(hWnd, hDC)
    
    If result = 0& Then
        CopyDesktop = CopyDesktop Or &H4&
    End If
    
    dest.Refresh
    
End Function





