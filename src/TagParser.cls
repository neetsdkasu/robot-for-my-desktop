VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TagParser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'
' タグを展開するだけ
'
'

Public Event FindStartTag(TagName As String, Params As Dictionary, ByRef Cancel As Boolean)
Public Event FindCommentTag(Comment As String, ByRef Cancel As Boolean)
Public Event FindEndTag(TagName As String, ByRef Cancel As Boolean)
Public Event FindSpecialTag(TagName As String, Text As String, ByRef Cancel As Boolean)
Public Event FindTextNode(Text As String, ByRef Cancel As Boolean)
Public Event FormatError(posLine As Long, posColumn As Long)

Public Function RemoveEntity(strData As String) As String
    RemoveEntity = Replace(Replace(Replace(Replace(Replace(strData, _
                    "&apos;", "'"), "&quot;", """"), "&gt;", ">"), "&lt;", "<"), "&amp;", "&")
End Function


Public Function Parse(doc As TextStream, Optional checkPair As Boolean = False) As Boolean
    Parse = False

    If doc Is Nothing Then
        Exit Function
    End If
    
    Dim Stack       As StringStack
    Dim Params      As New Dictionary
    Dim Text        As String
    Dim ParamName   As String
    Dim ParamValue  As String
    Dim TagName     As String
    Dim Term        As Integer
    Dim SpaceFlag   As Boolean
    Dim ch          As String
    Dim mCancel     As Boolean
    
    Term = 1
    mCancel = False
    Set Stack = New StringStack

    Do While doc.AtEndOfStream = False
        
        ch = doc.Read(1&)
                
        Select Case Term
        Case 1 ' TextNode or lt
            Select Case ch
            Case "<"
                Text = Trim$(Text)
                If Len(Text) > 0& Then
                    RaiseEvent FindTextNode(RemoveEntity(Text), mCancel)
                    If mCancel Then
                        Exit Function
                    End If
                    Text = ""
                    SpaceFlag = False
                End If
                Call Params.RemoveAll
                TagName = ""
                Term = 2
            Case ">"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                Select Case ch
                Case " ", vbCr, vbLf, vbTab
                    If SpaceFlag = False Then
                        SpaceFlag = True
                        Text = Text & " "
                    End If
                Case Else
                    If SpaceFlag = True Then
                        SpaceFlag = False
                    End If
                    Text = Text & ch
                End Select
            End Select
        Case 2  ' First char of TagName
            Select Case ch
            Case "!"
                TagName = ch
                Term = 9
            Case "?"
                TagName = ch
                Term = 12
            Case "/"
                Term = 8
            Case " ", vbLf, vbCr, vbTab
                ' continue term 2
            Case "<", ">"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                TagName = ch
                Term = 3
            End Select
        Case 3 ' Read TagName
            Select Case ch
            Case ">"
                RaiseEvent FindStartTag(TagName, Params, mCancel)
                Call Stack.Push(TagName)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case " ", vbLf, vbCr, vbTab ' To Param
                ParamName = ""
                Term = 4
            Case "/"
                Term = 7
            Case "<"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                TagName = TagName & ch
            End Select
        
        Case 4  ' Read ParamName
            Select Case ch
            Case ">"
                If ParamName <> "" Then
                    Call Params.Add(ParamName, ParamName)
                End If
                RaiseEvent FindStartTag(TagName, Params, mCancel)
                Call Stack.Push(TagName)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case " ", vbLf, vbCr, vbTab
                If ParamName <> "" Then
                    Call Params.Add(ParamName, ParamName)
                End If
                ParamName = ""
            Case "/"
                If ParamName <> "" Then
                    Call Params.Add(ParamName, ParamName)
                End If
                Term = 7
            Case "="
                ParamValue = ""
                Term = 5
            Case "<"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                ParamName = ParamName & ch
            End Select
        Case 5  ' Read ParamValue
            Select Case ch
            Case ">"
                Call Params.Add(ParamName, RemoveEntity(ParamValue))
                RaiseEvent FindStartTag(TagName, Params, mCancel)
                Call Stack.Push(TagName)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case " ", vbLf, vbCr, vbTab
                Call Params.Add(ParamName, RemoveEntity(ParamValue))
                ParamName = ""
                Term = 4
            Case "/"
                Call Params.Add(ParamName, RemoveEntity(ParamValue))
                Term = 7
            Case """"
                Term = 6
            Case "'"
                Term = 17
            Case Else
                ParamValue = ParamValue & ch
            End Select
        Case 6 ' Read ParamValue with Quote
            Select Case ch
            Case """"
                Call Params.Add(ParamName, RemoveEntity(ParamValue))
                ParamName = ""
                Term = 4
            Case Else
                ParamValue = ParamValue & ch
            End Select
        Case 7 ' SimpleTag's gt
            If ch = ">" Then
                RaiseEvent FindStartTag(TagName, Params, mCancel)
                If mCancel Then
                    Exit Function
                End If
                RaiseEvent FindEndTag(TagName, mCancel)
                If mCancel Then
                    Exit Function
                End If
                TagName = ""
                Term = 1
            Else
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            End If
        Case 8 ' EndTag Name
            Select Case ch
            Case ">"
                If TagName = "" Then
                    RaiseEvent FormatError(doc.Line, doc.Column)
                    Exit Function
                End If
                If TagName <> Stack.Peek() Then
                    ' Non Pair  starttag and endtag
                    If checkPair Then
                        RaiseEvent FormatError(doc.Line, doc.Column)
                        Exit Function
                    End If
                Else
                    Call Stack.Pop
                End If
                RaiseEvent FindEndTag(TagName, mCancel)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case "<", "/", " ", vbLf, vbCr, vbTab
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                TagName = TagName & ch
            End Select
        Case 9 ' !SpecialTag TagName
            Select Case ch
            Case " ", vbLf, vbCr, vbTab
                Text = ""
                If TagName = "!--" Then
                    Term = 10
                ElseIf TagName = "![CDATA[" Then
                    Term = 14
                Else
                    Term = 11
                End If
            Case ">"
                RaiseEvent FindSpecialTag(TagName, "", mCancel)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case "<"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                TagName = TagName & ch
            End Select
        Case 10 ' Comment Tag
            Text = Text & ch
            If Right$(Text, 3&) = "-->" Then
                RaiseEvent FindCommentTag(RemoveEntity(Left$(Text, Len(Text) - 3&)), mCancel)
                If mCancel Then
                    Exit Function
                End If
                Text = ""
                Term = 1
            ElseIf Left$(Right$(Text, 3&), 2&) = "--" Then
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            End If
        Case 11 ' !SpecialTag Value
            Select Case ch
            Case ">"
                RaiseEvent FindSpecialTag(TagName, Text, mCancel)
                If mCancel Then
                    Exit Function
                End If
                Text = ""
                Term = 1
            Case "["
                Text = Text & ch
                Term = 15
            Case "-"
                Text = Text & ch
                If Right$(Text, 2&) = "--" Then
                    Term = 16
                End If
            Case "<"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                Text = Text & ch
            End Select
        Case 12 ' ?SpecialTag TagName
            Select Case ch
            Case " ", vbLf, vbCr, vbTab
                Text = ""
                Term = 13
            Case ">"
                RaiseEvent FindSpecialTag(TagName, "", mCancel)
                If mCancel Then
                    Exit Function
                End If
                Term = 1
            Case "<"
                RaiseEvent FormatError(doc.Line, doc.Column)
                Exit Function
            Case Else
                TagName = TagName & ch
            End Select
        Case 13 ' ?SpecialTag Value
            Text = Text & ch
            If Right$(Text, 2&) = "?>" Then
                RaiseEvent FindSpecialTag(TagName, Left$(Text, Len(Text) - 2&), mCancel)
                If mCancel Then
                    Exit Function
                End If
                Text = ""
                Term = 1
            End If
        Case 14 ' CDATA Section
            Text = Text & ch
            If Right$(Text, 3&) = "]]>" Then
                RaiseEvent FindCommentTag(Left$(Text, Len(Text) - 3&), mCancel)
                If mCancel Then
                    Exit Function
                End If
                Text = ""
                Term = 1
            End If
        Case 15 ' !SpecialTag Value CDATA
            If ch = "]" Then
                Term = 11
            End If
            Text = Text & ch
        Case 16 ' !SpecialTag Value Comment
            Text = Text & ch
            If ch = "-" Then
                If Right$(Text, 2&) = "--" Then
                    Term = 11
                End If
            End If
        Case 17 ' Read ParamValue with Apos
            Select Case ch
            Case "'"
                Call Params.Add(ParamName, RemoveEntity(ParamValue))
                ParamName = ""
                Term = 4
            Case Else
                ParamValue = ParamValue & ch
            End Select
        End Select
    Loop
    
    If Term = 1 Then
        If Len(Text) > 0& Then
            ' TODO 最終タグ後のテキストはどうしようか
'            RaiseEvent FindTextNode(Text, mCancel)
        End If
        
        Parse = True
    Else
        RaiseEvent FormatError(doc.Line, doc.Column)
    End If
    
End Function



