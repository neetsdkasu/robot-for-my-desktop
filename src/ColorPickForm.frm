VERSION 5.00
Begin VB.Form ColorPickForm 
   BorderStyle     =   0  '�Ȃ�
   Caption         =   "Form1"
   ClientHeight    =   4635
   ClientLeft      =   -45
   ClientTop       =   -330
   ClientWidth     =   7185
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   14  '���Ƌ^�╄
   Moveable        =   0   'False
   ScaleHeight     =   309
   ScaleMode       =   3  '�߸��
   ScaleWidth      =   479
   ShowInTaskbar   =   0   'False
   Begin VB.Timer MessageTimer 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   1200
      Top             =   3120
   End
   Begin VB.PictureBox picColorPicker 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  '�Ȃ�
      Height          =   1935
      Left            =   0
      ScaleHeight     =   129
      ScaleMode       =   3  '�߸��
      ScaleWidth      =   241
      TabIndex        =   0
      Top             =   0
      Width           =   3615
      Begin VB.Label lblColorNumber 
         Alignment       =   2  '��������
         Appearance      =   0  '�ׯ�
         BackColor       =   &H80000005&
         BorderStyle     =   1  '����
         Caption         =   "lblColorNumber"
         ForeColor       =   &H80000008&
         Height          =   855
         Left            =   1800
         TabIndex        =   3
         Top             =   0
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblMessage 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   1  '����
         Caption         =   "lblMessage"
         BeginProperty Font 
            Name            =   "�l�r �o�S�V�b�N"
            Size            =   27.75
            Charset         =   128
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   585
         Left            =   240
         TabIndex        =   2
         Top             =   1200
         Visible         =   0   'False
         Width           =   2550
      End
      Begin VB.Label lblPosition 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000018&
         BorderStyle     =   1  '����
         Caption         =   "lblPosition"
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Visible         =   0   'False
         Width           =   825
      End
   End
End
Attribute VB_Name = "ColorPickForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mlngPickColor   As Long
Dim mlngPositionX   As Long
Dim mlngPositionY   As Long

Public Property Get PickColor() As Long
    PickColor = mlngPickColor
End Property

Public Property Get PositionX() As Long
    PositionX = mlngPositionX
End Property

Public Property Get PositionY() As Long
    PositionY = mlngPositionY
End Property

Public Sub SetMessage(msg As String)
    With lblMessage
        .Left = (Me.ScaleWidth - .Width) / 2!
        .Top = (Me.ScaleHeight - .Height) / 2!
        .Caption = msg
        .Enabled = True
        .Visible = True
    End With
    MessageTimer.Enabled = True
End Sub


Private Sub Form_Load()
    
    lblMessage.Move 10!, 10!
    
    With Me
        .Left = 0!
        .Top = 0!
        .Width = Screen.Width
        .Height = Screen.Height
    End With

    With picColorPicker
        .Left = 0!
        .Top = 0!
        .Width = Me.ScaleWidth
        .Height = Me.ScaleHeight
    End With
        
    With lblColorNumber
        .Left = .Width
        .Top = .Height
    End With
        
    CopyDesktop picColorPicker
        
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Cancel = True
        Me.Visible = False
    End If
End Sub

Private Sub lblColorNumber_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim w As Single
    w = picColorPicker.Width
    With lblColorNumber
        If .Left * 2! < w Then
            .Left = w - .Width * 2!
        Else
            .Left = .Width
        End If
    End With
    
End Sub

Private Sub lblMessage_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If lblMessage.Visible Then
        lblMessage.Enabled = False
        lblMessage.Visible = False
        MessageTimer.Enabled = False
    End If
End Sub

Private Sub MessageTimer_Timer()
    lblMessage.Enabled = False
    lblMessage.Visible = False
    MessageTimer.Enabled = False
End Sub

Private Sub picColorPicker_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Button = vbLeftButton Then
    
        mlngPositionX = CLng(X)
        mlngPositionY = CLng(Y)
        
        mlngPickColor = picColorPicker.Point(X, Y)
        
        lblMessage.Enabled = False
        lblMessage.Visible = False
        MessageTimer.Enabled = False
        
        Me.Visible = False
        
    Else
        
        With lblPosition
            .Visible = Not .Visible
        End With
        
        With lblColorNumber
            .Visible = Not .Visible
        End With
        
    End If

End Sub

Private Sub picColorPicker_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    With lblPosition
        
        .Caption = CStr(Int(X)) & " x " & CStr(Int(Y))
        
        If X * 2! > picColorPicker.Width Then
            .Left = X - 30! - .Width
        Else
            .Left = X + 30!
        End If
        
        If Y * 2! > picColorPicker.Height Then
            .Top = Y - 30! - .Height
        Else
            .Top = Y + 30!
        End If
        
    End With
    
    Dim c As Long
    
    c = picColorPicker.Point(X, Y)
    
    With lblColorNumber
        If c <> .BackColor Then
            .BackColor = c
            .Caption = Right$("00000" & Hex$(c), 6&)
            c = (c And &HFF&) + ((c / &H100&) And &HFF&) + (c / &H10000)
            If c < 384& Then
                .ForeColor = vbWhite
            Else
                .ForeColor = vbBlack
            End If
        End If
    End With
    
End Sub
