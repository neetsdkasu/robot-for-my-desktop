VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ListStack"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim mStack()    As PCodeList
Dim mCount      As Integer
Dim mSize       As Integer

Public Sub RemoveAll(Optional CleanUp As Boolean = True, Optional RenewSize As Integer = -1)
    Dim i   As Integer
           
    If RenewSize > 0 Then
        mSize = IIf(RenewSize > 5, RenewSize, 5)
        ReDim mStack(mSize)
    End If
    
    If CleanUp Then
        For i = 0 To mSize
            Set mStack(i) = Nothing
        Next i
    End If
    
    mCount = 0
End Sub

Public Function IsEmptyStack() As Boolean
    IsEmptyStack = (mCount = 0)
End Function

Public Sub Push(NewList As PCodeList)
    Set mStack(mCount) = NewList
    mCount = mCount + 1
    If mCount > mSize Then
        mSize = mSize + 5
        ReDim Preserve mStack(mSize)
    End If
End Sub

Public Function Pop() As PCodeList
    If IsEmptyStack() Then
        Set Pop = Nothing
    Else
        mCount = mCount - 1
        Set Pop = mStack(mCount)
    End If
End Function

Public Function Peek() As PCodeList
    If IsEmptyStack() Then
        Set Peek = Nothing
    Else
        Set Peek = mStack(mCount - 1)
    End If
End Function

Private Sub Class_Initialize()
    mCount = 0
    mSize = 5
    ReDim mStack(mSize)
End Sub
