VERSION 5.00
Begin VB.Form RectSelectForm 
   BorderStyle     =   0  '�Ȃ�
   Caption         =   "RectSelectForm"
   ClientHeight    =   4950
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6540
   LinkTopic       =   "Form1"
   MousePointer    =   14  '���Ƌ^�╄
   ScaleHeight     =   330
   ScaleMode       =   3  '�߸��
   ScaleWidth      =   436
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows �̊���l
   Begin VB.PictureBox picColorPicker 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  '�Ȃ�
      Height          =   3375
      Left            =   0
      ScaleHeight     =   225
      ScaleMode       =   3  '�߸��
      ScaleWidth      =   409
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.Label lblExplain 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000018&
         BorderStyle     =   1  '����
         Caption         =   "lblExplain"
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   5
         Top             =   2040
         Width           =   765
      End
      Begin VB.Label lblRightBottom 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000018&
         BorderStyle     =   1  '����
         Caption         =   "lblRightBottom"
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   4
         Top             =   960
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Label lblLeftTop 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000018&
         BorderStyle     =   1  '����
         Caption         =   "lblLeftTop"
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   3
         Top             =   600
         Visible         =   0   'False
         Width           =   795
      End
      Begin VB.Label lblPosition 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000018&
         BorderStyle     =   1  '����
         Caption         =   "lblPosition"
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   2
         Top             =   240
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label lblMessage 
         Appearance      =   0  '�ׯ�
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   1  '����
         Caption         =   "lblMessage"
         BeginProperty Font 
            Name            =   "�l�r �o�S�V�b�N"
            Size            =   27.75
            Charset         =   128
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   585
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Visible         =   0   'False
         Width           =   2550
      End
      Begin VB.Shape SelectedRect 
         BorderStyle     =   3  '�_��
         DrawMode        =   10  'Mask Pen
         FillStyle       =   7  '�Ԋ|��
         Height          =   975
         Left            =   1800
         Top             =   2280
         Visible         =   0   'False
         Width           =   2655
      End
   End
   Begin VB.Timer MessageTimer 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   1200
      Top             =   3720
   End
End
Attribute VB_Name = "RectSelectForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mlngPositionX           As Long
Dim mlngPositionY           As Long
Dim mlngPositionLeft        As Long
Dim mlngPositionTop         As Long
Dim mlngPositionRight       As Long
Dim mlngPositionBottom      As Long
Dim mblnStartSelect         As Boolean
Dim mblnEndSelect           As Boolean

Private Sub SetPosition(lblPos As Label, X As Single, Y As Single)
    With lblPos
        
        .Caption = CStr(Int(X)) & " x " & CStr(Int(Y))
        
        If X * 2! > picColorPicker.Width Then
            .Left = X - 30! - .Width
        Else
            .Left = X + 30!
        End If
        
        If Y * 2! > picColorPicker.Height Then
            .Top = Y - 30! - .Height
        Else
            .Top = Y + 30!
        End If
        
    End With
End Sub

Public Property Get PositionLeft() As Long
    PositionLeft = mlngPositionLeft
End Property

Public Property Get PositionTop() As Long
    PositionTop = mlngPositionTop
End Property

Public Property Get PositionRight() As Long
    PositionRight = mlngPositionRight
End Property

Public Property Get PositionBottom() As Long
    PositionBottom = mlngPositionBottom
End Property

Public Sub SetMessage(msg As String)
    With lblMessage
        .Left = (Me.ScaleWidth - .Width) / 2!
        .Top = (Me.ScaleHeight - .Height) / 2!
        .Caption = msg
        .Enabled = True
        .Visible = True
    End With
    MessageTimer.Enabled = True
End Sub


Private Sub Form_Load()
    
    lblMessage.Move 10!, 10!
    
    With Me
        .Left = 0!
        .Top = 0!
        .Width = Screen.Width
        .Height = Screen.Height
    End With

    With picColorPicker
        .Left = 0!
        .Top = 0!
        .Width = Me.ScaleWidth
        .Height = Me.ScaleHeight
    End With
    
    With lblExplain
        
        .Left = 30!
        .Top = 30!
        
        .Caption = "1st Left Click : Set Left-Top " & vbNewLine _
                        & "2nd Left Click : Set Right-Bottom " & vbNewLine _
                        & "3rd Left Click : Resolve " & vbNewLine & vbNewLine _
                        & "Right Click : Show Position " & vbNewLine _
                        & "Right Click after 1st Left Click : Cancel "
    End With
        
    CopyDesktop picColorPicker
        
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = vbFormControlMenu Then
        Cancel = True
        Me.Visible = False
    End If
End Sub

Private Sub lblExplain_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If lblExplain.Visible Then
        lblExplain.Visible = False
    End If
    
End Sub

Private Sub lblMessage_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If lblMessage.Visible Then
        lblMessage.Enabled = False
        lblMessage.Visible = False
        MessageTimer.Enabled = False
    End If
End Sub

Private Sub MessageTimer_Timer()
    lblMessage.Enabled = False
    lblMessage.Visible = False
    MessageTimer.Enabled = False
End Sub

Private Sub picColorPicker_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If mblnStartSelect Then
    
        If Button = vbLeftButton Then

            If lblPosition.Visible Then
                Call SetPosition(lblRightBottom, X, Y)
                lblRightBottom.Visible = True
            End If
            
            If mlngPositionX < X Then
                mlngPositionLeft = mlngPositionX
                mlngPositionRight = X
            Else
                mlngPositionLeft = X
                mlngPositionRight = mlngPositionX
            End If
        
            If mlngPositionY < Y Then
                mlngPositionTop = mlngPositionY
                mlngPositionBottom = Y
            Else
                mlngPositionTop = Y
                mlngPositionBottom = mlngPositionY
            End If
        
            mblnStartSelect = False
            mblnEndSelect = True
                    
        Else

            mblnStartSelect = False
            mblnEndSelect = False
            SelectedRect.Visible = False
            
            If lblPosition.Visible Then
                lblLeftTop.Visible = False
            End If
            
        End If
        
    ElseIf mblnEndSelect Then
        
        If Button = vbLeftButton Then
        
            lblMessage.Enabled = False
            lblMessage.Visible = False
            MessageTimer.Enabled = False
            
            Me.Visible = False
        
        Else
            
            mblnStartSelect = True
            mblnEndSelect = False
            
            If lblPosition.Visible Then
                lblRightBottom.Visible = False
            End If
            
        End If
        
    Else
        
        If Button = vbLeftButton Then
        
            mlngPositionX = CLng(X)
            mlngPositionY = CLng(Y)
            mblnStartSelect = True
            With SelectedRect
                .Left = X
                .Top = Y
                .Width = 1!
                .Height = 1!
                .Visible = True
            End With
            
            If lblPosition.Visible Then
                Call SetPosition(lblLeftTop, X, Y)
                lblLeftTop.Visible = True
            End If
            
        Else
        
            With lblPosition
                .Visible = Not .Visible
            End With
                
        End If
        
    End If
        
End Sub

Private Sub picColorPicker_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim L  As Single
    Dim T  As Single
    Dim W  As Single
    Dim H  As Single
    
    If mblnStartSelect Then
        
        If mlngPositionX < X Then
            L = mlngPositionX
        Else
            L = X
        End If
        W = Abs(X - mlngPositionX)
        If W < 1! Then
            W = 1!
        End If
        
        If mlngPositionY < Y Then
            T = mlngPositionY
        Else
            T = Y
        End If
        H = Abs(Y - mlngPositionY)
        If H < 1! Then
            H = 1!
        End If
        
        SelectedRect.Move L, T, W, H
        
    End If
    
    Call SetPosition(lblPosition, X, Y)
        
End Sub

