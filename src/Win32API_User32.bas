Attribute VB_Name = "Win32API_User32"
Option Explicit

'===========================================================================

'GetDesktopWindow
' http://msdn.microsoft.com/ja-jp/library/cc364616.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetDesktopWindow Lib "user32" () As Long

'===========================================================================

'GetDC
' http://msdn.microsoft.com/ja-jp/library/cc428664.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long

'===========================================================================

'ReleaseDC
' http://msdn.microsoft.com/ja-jp/library/cc410542.aspx
'  WindowsNT3.1/Windows95
Public Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long

'===========================================================================

'POINTAPI
' http://msdn.microsoft.com/ja-jp/library/dd162805.aspx
Public Type POINTAPI
        x As Long
        y As Long
End Type

'CURSORINFO
' http://msdn.microsoft.com/ja-jp/library/ms648381.aspx
Public Type CURSORINFO
    cbSize  As Long
    flags   As Long
    hCursor As Long
    ptScreenPos As POINTAPI
End Type

#If COMPILECONSTANT_WINDOWS = 95 Then

'GetCursorPos
' http://msdn.microsoft.com/ja-jp/library/cc364640.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

#Else

'GetCursorInfo
' http://msdn.microsoft.com/ja-jp/library/cc364612.aspx
'  WindowsNT4.0SP3/Windows98
Public Declare Function GetCursorInfo Lib "user32" (pci As CURSORINFO) As Long

#End If

'===========================================================================

'SetCursorPos
' http://msdn.microsoft.com/ja-jp/library/cc411029.aspx
'  WindowsNT3.1/Windows95
Public Declare Function SetCursorPos Lib "user32" (ByVal x As Long, ByVal y As Long) As Long

'===========================================================================

'GetAsyncKeyState
' http://msdn.microsoft.com/ja-jp/library/cc364583.aspx
'  WindowsNT3.1/Windows95
Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer

'===========================================================================

'MOUSEINPUT structure
' http://msdn.microsoft.com/ja-jp/library/ms646273.aspx
Public Type tagMOUSEINPUT
    dx          As Long
    dy          As Long
    mouseData   As Long
    dwFlags     As Long
    dwTime      As Long
    dwExtraInfo As Long
End Type

'mouseData
Public Const XBUTTON1 = &H1
Public Const XBUTTON2 = &H2

'dwFlags
' 注意：MOVEでの指定する座標・移動量はpixel単位ではない
Public Const MOUSEEVENTF_ABSOLUTE   As Long = &H8000&
Public Const MOUSEEVENTF_HWHEEL     As Long = &H1000&
Public Const MOUSEEVENTF_MOVE       As Long = &H1&
Public Const MOUSEEVENTF_MOVE_NOCOALESCE As Long = &H2000&
Public Const MOUSEEVENTF_LEFTDOWN   As Long = &H2&
Public Const MOUSEEVENTF_LEFTUP     As Long = &H4&
Public Const MOUSEEVENTF_RIGHTDOWN  As Long = &H8&
Public Const MOUSEEVENTF_RIGHTUP    As Long = &H10&
Public Const MOUSEEVENTF_MIDDLEDOWN As Long = &H20&
Public Const MOUSEEVENTF_MIDDLEUP   As Long = &H40&
Public Const MOUSEEVENTF_VIRTUALDESK As Long = &H4000&
Public Const MOUSEEVENTF_WHEEL      As Long = &H800&
Public Const MOUSEEVENTF_XDOWN      As Long = &H80&
Public Const MOUSEEVENTF_XUP        As Long = &H100&

'===========================================================================


'KEYBDINPUT structure
' http://msdn.microsoft.com/ja-jp/library/ms646271.aspx
Public Type tagKEYBDINPUT
    wVk         As Integer
    wScan       As Integer
    dwFlags     As Long
    dwExtraInfo As Long
    zzz1        As Long
    zzz2        As Long
    zzz3        As Long
End Type

'wVk
' http://msdn.microsoft.com/ja-jp/library/dd375731.aspx
' Virtual Keys, Standard Set
Public Const VK_LBUTTON = &H1
Public Const VK_RBUTTON = &H2
Public Const VK_CANCEL = &H3
Public Const VK_MBUTTON = &H4             '  NOT contiguous with L RBUTTON

Public Const VK_XBUTTON1 = &H5      ' * X1 mouse button
Public Const VK_XBUTTON2 = &H6      ' * X2 mouse button


Public Const VK_BACK = &H8
Public Const VK_TAB = &H9

Public Const VK_CLEAR = &HC
Public Const VK_RETURN = &HD

Public Const VK_SHIFT = &H10
Public Const VK_CONTROL = &H11
Public Const VK_MENU = &H12
Public Const VK_PAUSE = &H13
Public Const VK_CAPITAL = &H14

Public Const VK_KANA = &H15         ' * IME Kana mode
Public Const VK_HANGUEL = &H15      ' * IME Hanguel mode (maintained for compatibility; use VK_HANGUL)
Public Const VK_HANGUL = &H15       ' * IME Hangul mode

Public Const VK_JUNJA = &H17        ' * IME Junja mode
Public Const VK_FINAL = &H18        ' * IME final mode
Public Const VK_HANJA = &H19        ' * IME Hanja mode
Public Const VK_KANJI = &H19        ' * IME Kanji mode

Public Const VK_ESCAPE = &H1B

Public Const VK_CONVERT = &H1C      ' * IME convert
Public Const VK_NONCONVERT = &H1D   ' * IME nonconvert
Public Const VK_ACCEPT = &H1E       ' * IME accept
Public Const VK_MODECHANGE = &H1F   ' * IME mode change request

Public Const VK_SPACE = &H20
Public Const VK_PRIOR = &H21
Public Const VK_NEXT = &H22
Public Const VK_END = &H23
Public Const VK_HOME = &H24
Public Const VK_LEFT = &H25
Public Const VK_UP = &H26
Public Const VK_RIGHT = &H27
Public Const VK_DOWN = &H28
Public Const VK_SELECT = &H29
Public Const VK_PRINT = &H2A
Public Const VK_EXECUTE = &H2B
Public Const VK_SNAPSHOT = &H2C
Public Const VK_INSERT = &H2D
Public Const VK_DELETE = &H2E
Public Const VK_HELP = &H2F

' VK_A thru VK_Z are the same as their ASCII equivalents: 'A' thru 'Z'
' VK_0 thru VK_9 are the same as their ASCII equivalents: '0' thru '9'

Public Const VK_LWIN = &H5B         ' * Left Windows key (Natural keyboard)
Public Const VK_RWIN = &H5C         ' * Right Windows key (Natural keyboard)
Public Const VK_APPS = &H5D         ' * Applications key (Natural keyboard)

Public Const VK_SLEEP = &H5F        ' * Computer Sleep key

Public Const VK_NUMPAD0 = &H60
Public Const VK_NUMPAD1 = &H61
Public Const VK_NUMPAD2 = &H62
Public Const VK_NUMPAD3 = &H63
Public Const VK_NUMPAD4 = &H64
Public Const VK_NUMPAD5 = &H65
Public Const VK_NUMPAD6 = &H66
Public Const VK_NUMPAD7 = &H67
Public Const VK_NUMPAD8 = &H68
Public Const VK_NUMPAD9 = &H69
Public Const VK_MULTIPLY = &H6A
Public Const VK_ADD = &H6B
Public Const VK_SEPARATOR = &H6C
Public Const VK_SUBTRACT = &H6D
Public Const VK_DECIMAL = &H6E
Public Const VK_DIVIDE = &H6F
Public Const VK_F1 = &H70
Public Const VK_F2 = &H71
Public Const VK_F3 = &H72
Public Const VK_F4 = &H73
Public Const VK_F5 = &H74
Public Const VK_F6 = &H75
Public Const VK_F7 = &H76
Public Const VK_F8 = &H77
Public Const VK_F9 = &H78
Public Const VK_F10 = &H79
Public Const VK_F11 = &H7A
Public Const VK_F12 = &H7B
Public Const VK_F13 = &H7C
Public Const VK_F14 = &H7D
Public Const VK_F15 = &H7E
Public Const VK_F16 = &H7F
Public Const VK_F17 = &H80
Public Const VK_F18 = &H81
Public Const VK_F19 = &H82
Public Const VK_F20 = &H83
Public Const VK_F21 = &H84
Public Const VK_F22 = &H85
Public Const VK_F23 = &H86
Public Const VK_F24 = &H87

Public Const VK_NUMLOCK = &H90
Public Const VK_SCROLL = &H91

'
'   VK_L VK_R - left and right Alt, Ctrl and Shift virtual keys.
'   Used only as parameters to GetAsyncKeyState() and GetKeyState().
'   No other API or message will distinguish left and right keys in this way.
'  /
Public Const VK_LSHIFT = &HA0
Public Const VK_RSHIFT = &HA1
Public Const VK_LCONTROL = &HA2
Public Const VK_RCONTROL = &HA3
Public Const VK_LMENU = &HA4
Public Const VK_RMENU = &HA5

Public Const VK_BROWSER_BACK = &HA6         ' * Browser Back key
Public Const VK_BROWSER_FORWARD = &HA7      ' * Browser Forward key
Public Const VK_BROWSER_REFRESH = &HA8      ' * Browser Refresh key
Public Const VK_BROWSER_STOP = &HA9         ' * Browser Stop key
Public Const VK_BROWSER_SEARCH = &HAA       ' * Browser Search key
Public Const VK_BROWSER_FAVORITES = &HAB    ' * Browser Favorites key
Public Const VK_BROWSER_HOME = &HAC         ' * Browser Start and Home key
Public Const VK_VOLUME_MUTE = &HAD          ' * Volume Mute key
Public Const VK_VOLUME_DOWN = &HAE          ' * Volume Down key
Public Const VK_VOLUME_UP = &HAF            ' * Volume Up key
Public Const VK_MEDIA_NEXT_TRACK = &HB0     ' * Next Track key
Public Const VK_MEDIA_PREV_TRACK = &HB1     ' * Previous Track key
Public Const VK_MEDIA_STOP = &HB2           ' * Stop Media key
Public Const VK_MEDIA_PLAY_PAUSE = &HB3     ' * Play/Pause Media key
Public Const VK_LAUNCH_MAIL = &HB4          ' * Start Mail key
Public Const VK_LAUNCH_MEDIA_SELECT = &HB5  ' * Select Media key
Public Const VK_LAUNCH_APP1 = &HB6          ' * Start Application 1 key
Public Const VK_LAUNCH_APP2 = &HB7          ' * Start Application 2 key

Public Const VK_OEM_1 = &HBA                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the ';:' key
Public Const VK_OEM_PLUS = &HBB             ' * For any country/region, the '+' key
Public Const VK_OEM_COMMA = &HBC            ' * For any country/region, the ',' key
Public Const VK_OEM_MINUS = &HBD            ' * For any country/region, the '-' key
Public Const VK_OEM_PERIOD = &HBE           ' * For any country/region, the '.' key
Public Const VK_OEM_2 = &HBF                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the '/?' key
Public Const VK_OEM_3 = &HC0                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the '`~' key

Public Const VK_OEM_4 = &HDB                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the '[{' key
Public Const VK_OEM_5 = &HDC                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the '\|' key
Public Const VK_OEM_6 = &HDD                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard, the ']}' key
Public Const VK_OEM_7 = &HDE                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            '   For the US standard keyboard,  the 'single-quote/double-quote' key
Public Const VK_OEM_8 = &HDF                ' * Used for miscellaneous characters; it can vary by keyboard.
                                            

Public Const VK_OEM_102 = &HE2              ' * Either the angle bracket key or the backslash key on the RT 102-key keyboard

Public Const VK_PROCESSKEY = &HE5           ' * IME PROCESS key

Public Const VK_PACKET = &HE7               ' * Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods.

Public Const VK_ATTN = &HF6
Public Const VK_CRSEL = &HF7
Public Const VK_EXSEL = &HF8
Public Const VK_EREOF = &HF9
Public Const VK_PLAY = &HFA
Public Const VK_ZOOM = &HFB
Public Const VK_NONAME = &HFC
Public Const VK_PA1 = &HFD
Public Const VK_OEM_CLEAR = &HFE


'dwFlags
Public Const KEYEVENTF_EXTENDEDKEY = &H1
Public Const KEYEVENTF_KEYUP = &H2
Public Const KEYEVENTF_SCANCODE = &H8
Public Const KEYEVENTF_UNICODE = &H4

'===========================================================================

'HARDWAREINPUT structure
' http://msdn.microsoft.com/ja-jp/library/ms646269.aspx
Public Type tagHARDWAREINPUT
    uMsg        As Long
    wParamL     As Integer
    wParamH     As Integer
    zzz1        As Long
    zzz2        As Long
    zzz3        As Long
    zzz4        As Long
End Type

'===========================================================================

'INPUT structure
' http://msdn.microsoft.com/ja-jp/library/ms646270.aspx
Public Type MOUSEINPUT
    dwType      As Long
    mi          As tagMOUSEINPUT
End Type

Public Type KEYBDINPUT
    dwType      As Long
    ki          As tagKEYBDINPUT
End Type

Public Type HARDWAREINPUT
    dwType      As Long
    hi          As tagHARDWAREINPUT
End Type

Public Const INPUT_MOUSE = 0
Public Const INPUT_KEYBOARD = 1
Public Const INPUT_HARDWARE = 2

'===========================================================================

#If COMPILECONSTANT_WINDOWS = 95 Then

'mouse_event
' http://msdn.microsoft.com/ja-jp/library/cc410921.aspx
'  WindowsNT3.1/Windows95
Public Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)

'emulate Win32API keybd_event
' http://msdn.microsoft.com/ja-jp/library/ms646304.aspx
'  WindowsNT3.1/Windows95
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

#Else

'SendInput
' http://msdn.microsoft.com/ja-jp/library/cc411004.aspx
'  WindowsNT4.0SP3/Windows98
Public Declare Function SendInputM Lib "user32" Alias "SendInput" (ByVal nInputs As Long, ByRef pInputs As MOUSEINPUT, ByVal cbSize As Long) As Long
Public Declare Function SendInputK Lib "user32" Alias "SendInput" (ByVal nInputs As Long, ByRef pInputs As KEYBDINPUT, ByVal cbSize As Long) As Long
Public Declare Function SendInputH Lib "user32" Alias "SendInput" (ByVal nInputs As Long, ByRef pInputs As HARDWAREINPUT, ByVal cbSize As Long) As Long

#End If

'===========================================================================
'=== emulations ============================================================
'===========================================================================


#If COMPILECONSTANT_WINDOWS <> 95 Then

'===========================================================================

'emulate GetCursorPos
' http://msdn.microsoft.com/ja-jp/library/cc364640.aspx
Public Function GetCursorPos(lpPoint As POINTAPI) As Long
    Dim pci As CURSORINFO
    
    pci.cbSize = Len(pci)
    
    GetCursorPos = GetCursorInfo(pci)
    
    lpPoint = pci.ptScreenPos
    
End Function

'===========================================================================

'emulate mouse_event
' http://msdn.microsoft.com/ja-jp/library/cc410921.aspx
Public Sub mouse_event(ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal dwData As Long, ByVal dwExtraInfo As Long)
    Dim pInputs As MOUSEINPUT
    
    With pInputs
        .dwType = INPUT_MOUSE
        With .mi
            .dx = dx
            .dy = dy
            .dwFlags = dwFlags
            .mouseData = dwData
            .dwExtraInfo = dwExtraInfo
        End With
    End With
    
    Call SendInputM(1, pInputs, Len(pInputs))
    
End Sub

'===========================================================================

'emulate keybd_event
' http://msdn.microsoft.com/ja-jp/library/ms646304.aspx
Public Sub keybd_event(ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
    Dim pInputs As KEYBDINPUT
    
    With pInputs
        .dwType = INPUT_KEYBOARD
         With .ki
            .wVk = CLng(bVk)
            .wScan = CLng(bScan)
            .dwFlags = dwFlags
            .dwExtraInfo = dwExtraInfo
        End With
    End With
    
    Call SendInputK(1, pInputs, Len(pInputs))
    
End Sub

'===========================================================================

#End If
