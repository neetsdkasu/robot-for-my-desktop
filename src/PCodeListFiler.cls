VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PCodeListFiler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'#Const DEBUG_MODE = 1

'===========================================================================

Private Const FORMAT_VERSION                As Integer = 2

'===========================================================================

Private Const PROGRAM_TAG                   As String = "program"
Private Const TRUE_TAG                      As String = "true"
Private Const FALSE_TAG                     As String = "false"

'===========================================================================
'Param Key

Private Const PARAM_KEY_COUNT               As String = "count"
Private Const PARAM_KEY_TIME                As String = "time"
Private Const PARAM_KEY_TYPE                As String = "type"
Private Const PARAM_KEY_LIMIT               As String = "limit"
Private Const PARAM_KEY_TIME_ID             As String = "time-id"
Private Const PARAM_KEY_CAP_ID              As String = "cap-id"
Private Const PARAM_KEY_POS_ID              As String = "pos-id"
Private Const PARAM_KEY_X                   As String = "x"
Private Const PARAM_KEY_Y                   As String = "y"
Private Const PARAM_KEY_BASE                As String = "base"
Private Const PARAM_KEY_BASE_POS_ID         As String = "base-pos-id"
Private Const PARAM_KEY_UPPER               As String = "upper"
Private Const PARAM_KEY_UNDER               As String = "under"
Private Const PARAM_KEY_LEFT                As String = "left"
Private Const PARAM_KEY_RIGHT               As String = "right"
Private Const PARAM_KEY_TOP                 As String = "top"
Private Const PARAM_KEY_BOTTOM              As String = "bottom"
Private Const PARAM_KEY_COLOR               As String = "color"
Private Const PARAM_KEY_FROM                As String = "from"
Private Const PARAM_KEY_TO                  As String = "to"
Private Const PARAM_KEY_FORMAT              As String = "format"
Private Const PARAM_KEY_TITLE               As String = "title"
Private Const PARAM_KEY_STOPTIME            As String = "stop-time"
Private Const PARAM_KEY_COLOR_ID            As String = "color-id"
Private Const PARAM_KEY_SRC                 As String = "src"
Private Const PARAM_KEY_SAVED_ID            As String = "saved-id"

'===========================================================================

Dim WithEvents Parser   As TagParser
Attribute Parser.VB_VarHelpID = -1

'===========================================================================

Dim mCodeList           As PCodeList
Dim mStack              As ListStack
Dim mTerm               As Integer
Dim mScreenWidth        As Long
Dim mScreenHeight       As Long
Dim mTitle              As String
Dim mStopTime           As Long
Dim mErrorMessage       As String
Dim mVersion            As Integer

Public Property Get Title() As String
    Title = mTitle
End Property

Public Property Get StopTime() As Long
    StopTime = mStopTime
End Property

Public Property Get ErrorMessage() As String
    ErrorMessage = mErrorMessage
End Property

Private Function WriteBasePos(Writer As TagWriter, Code As PCode, BaseIndex As Integer, _
                            BasePosIndex As Integer, AddXIndex As Integer, AddYIndex As Integer)
    Call Writer.AddParam(PARAM_KEY_BASE, POS_BASE_NAME(Code.IntData(BaseIndex)))
    If Code.IntData(BaseIndex) = POS_BASE_SAVED_POSITION Then
        Call Writer.AddParam(PARAM_KEY_BASE_POS_ID, CStr(Code.IntData(BasePosIndex)))
    End If
    Call Writer.AddParam(PARAM_KEY_X, CStr(Code.LngData(AddXIndex)))
    Call Writer.AddParam(PARAM_KEY_Y, CStr(Code.LngData(AddYIndex)))
End Function

Private Function WritePcode(Writer As TagWriter, Code As PCode)

    Select Case Code.CodeID
    
    Case PCODE_BREAK
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_COUNT, CStr(Code.IntData(PARAM_BREAK_COUNT_INT)))
    
    Case PCODE_REPEAT
        Call Writer.AppendStartTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_COUNT, CStr(Code.LngData(PARAM_REPEAT_COUNT_LNG)))
    
    Case PCODE_CAPTURE
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_CAP_ID, CStr(Code.IntData(PARAM_CAPTURE_CAP_ID_INT)))
    
    Case PCODE_TIMESTAMP
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_TIME_ID, CStr(Code.IntData(PARAM_TIMESTAMP_TIME_ID_INT)))
    
    Case PCODE_WAIT
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_TYPE, WAIT_TYPE_NAME(Code.IntData(PARAM_WAIT_TYPE_INT)))
        If Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT Then
            Call Writer.AddParam(PARAM_KEY_TIME, CStr(Code.LngData(PARAM_WAIT_TIME_LNG)))
        Else
            Call Writer.AddParam(PARAM_KEY_UNDER, CStr(Code.LngData(PARAM_WAIT_UNDER_LNG)))
            Call Writer.AddParam(PARAM_KEY_UPPER, CStr(Code.LngData(PARAM_WAIT_UPPER_LNG)))
        End If
    
    Case PCODE_MOUSE_MOVE
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call WriteBasePos(Writer, Code, PARAM_MOVE_BASE_INT, PARAM_MOVE_SPOS_INT, _
                        PARAM_MOVE_ADDX_LNG, PARAM_MOVE_ADDY_LNG)
   
    Case PCODE_SET_POS
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_FROM, CStr(Code.IntData(PARAM_SET_POS_FROM_INT)))
        Call Writer.AddParam(PARAM_KEY_TO, CStr(Code.IntData(PARAM_SET_POS_TO_INT)))
        Call Writer.AddParam(PARAM_KEY_TYPE, SET_POS_TYPE_NAME(Code.IntData(PARAM_SET_POS_TYPE_INT)))
        If Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_AUTOMATIC Then
            Call WriteBasePos(Writer, Code, PARAM_SET_POS_BASE_INT, PARAM_SET_POS_SPOS_INT, _
                            PARAM_SET_POS_ADDX_LNG, PARAM_SET_POS_ADDY_LNG)
        End If
    
    Case PCODE_IF_COLOR
        Call Writer.AppendStartTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_COLOR_ID, CStr(Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT)))
        Call Writer.AddParam(PARAM_KEY_TYPE, IF_COLOR_TYPE_NAME(Code.IntData(PARAM_IF_COLOR_TYPE_INT)))
        Select Case Code.IntData(PARAM_IF_COLOR_TYPE_INT)
        Case IF_COLOR_CONSTANT
            Call Writer.AddParam(PARAM_KEY_COLOR, CStr(Code.LngData(PARAM_IF_COLOR_COLOR_LNG)))
        Case IF_COLOR_SAVED_COLOR
            Call Writer.AddParam(PARAM_KEY_SAVED_ID, CStr(Code.IntData(PARAM_IF_COLOR_SAVED_ID)))
        Case IF_COLOR_PICK_COLOR
            Call Writer.AddParam(PARAM_KEY_CAP_ID, CStr(Code.IntData(PARAM_IF_COLOR_CAP_ID_INT)))
            Call WriteBasePos(Writer, Code, PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, _
                            PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG)
        End Select

    Case PCODE_IF_POS_INTO
        Call Writer.AppendStartTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_LEFT, CStr(Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG)))
        Call Writer.AddParam(PARAM_KEY_RIGHT, CStr(Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG)))
        Call Writer.AddParam(PARAM_KEY_TOP, CStr(Code.LngData(PARAM_IF_POS_INTO_TOP_LNG)))
        Call Writer.AddParam(PARAM_KEY_BOTTOM, CStr(Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG)))
        Call WriteBasePos(Writer, Code, PARAM_IF_POS_INTO_BASE_INT, PARAM_IF_POS_INTO_SPOS_INT, _
                        PARAM_IF_POS_INTO_ADDX_LNG, PARAM_IF_POS_INTO_ADDY_LNG)
    
    Case PCODE_IF_TIME_OVER
        Call Writer.AppendStartTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_TIME_ID, CStr(Code.IntData(PARAM_IF_TIME_OVER_TIME_ID_INT)))
        Call Writer.AddParam(PARAM_KEY_LIMIT, CStr(Code.LngData(PARAM_IF_TIME_OVER_LIMIT_LNG)))
    
    Case PCODE_LOOP
        Call Writer.AppendStartTag(PCODE_NAME(Code.CodeID))
    
    Case PCODE_SET_COLOR
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        Call Writer.AddParam(PARAM_KEY_COLOR_ID, CStr(Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT)))
        Call Writer.AddParam(PARAM_KEY_TYPE, SET_COLOR_TYPE_NAME(Code.IntData(PARAM_SET_COLOR_TYPE_INT)))
        Select Case Code.IntData(PARAM_SET_COLOR_TYPE_INT)
        Case SET_COLOR_CONSTANT
            Call Writer.AddParam(PARAM_KEY_COLOR, CStr(Code.LngData(PARAM_SET_COLOR_COLOR_LNG)))
        Case SET_COLOR_COPY
            Call Writer.AddParam(PARAM_KEY_SRC, CStr(Code.IntData(PARAM_SET_COLOR_SRC_ID_INT)))
        Case SET_COLOR_AUTOMATIC_PICK
            Call Writer.AddParam(PARAM_KEY_CAP_ID, CStr(Code.IntData(PARAM_SET_COLOR_CAP_ID_INT)))
            Call WriteBasePos(Writer, Code, PARAM_SET_COLOR_BASE_INT, PARAM_SET_COLOR_SPOS_INT, _
                            PARAM_SET_COLOR_ADDX_LNG, PARAM_SET_COLOR_ADDY_LNG)
        End Select
    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    
    Case Else
        Call Writer.AppendSimpleTag(PCODE_NAME(Code.CodeID))
        
    End Select

End Function

Private Function WriteIfNest(Writer As TagWriter, Code As PCode, TrueIndex As Integer, FalseIndex As Integer)
    Call Writer.AppendStartTag(TRUE_TAG)
    Call WriteCodeList(Writer, Code.NestList(TrueIndex))
    Call Writer.AppendCloseTag
    Call Writer.AppendStartTag(FALSE_TAG)
    Call WriteCodeList(Writer, Code.NestList(FalseIndex))
    Call Writer.AppendCloseTag
    Call Writer.AppendCloseTag
End Function

Private Function WriteCodeList(Writer As TagWriter, CodeList As PCodeList)
    Dim Code As PCode
    
    If CodeList Is Nothing Then
        Exit Function
    End If

    Call CodeList.Reset
    
    Do While CodeList.HasNext()
        Set Code = CodeList.GetNext()
        Call WritePcode(Writer, Code)
        Select Case Code.CodeID
        Case PCODE_LOOP
            Call WriteCodeList(Writer, Code.NestList(PARAM_LOOP_NEST))
            Call Writer.AppendCloseTag
        
        Case PCODE_REPEAT
            Call WriteCodeList(Writer, Code.NestList(PARAM_REPEAT_NEST))
            Call Writer.AppendCloseTag
        
        Case PCODE_IF_COLOR
            Call WriteIfNest(Writer, Code, PARAM_IF_COLOR_TRUE_NEST, PARAM_IF_COLOR_FALSE_NEST)
        
        Case PCODE_IF_POS_INTO
            Call WriteIfNest(Writer, Code, PARAM_IF_POS_INTO_TRUE_NEST, PARAM_IF_POS_INTO_FALSE_NEST)
        
        Case PCODE_IF_TIME_OVER
            Call WriteIfNest(Writer, Code, PARAM_IF_TIME_OVER_TRUE_NEST, PARAM_IF_TIME_OVER_FALSE_NEST)
        
        ' Position of [NEW-CODE] with NEST
        ' Case PCODE_NEW_CODE
        
        End Select
    Loop
        
End Function


Public Function SaveCodeList(aPath As String, CodeList As PCodeList, _
                    Optional aTitle As String = "Non Title", _
                    Optional aStopTime As Long = DEFAULT_STOP_TIME) As Integer
    Dim doc         As TextStream
    Dim Writer      As TagWriter
    
    mTitle = aTitle
    mStopTime = aStopTime
    mErrorMessage = ""
    
    Set Writer = New TagWriter
    
    Set doc = FSO.CreateTextFile(aPath)
        
    Call Writer.SetDocument(doc)
    
    Call Writer.AppendStartTag(PROGRAM_TAG)
    Call Writer.AddParam(PARAM_KEY_FORMAT, CStr(FORMAT_VERSION))
    Call Writer.AddParam(PARAM_KEY_TITLE, mTitle)
    Call Writer.AddParam(PARAM_KEY_STOPTIME, CStr(mStopTime))
    
    Call WriteCodeList(Writer, CodeList)
    
    Call Writer.AppendCloseTag
    
    Call doc.Close
    
    Set Writer = Nothing
    Set doc = Nothing
    
End Function


Public Function LoadCodeList(aPath As String, _
                    Optional ByRef aTitle As String, Optional ByRef aStopTime As Long) As PCodeList

    Dim doc         As TextStream
    
    If FSO.FileExists(aPath) = False Then
        Set LoadCodeList = Nothing
        Exit Function
    End If
    
    Set Parser = New TagParser
    
    Set doc = FSO.OpenTextFile(aPath)
    
    Set mCodeList = New PCodeList
    Set mStack = New ListStack
    mTerm = 0
    mScreenWidth = CLng(GetScreenWidth())
    mScreenHeight = CLng(GetScreenHeight())
    mErrorMessage = "Syntax Error"
    mVersion = FORMAT_VERSION
    
    If Parser.Parse(doc) = False Then
        Set mCodeList = Nothing
    Else
        mErrorMessage = ""
    End If
    
    Call doc.Close
    
    Call mStack.RemoveAll
    
    aTitle = mTitle
    aStopTime = mStopTime
    
    Set LoadCodeList = mCodeList
    Set mStack = Nothing
    Set mCodeList = Nothing
    Set Parser = Nothing
    Set doc = Nothing
    
End Function

Private Function TryParamInt(Params As Dictionary, Code As PCode, Key As String, Index As Integer, _
        minValue As Integer, maxValue As Integer, Optional FullRange As Boolean = False) As Boolean
    Dim Value   As Integer
    Dim Flag    As Integer
    
    TryParamInt = False
    If Params.Exists(Key) = False Then
        Exit Function
    End If
    
    Flag = IIf(FullRange, 1, 0)
    Value = ParseInt(Params.Item(Key), minValue, maxValue, Flag)
    
    If Flag < 0 Then
        Exit Function
    End If
    
    Code.IntData(Index) = Value
    
    TryParamInt = True
End Function

Private Function TryParamLng(Params As Dictionary, Code As PCode, Key As String, Index As Integer, _
        minValue As Long, maxValue As Long, Optional FullRange As Boolean = False) As Boolean
    Dim Value   As Long
    Dim Flag    As Integer
    
    TryParamLng = False
    If Params.Exists(Key) = False Then
        Exit Function
    End If
    
    Flag = IIf(FullRange, 1, 0)
    Value = ParseLng(Params.Item(Key), minValue, maxValue, Flag)
    
    If Flag < 0 Then
        Exit Function
    End If
    
    Code.LngData(Index) = Value
    
    TryParamLng = True
End Function

Private Function TryParamPos(Params As Dictionary, Code As PCode, BaseIndex As Integer, _
            BasePosIndex As Integer, AddXIndex As Integer, AddYIndex As Integer) As Boolean
    Dim sParam  As String
    Dim i       As Integer
    
    TryParamPos = False
    
    If Params.Exists(PARAM_KEY_BASE) = False Then
        Exit Function
    End If
    
    sParam = Params.Item(PARAM_KEY_BASE)
    
    For i = 0 To POS_BASE_COUNT - 1
        If sParam = POS_BASE_NAME(i) Then
            Exit For
        End If
    Next i
    
    If i = POS_BASE_COUNT Then
        Exit Function
    End If
    
    Code.IntData(BaseIndex) = i
    
    If i = POS_BASE_SAVED_POSITION Then
        If TryParamPosID(Params, Code, PARAM_KEY_BASE_POS_ID, BasePosIndex) = False Then
            Exit Function
        End If
    End If
    
    If TryParamX(Params, Code, PARAM_KEY_X, AddXIndex) = False Then
        Exit Function
    End If
    
    If TryParamY(Params, Code, PARAM_KEY_Y, AddYIndex) = False Then
        Exit Function
    End If
    
    TryParamPos = True
End Function

Private Function TryParamX(Params As Dictionary, Code As PCode, Key As String, Index As Integer) As Boolean
    TryParamX = TryParamLng(Params, Code, Key, Index, -mScreenWidth, mScreenWidth)
End Function

Private Function TryParamY(Params As Dictionary, Code As PCode, Key As String, Index As Integer) As Boolean
    TryParamY = TryParamLng(Params, Code, Key, Index, -mScreenHeight, mScreenHeight)
End Function

Private Function TryParamCapID(Params As Dictionary, Code As PCode, Index As Integer) As Boolean
    TryParamCapID = TryParamInt(Params, Code, PARAM_KEY_CAP_ID, Index, 0, MAX_COUNT_OF_CAPTURE)
End Function

Private Function TryParamTimeID(Params As Dictionary, Code As PCode, Index As Integer) As Boolean
    TryParamTimeID = TryParamInt(Params, Code, PARAM_KEY_TIME_ID, Index, 0, MAX_COUNT_OF_TIMESTAMP)
End Function

Private Function TryParamPosID(Params As Dictionary, Code As PCode, Key As String, Index As Integer) As Boolean
    TryParamPosID = TryParamInt(Params, Code, Key, Index, 0, MAX_COUNT_OF_POSITION)
End Function

Private Function TryParamColorID(Params As Dictionary, Code As PCode, Key As String, Index As Integer) As Boolean
    TryParamColorID = TryParamInt(Params, Code, Key, Index, 0, MAX_COUNT_OF_COLOR)
End Function

Private Function TryParamColor(Params As Dictionary, Code As PCode, Index As Integer) As Boolean
    TryParamColor = TryParamLng(Params, Code, PARAM_KEY_COLOR, Index, &H0&, &HFFFFFF)
End Function

Private Function LoadParamsV1(id As Integer, Params As Dictionary) As Integer
    Dim Code        As PCode
    
    LoadParamsV1 = -1
    
    If id <> PCODE_IF_COLOR Then
        Exit Function
    End If
    
    ' Version-1 If-Color divide Set-Color & If-Color
    
    Set Code = New PCode
    
    Call Code.InitCode(PCODE_SET_COLOR)
    
    Code.IntData(PARAM_SET_COLOR_COLOR_ID_INT) = MAX_COUNT_OF_COLOR
    Code.IntData(PARAM_SET_COLOR_TYPE_INT) = SET_COLOR_CONSTANT
    If TryParamColor(Params, Code, PARAM_SET_COLOR_COLOR_LNG) = False Then
        Exit Function
    End If
    
    Call mCodeList.Add(Code)
    
    Set Code = New PCode
    
    Call Code.InitCode(PCODE_IF_COLOR)
    
    Code.IntData(PARAM_IF_COLOR_COLOR_ID_INT) = MAX_COUNT_OF_COLOR
    Code.IntData(PARAM_IF_COLOR_TYPE_INT) = IF_COLOR_PICK_COLOR
    
    If TryParamCapID(Params, Code, PARAM_IF_COLOR_CAP_ID_INT) = False Then
        Exit Function
    End If
    If TryParamPos(Params, Code, PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG) = False Then
        Exit Function
    End If
    
    Call mCodeList.Add(Code)
    
    LoadParamsV1 = 2
    
    Set Code = Nothing
    
End Function

Private Function LoadParams(id As Integer, Params As Dictionary) As Integer
    Dim Code        As New PCode
    Dim sParam      As String
    Dim iParam      As Integer
    Dim lParam      As Long
    Dim Nest        As Integer
    Dim ErrorFlag   As Integer
    Dim i           As Integer
    
    LoadParams = -1
    Nest = 0
    ErrorFlag = 0
        
    Call Code.InitCode(id)
    
    Select Case id
    Case PCODE_BREAK
        If TryParamInt(Params, Code, PARAM_KEY_COUNT, PARAM_BREAK_COUNT_INT, BREAK_LOWER, BREAK_LIMIT) = False Then
            Exit Function
        End If
        
    Case PCODE_CAPTURE
        If TryParamCapID(Params, Code, PARAM_CAPTURE_CAP_ID_INT) = False Then
            Exit Function
        End If
    
    Case PCODE_TIMESTAMP
        If TryParamTimeID(Params, Code, PARAM_TIMESTAMP_TIME_ID_INT) = False Then
            Exit Function
        End If
        
    Case PCODE_WAIT
        If Params.Exists(PARAM_KEY_TYPE) = False Then
            Exit Function
        End If
        sParam = Params.Item(PARAM_KEY_TYPE)
        If sParam = WAIT_TYPE_NAME(WAIT_CONSTANT) Then
            Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_CONSTANT
            If TryParamLng(Params, Code, PARAM_KEY_TIME, PARAM_WAIT_TIME_LNG, WAIT_LOWER, WAIT_LIMIT) = False Then
                Exit Function
            End If
            
        ElseIf sParam = WAIT_TYPE_NAME(WAIT_RANDOM) Then
            Code.IntData(PARAM_WAIT_TYPE_INT) = WAIT_RANDOM
            If TryParamLng(Params, Code, PARAM_KEY_UPPER, PARAM_WAIT_UPPER_LNG, WAIT_LOWER, WAIT_LIMIT) = False Then
                Exit Function
            End If
            If TryParamLng(Params, Code, PARAM_KEY_UNDER, PARAM_WAIT_UNDER_LNG, WAIT_LOWER, Code.LngData(PARAM_WAIT_UPPER_LNG)) = False Then
                Exit Function
            End If
        
        Else
            Exit Function
        End If
        
        
    Case PCODE_MOUSE_MOVE
        If TryParamPos(Params, Code, PARAM_MOVE_BASE_INT, PARAM_MOVE_SPOS_INT, PARAM_MOVE_ADDX_LNG, PARAM_MOVE_ADDY_LNG) = False Then
            Exit Function
        End If
        
    Case PCODE_SET_POS
        If TryParamPosID(Params, Code, PARAM_KEY_FROM, PARAM_SET_POS_FROM_INT) = False Then
            Exit Function
        End If
        If TryParamPosID(Params, Code, PARAM_KEY_TO, PARAM_SET_POS_TO_INT) = False Then
            Exit Function
        End If
        If Code.IntData(PARAM_SET_POS_FROM_INT) > Code.IntData(PARAM_SET_POS_TO_INT) Then
            Exit Function
        End If
        If Params.Exists(PARAM_KEY_TYPE) = False Then
            Exit Function
        End If
        sParam = Params.Item(PARAM_KEY_TYPE)
        If sParam = SET_POS_TYPE_NAME(SET_POS_AUTOMATIC) Then
            Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_AUTOMATIC
            If TryParamPos(Params, Code, PARAM_SET_POS_BASE_INT, PARAM_SET_POS_SPOS_INT, PARAM_SET_POS_ADDX_LNG, PARAM_SET_POS_ADDY_LNG) = False Then
                Exit Function
            End If
        ElseIf sParam = SET_POS_TYPE_NAME(SET_POS_MANUAL) Then
            Code.IntData(PARAM_SET_POS_TYPE_INT) = SET_POS_MANUAL
        Else
            Exit Function
        End If
    
    Case PCODE_IF_COLOR
        If TryParamColorID(Params, Code, PARAM_KEY_COLOR_ID, PARAM_IF_COLOR_COLOR_ID_INT) = False Then
            Exit Function
        End If
        If Params.Exists(PARAM_KEY_TYPE) = False Then
            Exit Function
        End If
        sParam = Params.Item(PARAM_KEY_TYPE)
        For i = 0 To IF_COLOR_COUNT - 1
            If sParam = IF_COLOR_TYPE_NAME(i) Then
                Code.IntData(PARAM_IF_COLOR_TYPE_INT) = i
                Exit For
            End If
        Next i
        Select Case i
        Case IF_COLOR_CONSTANT
            If TryParamColor(Params, Code, PARAM_IF_COLOR_COLOR_LNG) = False Then
                Exit Function
            End If
        Case IF_COLOR_SAVED_COLOR
            If TryParamColorID(Params, Code, PARAM_KEY_SAVED_ID, PARAM_IF_COLOR_SAVED_ID) = False Then
                Exit Function
            End If
        Case IF_COLOR_PICK_COLOR
            If TryParamCapID(Params, Code, PARAM_IF_COLOR_CAP_ID_INT) = False Then
                Exit Function
            End If
            If TryParamPos(Params, Code, PARAM_IF_COLOR_BASE_INT, PARAM_IF_COLOR_SPOS_INT, PARAM_IF_COLOR_ADDX_LNG, PARAM_IF_COLOR_ADDY_LNG) = False Then
                Exit Function
            End If
        Case Else
            Exit Function
        End Select
        Nest = 2
        
    Case PCODE_IF_POS_INTO
        If TryParamLng(Params, Code, PARAM_KEY_LEFT, PARAM_IF_POS_INTO_LEFT_LNG, 0&, mScreenWidth) = False Then
            Exit Function
        End If
        If TryParamLng(Params, Code, PARAM_KEY_RIGHT, PARAM_IF_POS_INTO_RIGHT_LNG, 0&, mScreenWidth) = False Then
            Exit Function
        End If
        If Code.LngData(PARAM_IF_POS_INTO_LEFT_LNG) > Code.LngData(PARAM_IF_POS_INTO_RIGHT_LNG) Then
            Exit Function
        End If
        If TryParamLng(Params, Code, PARAM_KEY_TOP, PARAM_IF_POS_INTO_TOP_LNG, 0&, mScreenHeight) = False Then
            Exit Function
        End If
        If TryParamLng(Params, Code, PARAM_KEY_BOTTOM, PARAM_IF_POS_INTO_BOTTOM_LNG, 0&, mScreenHeight) = False Then
            Exit Function
        End If
        If Code.LngData(PARAM_IF_POS_INTO_TOP_LNG) > Code.LngData(PARAM_IF_POS_INTO_BOTTOM_LNG) Then
            Exit Function
        End If
        If TryParamPos(Params, Code, PARAM_IF_POS_INTO_BASE_INT, PARAM_IF_POS_INTO_SPOS_INT, PARAM_IF_POS_INTO_ADDX_LNG, PARAM_IF_POS_INTO_ADDY_LNG) = False Then
            Exit Function
        End If
        Nest = 2
        
    Case PCODE_IF_TIME_OVER
        If TryParamTimeID(Params, Code, PARAM_IF_TIME_OVER_TIME_ID_INT) = False Then
            Exit Function
        End If
        If TryParamLng(Params, Code, PARAM_KEY_LIMIT, PARAM_IF_TIME_OVER_LIMIT_LNG, TIME_OVER_LIMIT_LOWER, TIME_OVER_LIMIT_LIMIT) = False Then
            Exit Function
        End If
        Nest = 2
        
    Case PCODE_LOOP
        Nest = 1

    Case PCODE_REPEAT
        If TryParamLng(Params, Code, PARAM_KEY_COUNT, PARAM_REPEAT_COUNT_LNG, REPEAT_LOWER, REPEAT_LIMIT) = False Then
            Exit Function
        End If
        Nest = 1
    
    Case PCODE_SET_COLOR
        If TryParamColorID(Params, Code, PARAM_KEY_COLOR_ID, PARAM_SET_COLOR_COLOR_ID_INT) = False Then
            Exit Function
        End If
        If Params.Exists(PARAM_KEY_TYPE) = False Then
            Exit Function
        End If
        sParam = Params.Item(PARAM_KEY_TYPE)
        For i = 0 To SET_COLOR_COUNT - 1
            If sParam = SET_COLOR_TYPE_NAME(i) Then
                Code.IntData(PARAM_SET_COLOR_TYPE_INT) = i
                Exit For
            End If
        Next i
        Select Case i
        Case SET_COLOR_CONSTANT
            If TryParamColor(Params, Code, PARAM_SET_COLOR_COLOR_LNG) = False Then
                Exit Function
            End If
        Case SET_COLOR_COPY
            If TryParamColorID(Params, Code, PARAM_KEY_SRC, PARAM_SET_COLOR_SRC_ID_INT) = False Then
                Exit Function
            End If
        Case SET_COLOR_MANUAL_PICK
        Case SET_COLOR_AUTOMATIC_PICK
            If TryParamCapID(Params, Code, PARAM_SET_COLOR_CAP_ID_INT) = False Then
                Exit Function
            End If
            If TryParamPos(Params, Code, PARAM_SET_COLOR_BASE_INT, PARAM_SET_COLOR_SPOS_INT, PARAM_SET_COLOR_ADDX_LNG, PARAM_SET_COLOR_ADDY_LNG) = False Then
                Exit Function
            End If
        Case Else
            Exit Function
        End Select
    
    ' Position of [NEW-CODE]
    ' Case PCODE_NEW_CODE
    
    End Select
    
    Call mCodeList.Add(Code)
    
    LoadParams = Nest
    
End Function

Private Sub Parser_FindEndTag(TagName As String, Cancel As Boolean)
    Dim Code        As PCode
    Dim id          As Integer
    Dim i           As Integer
    
#If DEBUG_MODE > 0 Then
    Debug.Print "Find End Tag:", TagName
#End If
    
    Select Case mTerm
    Case 0, 2, 3
        Cancel = True
    Case 1
        id = GetPCodeID(TagName)
        If id < 0 Then
            Select Case TagName
            Case PROGRAM_TAG
                If mStack.IsEmptyStack() = False Then
                    Cancel = True
                End If
                mTerm = 99
                
            Case TRUE_TAG
                If mStack.IsEmptyStack() Then
                    Cancel = True
                    Exit Sub
                End If
                Set Code = mStack.Peek().GetTail()
                Select Case Code.CodeID
                Case PCODE_IF_COLOR
                    i = PARAM_IF_COLOR_FALSE_NEST
                Case PCODE_IF_POS_INTO
                    i = PARAM_IF_POS_INTO_FALSE_NEST
                Case PCODE_IF_TIME_OVER
                    i = PARAM_IF_TIME_OVER_FALSE_NEST
                ' Position of [NEW-CODE] with NEST-IF
                ' Case PCODE_NEW_CODE
                Case Else
                    Cancel = True
                    Exit Sub
                End Select
                If Not Code.NestList(i) Is Nothing Then
                    Cancel = True
                    Exit Sub
                End If
                Set Code.NestList(i) = New PCodeList
                Set mCodeList = Code.NestList(i)
                mTerm = 3 ' wait for <false>
                
            Case FALSE_TAG
                If mStack.IsEmptyStack() Then
                    Cancel = True
                    Exit Sub
                End If
                Set Code = mStack.Peek().GetTail()
                Select Case Code.CodeID
                Case PCODE_IF_COLOR
                    i = PARAM_IF_COLOR_FALSE_NEST
                Case PCODE_IF_POS_INTO
                    i = PARAM_IF_POS_INTO_FALSE_NEST
                Case PCODE_IF_TIME_OVER
                    i = PARAM_IF_TIME_OVER_FALSE_NEST
                ' Position of [NEW-CODE] with NEST-IF
                ' Case PCODE_NEW_CODE
                Case Else
                    Cancel = True
                    Exit Sub
                End Select
                If Code.NestList(i) Is Nothing Then
                    Cancel = True
                    Exit Sub
                End If
                Set mCodeList = mStack.Pop()
                mTerm = 1
                
            Case Else
                Cancel = True
            End Select
            Exit Sub
        End If
        Select Case id
        Case PCODE_REPEAT, PCODE_LOOP
            If mStack.IsEmptyStack() Then
                Cancel = True
                Exit Sub
            End If
            Set mCodeList = mStack.Pop()
        ' Position of [NEW-CODE] with NEST-ONE
        ' Case PCODE_NEW_CODE
        End Select
        If mCodeList.Count = 0 Then
            Cancel = True
            Exit Sub
        End If
        If id <> mCodeList.GetTail().CodeID Then
            Cancel = True
        End If
        
    Case Else
        Cancel = True
        
    End Select
End Sub

Private Sub Parser_FindStartTag(TagName As String, Params As Scripting.Dictionary, Cancel As Boolean)
    Dim sParam      As String
    Dim iParam      As Integer
    Dim id          As Integer
    Dim ErrorFlag   As Integer
    Dim Nest        As Integer
    Dim i           As Integer
    
#If DEBUG_MODE > 0 Then
    Debug.Print "Find Start Tag:", TagName
    Dim v
    For Each v In Params
        Debug.Print " param:", v; ";", Params.Item(v)
    Next v
#End If
    
    Select Case mTerm
    Case 0
        If TagName <> PROGRAM_TAG Then
            Cancel = True
            Exit Sub
        End If
        mErrorMessage = "Paramater Error"
        If Params.Exists(PARAM_KEY_FORMAT) = False Then
            Cancel = True
            Exit Sub
        End If
        iParam = ParseInt(Params.Item(PARAM_KEY_FORMAT), 1, FORMAT_VERSION, ErrorFlag)
        If ErrorFlag < 0 Then
            Cancel = True
            Exit Sub
        End If
        If iParam > FORMAT_VERSION Then
            mErrorMessage = "Unsupported Format Version : " & CStr(iParam)
            Cancel = True
            Exit Sub
        End If
        mVersion = iParam
        If Params.Exists(PARAM_KEY_TITLE) Then
            mTitle = Params.Item(PARAM_KEY_TITLE)
        Else
            mTitle = ""
        End If
        If Params.Exists(PARAM_KEY_STOPTIME) Then
            mStopTime = ParseLng(Params.Item(PARAM_KEY_STOPTIME), STOP_TIME_LOWER, STOP_TIME_LIMIT, ErrorFlag)
            If ErrorFlag < 0 Then
                Cancel = True
                Exit Sub
            End If
        Else
            mStopTime = DEFAULT_STOP_TIME
        End If
        mErrorMessage = "Syntax Error"
        mTerm = 1
    
    Case 1
        id = GetPCodeID(TagName)
        If id < 0 Then
            Cancel = True
            Exit Sub
        End If
        Nest = LoadParams(id, Params)
        If Nest < 0 Then
            ' Check Old Versions
            If mVersion = 1 Then
                Nest = LoadParamsV1(id, Params) ' Format Version 1
            End If
        End If
#If DEBUG_MODE > 0 Then
        Debug.Print "nest", Nest
#End If
        If Nest < 0 Then
            mErrorMessage = "Paramater Error"
            Cancel = True
            Exit Sub
        ElseIf Nest = 1 Then
            Call mStack.Push(mCodeList)
            Select Case id
            Case PCODE_REPEAT
                i = PARAM_REPEAT_NEST
            Case PCODE_LOOP
                i = PARAM_LOOP_NEST
            ' Position of [NEW-CODE] with NEST
            ' Case PCODE_NEW_CODE
            Case Else
                Cancel = True
                Exit Sub
            End Select
            Set mCodeList.GetTail().NestList(i) = New PCodeList
            Set mCodeList = mCodeList.GetTail().NestList(i)
            
        ElseIf Nest = 2 Then
            Call mStack.Push(mCodeList)
            Select Case id
            Case PCODE_IF_COLOR
                i = PARAM_IF_COLOR_TRUE_NEST
            Case PCODE_IF_POS_INTO
                i = PARAM_IF_POS_INTO_TRUE_NEST
            Case PCODE_IF_TIME_OVER
                i = PARAM_IF_TIME_OVER_TRUE_NEST
            ' Position of [NEW-CODE] with NEST-IF
            ' Case PCODE_NEW_CODE
            Case Else
                Cancel = True
                Exit Sub
            End Select
            Set mCodeList.GetTail().NestList(i) = New PCodeList
            Set mCodeList = mCodeList.GetTail().NestList(i)
            mTerm = 2 ' wait for <true>
        End If
    Case 2
        If TagName <> TRUE_TAG Then
            Cancel = True
            Exit Sub
        End If
        mTerm = 1
    Case 3
        If TagName <> FALSE_TAG Then
            Cancel = True
            Exit Sub
        End If
        mTerm = 1
    
    Case Else
        Cancel = True
        
    End Select
    
End Sub

Private Sub Parser_FormatError(posLine As Long, posColumn As Long)
    mErrorMessage = "Parse Error"
End Sub
